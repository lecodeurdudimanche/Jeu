package reseau.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import partie.partie.Partie;
import partie.partie.PartieAvecMoteur;
import reseau.server.Client;
import utils.LogLevel;
import utils.Logger;

public class ThreadClient extends Thread{

	private static final float MAX_FPS = 80;
	
	PartieAvecMoteur base, copieCourante;
	private Object lock;
	private long last, nb;
	private Boolean fini, erreur;
	
	public ThreadClient(DataInputStream in, DataOutputStream out) throws IOException {
		lock = new Object();
		fini = false;
		erreur = false;
		last = System.currentTimeMillis();

		out.writeInt(Client.PARTIE_QUERY);
		out.flush();
		
		int taille = in.readInt();
		if (taille > 0)
			copieCourante = base = new PartieAvecMoteur(in);
	}
	
	public boolean estFini() {
		synchronized(fini) {
			return fini;
		}
	}

	public boolean getErreur() {
		synchronized(erreur)
		{
			return erreur;
		}
	}

	public Partie getPartie() {
		synchronized (lock)
		{
			return copieCourante;
		}
	}
	
	public float getNbPerSec()
	{
		long t = System.currentTimeMillis();
		long elapsed = t - last;
		float ret = (nb * 1000) / (elapsed == 0 ? 1 : elapsed);

		if (t - last > 2000)
		{
			nb = (int)ret;
			last = t - 1000;
		}
		return ret < 1 ? 1 : ret;
	}
	
	@Override
	public void run() {
		Logger.println("THREAD RECEPTION : debut", LogLevel.DEBUG);
		
		boolean erreur = false, fini = false;
		while (!Thread.interrupted() && !erreur && !fini)
		{
			long t = System.currentTimeMillis();
			
			PartieAvecMoteur copie = null;
			try {
				copie = new PartieAvecMoteur(base);	

				fini = !DialogueClient.recevoirPartie(copie);
				
				nb++;
			} catch (IOException e) {
				e.printStackTrace();
				erreur = true;
			}

			if (!erreur)
			{
				synchronized (lock)
				{
					copieCourante = copie;
				}
			}
			
			long elapsed = System.currentTimeMillis() - t;
			long shouldSleep = (long) (1000f / MAX_FPS - elapsed);
			if (shouldSleep > 0)
			{
				try {
					Thread.sleep(shouldSleep);
				} catch (InterruptedException e) {
					Logger.println("Interrupted", LogLevel.DEBUG);
					break;
				}
			}
		}
		synchronized(this.fini) {
			this.fini = true;
		}
		synchronized (this.erreur) {
			this.erreur = erreur;
		}
		if (erreur)
			Logger.println("THREAD RECEPETION : erreur", LogLevel.DEBUG);
		
		Logger.println("THREAD RECEPTION : fin",  LogLevel.DEBUG);
	}
	

}
