package reseau.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import element.element.Element;
import utils.LogLevel;
import utils.Logger;

public class AlgoListes {
	public static void receptionListe(DataInputStream stream, List<? extends Element> destination, Function<? super Element, Object> ajouter) throws IOException {
		List<Integer> aSupprimer = new ArrayList<Integer>();
		int nb = stream.readInt();
		Logger.println("nb " + nb, LogLevel.NET_DEBUG);
		for(int i = 0, j = 0; i < nb; i++, j++)
		{
			long id = stream.readLong();
			Logger.println("id " + id, LogLevel.NET_DEBUG);
			j = insert(id, j, destination, aSupprimer);
			
			if (j < destination.size()) 
				destination.get(j).lire(stream, false);
			else
				ajouter.apply(Element.creer(stream, id));
		}
		
		for (int i = aSupprimer.size() - 1; i >=0; i--)
			destination.remove((int)aSupprimer.get(i));
		
		for (int i = destination.size() - 1; i >= nb; i--)
			destination.remove(i);
	}
	
	public static int insert(long id, int i, List<? extends Element> liste, List<Integer> aSupprimer) {
		for (int j = i; j < liste.size() && id != liste.get(j).getId(); j++, i++)
		{
			aSupprimer.add(j);
		}
		return i;
	}

	public static void receptionListeSansModif(DataInputStream stream, List<? extends Element> destination, Function<? super Element, Object> ajouter) throws IOException {
		destination.clear();
		int nb = stream.readInt();

		for (int i = 0; i < nb; i++)
			ajouter.apply(Element.creer(stream, 0));
		
	}
	
	public static void envoiListe(DataOutputStream stream, List<? extends Element> elements, boolean init) throws IOException {
		envoiListe(stream, elements, init, true);
	}

	public static void envoiListeSansModif(DataOutputStream stream, List<? extends Element> elements, boolean init) throws IOException {
		envoiListe(stream, elements, init, false);
	}
	
	private static void envoiListe(DataOutputStream stream, List<? extends Element> elements, boolean init, boolean envoyerID) throws IOException {
		stream.writeInt(elements.size());
		for(Element p : elements)
		{
			if (envoyerID)
				stream.writeLong(p.getId());
			p.ecrire(stream, init);
		}
		
	}
}
