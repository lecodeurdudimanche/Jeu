package reseau.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.util.function.Consumer;

import config.Config;
import config.ConfigKey;
import partie.controles.Controle;
import partie.partie.Partie;
import partie.partie.PartieAvecMoteur;
import partie.statistiques.ListeStatistiques;
import reseau.server.Client;
import reseau.server.DiscoveryServer;
import reseau.server.Salle;
import reseau.server.Server;
import utils.LogLevel;
import utils.Logger;

public class DialogueClient{
	
	private static final int MAJOR_VER = 10, MINOR_VER = 0;

	public static final String 
		VERSION_OK = "ok", 
		VERSION_TROP_RECENTE = "trop récente",
		VERSION_PLUS_RECENTE =  "plus récente",
		VERSION_PLUS_ANCIENNE = "plus ancienne",
		VERSION_TROP_ANCIENNE = "trop ancienne";
	
	private static DatagramSocket sendSocket;
	private static DataInputStream inStream;
	private static DataOutputStream outStream;
	private static Socket socket;
	private static Thread thread;
	private static ThreadClient threadClient;
	private static long idJoueur = -1, lastPing = -1;
	private static String idClient = "";
	
	private static int serverMajorVer = -1, serverMinorVer = -1;
	

	// -------- SCAN ---------
	
	public static void scannerServeursEnLigne(Consumer<String[]> onAddress, Consumer<Long> onStart) throws IOException{
		initScan(onAddress, onStart);
	
		byte[] buffer = {MAJOR_VER, MINOR_VER};
		for (String ip : Config.readString(ConfigKey.SERVER_LIST).split(" "))
		{
			InetAddress addr = InetAddress.getByName(ip);
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length, addr, DiscoveryServer.BROADCAST_PORT);
			sendSocket.send(packet);
			Logger.println("Sended packet to addr : " + ip + " (inetAddr " + addr + ")", LogLevel.DEBUG);
		}
		
		thread.start();
	}
	
	public static void scannerReseauLocal(Consumer<String[]> onAddress, Consumer<Long> onStart) throws IOException {		
		initScan(onAddress, onStart);
		
		InetAddress broadcastAddr = InetAddress.getByName("255.255.255.255");

		byte[] buffer = {MAJOR_VER, MINOR_VER};
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, broadcastAddr, DiscoveryServer.BROADCAST_PORT);

		sendSocket.send(packet);
		
		thread.start();
		
	}
	
	private static void initScan(Consumer<String[]> onAddress, Consumer<Long> onStart) throws IOException
	{
		//Cleanup
		terminerScan();
		
		sendSocket = new DatagramSocket();
		sendSocket.setSoTimeout(50);
		
		thread = new Thread() {
			@Override
			public void run() {
				boolean err = false;
				long debut = System.currentTimeMillis();
				byte[] buffer = new byte[2];
				
				DatagramPacket packetR = new DatagramPacket(buffer, 2);
				Logger.println("Début de la découverte des serveurs...");
				
				onStart.accept(debut);
				
				while (!err && !Thread.interrupted())
				{
					boolean received = true;
					try {
						sendSocket.receive(packetR);
						Logger.println("" + packetR, LogLevel.NET_DEBUG);
					}catch(SocketTimeoutException e) { 
						received = false;
					} catch (IOException e) {
						Logger.printlnErr("Erreur réseau : " + e.getMessage());
						received = false;
						err = true;
					}
					
					if (received)
					{
						String version = VERSION_OK;
						if (buffer[0] > MAJOR_VER)
							version = VERSION_TROP_RECENTE;
						else if (buffer[0] < MAJOR_VER)
							version = VERSION_TROP_ANCIENNE;
						else if (buffer[1] > MINOR_VER)
							version = VERSION_PLUS_RECENTE;
						else if (buffer[1] < MINOR_VER)
							version = VERSION_PLUS_ANCIENNE;							
						
						String ping = "" + (System.currentTimeMillis() - debut);

						Logger.println("Received : v" + buffer[0] + "." + buffer[1] + " " + ping + "ms", LogLevel.NET_DEBUG);
						
						onAddress.accept(new String[] {packetR.getAddress().getHostAddress(), version, ping});
					}
				}
				Logger.println("Fin de la découverte des serveurs...", LogLevel.NET_DEBUG);
			}
		};
		
	}

	public static void terminerScan() {
		if (thread != null)
		{
			thread.interrupt();
			try {
				thread.join();
			} catch (InterruptedException e) {}
			thread = null;
			sendSocket.close();
		}
	}
	
	// ----- CONNECTION SERVEUR -----

	public static void connecterServeur(String adresse, String pseudo) throws IOException {
		
		socket = new Socket();
		
		socket.connect(new InetSocketAddress(adresse, Server.GAME_PORT), 5000);
		if (! socket.isConnected())
			throw new IOException("Impossible de se connecter au serveur");
			
		socket.setSoTimeout(Config.readInt(ConfigKey.SERVER_TIMEOUT)); //Si on attend plus de 5 secondes sans réponse du serveur on estime que la connexion est invalidée
		Logger.println("Connected ! ");
		
		outStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));		
		inStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		
		//Connection init
		
		//Version exchange
		serverMajorVer = inStream.readInt();
		serverMinorVer = inStream.readInt();
		outStream.writeInt(MAJOR_VER);
		outStream.writeInt(MINOR_VER);
		
		//Envoi d'infos aux serveur
		outStream.writeUTF(pseudo);
		outStream.flush();

		Logger.println("Version serveur : " + serverMajorVer + "." + serverMinorVer);
		if (serverMajorVer >= 10)
		{
			idClient = inStream.readUTF();
			Logger.println("Id Client : " + idClient);
		}
		
		if (serverMajorVer != MAJOR_VER)
		{
			disconnect();
			throw new IOException("La version du serveur ne correspond pas");
		}
	}
	
	public static void disconnect() throws IOException
	{
		if (!communicationFinie())
			outStream.writeInt(Client.DISCONNECT);
		
		socket.close();
	}
				
	
	// ---- CREATION/SELECTION DE SALLES ----


	public static void recupererSalles(Consumer<Salle> onSalleCreated) throws IOException {
		outStream.writeInt(Client.SALLE_QUERY);
		outStream.flush();

		int nb = inStream.readInt();
		
		for (int i = 0; i < nb; i++)
			onSalleCreated.accept(new Salle(inStream));
		
	}
	
	public static boolean creerSalle(String nom, String clef, int nb, int w, int h)
	{
		boolean accepted= false;
		try {
			outStream.writeInt(Client.SALLE_CREATE);
			outStream.writeUTF(nom);
			outStream.writeBoolean(clef != null);
			if (clef != null)
				outStream.writeUTF(clef);
			outStream.writeInt(nb);
			outStream.writeInt(w);
			outStream.writeInt(h);
			outStream.flush();
			
			accepted = inStream.readBoolean();
		} catch (IOException e) {
			Logger.printlnErr("Erreur à la création de la salle : " + e);
			return false;
		}
		return accepted;			
	}
	
	public static boolean connecterSalle(String nom) throws IOException{
		return connecterSalle(nom, null);
	}
	
	public static boolean connecterSalle(String nom, String clef) throws IOException{
		boolean ok = false;
		
		outStream.writeInt(Client.SALLE_CONNECT);
		Logger.println("Sended: salle_connect", LogLevel.NET_DEBUG);
		outStream.writeUTF(nom);
		Logger.println("Sended nom : " + nom, LogLevel.NET_DEBUG);
		
		outStream.writeBoolean(clef != null);
		Logger.println("Sended has clef : " + (clef != null), LogLevel.NET_DEBUG);
		if (clef != null)
		{
			outStream.writeUTF(clef);		
			Logger.println("Sended clef : " + (clef), LogLevel.NET_DEBUG);

		}
			
		outStream.flush();
		Logger.println("Flushed", LogLevel.NET_DEBUG);

		ok = inStream.readBoolean();
		Logger.println("Received : " + ok, LogLevel.NET_DEBUG);
		
		if (ok)
		{
			idJoueur  = inStream.readLong();
			Logger.println("Received id = : " + idJoueur, LogLevel.NET_DEBUG);
		}
		
		Logger.println("Returning : " + ok, LogLevel.NET_DEBUG);
		return ok;
	}
	
	// ---- LOBBY -----
	
	public static void setEquipe(int team) throws IOException
	{
		Logger.println("Envoie de l'equipe " + team, LogLevel.NET_DEBUG);
		outStream.writeInt(Client.SET_TEAM);
		outStream.writeInt(team);
		outStream.flush();
	}

	public static void deconnexionSalle() throws IOException {
		Logger.println("Envoie deconnexion de la salle", LogLevel.NET_DEBUG);
		outStream.writeInt(Client.SALLE_DISCONNECT);
		outStream.flush();
	}
	
	public static Salle recupererLobby() throws IOException {
		Logger.println("Envoie demande de lobby", LogLevel.NET_DEBUG);

		outStream.writeInt(Client.LOBBY_QUERY);
		outStream.flush();
		
		boolean salleNonNulle = inStream.readBoolean();
		Logger.println("Reçu : " + (salleNonNulle ? "salle non null" : "salle nulle"), LogLevel.NET_DEBUG);

		if (salleNonNulle)
			return new Salle(inStream);
		
		return null;
	}
	
	public static void lancerPartie() throws IOException
	{
		Logger.println("Envoie debut de partie", LogLevel.NET_DEBUG);
		outStream.writeInt(Client.PARTIE_START);
		outStream.flush();
	}

	public static void initPartie() throws IOException {
		threadClient = new ThreadClient(inStream, outStream);
		threadClient.start();
	}

	public static void kick(String pseudo) throws IOException {
		Logger.println("Envoie kick " + pseudo, LogLevel.NET_DEBUG);
		outStream.writeInt(Client.LOBBY_KICK);
		outStream.writeUTF(pseudo);
		outStream.flush();
	}
	
	
	// ----- JEU ----
	
	public static void envoyerCommande(Controle c, float[] args) {
		long t = System.nanoTime();
		try{
			outStream.writeInt(Client.ENVOI_COMMANDE);
			
			outStream.writeInt(c.getIndex());
			
			outStream.writeInt(args == null ? 0 : args.length);
			if (args != null)
			{
				for (float a : args)
					outStream.writeFloat(a);
			}
			outStream.flush();
		}catch(IOException e)
		{
			Logger.printlnErr("Erreur d'écriture au serveur : " + e);
		}	
	}
	
	public static boolean recevoirPartie(PartieAvecMoteur partie) throws IOException {
		long debut = System.currentTimeMillis();
		
		outStream.writeInt(Client.PARTIE_QUERY);
		outStream.flush();

		int reponse = inStream.readInt();
		lastPing = System.currentTimeMillis() - debut;
		
		Logger.println("Received size = " + reponse, LogLevel.NET_DEBUG);

		if (reponse == 0)
			return true;
		else if (reponse == Client.SALLE_DISCONNECT)
			return false;
		else if (reponse == Client.DISCONNECT)
			return false;
		
		/*
		 * Plus la valeur de coeff est grande :
		 * 		- plus le retard maximum est augmenté en cas de connexion lente
		 * 		- plus les risques de drop de FPS sont diminés (buffer plus grand)
		 */
		//50 ms de retard max et on atteint 155 FPS (-25% par rapport à l'envoi)
		/*float coeff = getNbPerSec() / 20;
		if (coeff < 1)
			coeff = 1;
		while (taille * coeff < inStream.available())
		{
			int n = inStream.skipBytes(taille);
			Logger.println("Discarded " + n + "/" + taille + " bytes",  LogLevel.NET_DEBUG);
			taille = inStream.readInt();
		}*/
		
		partie.lire(inStream);
		
		if (partie.estFinie())
		{
			ListeStatistiques.lire(inStream);
			return false;
		}

		return true;
	}

	public static boolean communicationFinie() {
		return threadClient != null && threadClient.estFini();
	}


	public static boolean getErreur() {
		return threadClient != null && threadClient.getErreur();
	}
	public static Partie getPartie() {
		return threadClient == null ? null : threadClient.getPartie();
	}

	public static float getNbPerSec() {
		return threadClient == null ?  0 : threadClient.getNbPerSec();
	}

	public static long getIdJoueur() {
		return idJoueur;
	}

	public static int getPing() {
		return (int) lastPing;
	}

	public static String getIdClient() {
		return idClient;
	}
}
