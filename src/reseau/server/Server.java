package reseau.server;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import config.Config;
import config.ConfigKey;
import partie.configuration.ConditionArret;
import partie.configuration.ParametresPartie;
import updater.Updater;
import utils.Logger;
import utils.Ressources;

import java.awt.GraphicsEnvironment;
import java.io.*;

/**
 * @author Mael
 *
 */

public class Server{

	public static final int MAJOR_VER = 10;
	public static final int MINOR_VER = 0;
	public static final int GAME_PORT = 5000;
	private List<Salle> salles;
	private List<Thread> threads;
	private List<Client> clients;
	private boolean closed;
	private int port;
	
	private DiscoveryServer discovery;
	private ListeningServer listening;

	
	public Server()		
	{		
		salles = new ArrayList<>();
		threads = new ArrayList<>();
		clients = new ArrayList<>();
	}
	
	public boolean start(int port)
	{
		try {
			initServer(port);
		} catch (IOException e) {
			Logger.printlnErr("Erreur lors de la création du serveur : " + e.getMessage());
			return false;
		}
		return true;
		
	}
	
	public void ajouterSalle(Salle s) {
		Thread t;
		synchronized(salles)
		{
			salles.add(s);
			t = new Thread(s, "Salle " + salles.size());
		}
		t.setDaemon(true);
		t.start();
		threads.add(t);
	}
	
	public List<Salle> getSalles()
	{
		synchronized(salles)
		{
			return new ArrayList<>(salles);
		}
	}
	
	private void initServer(int port) throws IOException
	{
		Logger.println("Starting server...");
		
		this.port = port;
		
		String[] noms = {"Principale"};
		int[] maxJoueurs = {20, 3, 3, 3, 3, 0, 2, 1};
		int nb = noms.length;
		for (int i = 0; i < nb; i++)
		{
			ParametresPartie params = new ParametresPartie();
			params.setConditionArret(new ConditionArret());
			
			Salle s = new Salle(noms[i], maxJoueurs[i]);
			s.setParams(params);
			s.demarrer();
			s.setRejoignable(true);
			
			ajouterSalle(s);
		}
		
		discovery = new DiscoveryServer();
		discovery.setDaemon(true);
		discovery.start();
		
		Consumer<Socket> addClient = s -> {
			//On essaye d'etablir la connexion
			try {
				
				s.setSoTimeout(5000);
				Client c = new Client(s, this);
				c.setDaemon(true);
				synchronized (clients)
				{
					clients.add(c);
				}
			
				Logger.println("Connexion réussie avec le client "+c.getId()+" ("+s.getRemoteSocketAddress()+") !");

			}catch (IOException e)
			{
				Logger.printlnErr("Impossible d'établir une connexion avec le client : " + e);
			}
		};
		
		listening = new ListeningServer(port, () -> closed, addClient);
		listening.setDaemon(true);
		listening.start();

		closed = false;
		
	}
	
	public void close(){
		Logger.println("Stoping server...");
		closed = true;
		
		for (Client c : clients)
		{
			try {
				c.interrupt();
				c.join();
			} catch (InterruptedException e) {
				Logger.printlnErr("Interruption lors de l'attente de la fermeture du client");
			}
		}
		clients.clear();
		
		for (Thread t: threads)
		{
			t.interrupt();
			try {
				t.join();
			} catch (InterruptedException e) {
				Logger.printlnErr("Interruption lors de l'attente de la fermeture de la salle");
			}
		}
		threads.clear();
		salles.clear();
		
		discovery.interrupt();
		try {
			discovery.join();
		} catch (InterruptedException e) {
			Logger.printlnErr("Interruption lors de l'attente du serveur d'écoute du réseau local");
		}
		
		try {
			listening.join();
		} catch (InterruptedException e) {
			Logger.printlnErr("Interruption lors de l'attente du serveur principal");
		}
	}
	
	public void restart()
	{
		close();
		try {
			initServer(port);
		} catch (IOException e) {
			Logger.printlnErr("Erreur lors du démarrage du serveur : " + e.getMessage());
		}	
		
	}
	
	public void manageClientsEtSalles()
	{
		synchronized (clients) {
			//On start les clients
			clients.forEach(c -> {
				if (! c.estDemarre() && ! c.isAlive())
					c.start();
			});
			
			//On enleve les clients qui sont finis
			clients.removeIf(c -> ! c.isAlive());
		}
		
		synchronized(salles) {
			//On enleve les salles finies
			salles.removeIf(s -> s.estFini());
		}
	}

	public static void main(String[] args) throws IOException
	{
		
		boolean headless = GraphicsEnvironment.isHeadless() || (args.length != 0 && Arrays.asList(args).indexOf("--headless") != -1);
		
		Gestion gestion;
		
		if (headless)
			gestion = new GestionConsole();
		else
			gestion = new InterfaceGestion();
		
		if (Config.readBoolean(ConfigKey.UPDATE_CHECK))
		{
			if (headless)
				Updater.launchConsoleUpdate();
			else
				Updater.launchGUIUpdate();
		}
		
		Logger.println("Niveau de verbosité : " + Logger.getLogLevel().toString());
		
		Server s = new Server();
		
		Ressources.init(null);
		
		boolean fini = ! s.start(GAME_PORT);
		
		while(! fini)
		{
			List<EvenementServeur> events = gestion.getEvenements(); 
			for (EvenementServeur e : events)
			{
				switch(e)
				{
				case CLOSE:
					s.close();
					fini = true;
					break;
				case RESTART:
					s.restart();
					break;
				}
			}
			
			s.manageClientsEtSalles();
			
			try {
				Thread.sleep(100);
			}catch(InterruptedException e)
			{
				s.close();
				fini = true;
			}
		}
		
		Logger.println("Fermeture du serveur");
		System.exit(0);
	}

}
