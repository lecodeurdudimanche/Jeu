package reseau.server;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import utils.LogLevel;
import utils.Logger;

public class InterfaceGestion extends Gestion {
	
	private JTextArea outField, errField;
	private JButton restart, clearErr, clearOut;
	private JComboBox<LogLevel> comboLog;
	private JFrame fenetre;
	
	public InterfaceGestion()
	{
		fenetre = new JFrame("Gestion du serveur");
		
		fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fenetre.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Logger.setErrStream(System.err);
				Logger.setStdStream(System.out);
				
				pushEvent(EvenementServeur.CLOSE);
			}
		});
		
		comboLog = new JComboBox<>(LogLevel.values());
		comboLog.setSelectedItem(Logger.getLogLevel());
		
		outField = new JTextArea(15, 50);
		errField = new JTextArea(15, 50);
		
		outField.setEditable(false);
		errField.setEditable(false);
		errField.setForeground(Color.RED);
		
		Logger.setStdStream(new PrintStream(new TextAreaStream(outField)));
		Logger.setErrStream(new PrintStream(new TextAreaStream(errField)));
		
		clearErr = new JButton("Effacer");
		clearOut = new JButton("Effacer");
		
		JPanel principal = new JPanel(new BorderLayout());
		principal.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JPanel pnord = new JPanel();
		
		restart = new JButton("Relancer le serveur");

		pnord.add(restart);
		pnord.add(new JLabel("Niveau de verbosité :"));
		pnord.add(comboLog);
		
		principal.add(pnord, BorderLayout.NORTH);
		JPanel streams = new JPanel(new GridLayout(2, 1));
		JPanel outStream = new JPanel(new BorderLayout());
		JPanel outStreamHeader = new JPanel(new BorderLayout());
		
		outStreamHeader.add(new JLabel("Informations du serveur"));
		outStreamHeader.add(clearOut, BorderLayout.EAST);
		
		outStream.add(outStreamHeader, BorderLayout.NORTH);
		outStream.add(new JScrollPane(outField));
		
		JPanel errStream = new JPanel(new BorderLayout());
		JPanel errStreamHeader = new JPanel(new BorderLayout());
		
		errStreamHeader.add(new JLabel("Erreurs du serveur"));
		errStreamHeader.add(clearErr, BorderLayout.EAST);
		
		errStream.add(errStreamHeader, BorderLayout.NORTH);
		errStream.add(new JScrollPane(errField));
		
		streams.add(outStream);
		streams.add(errStream);
		
		principal.add(streams);
		
		
		//EVENTS
		restart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pushEvent(EvenementServeur.RESTART);
			}
		});
	
		clearOut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				outField.setText("");
			}
		});
		clearErr.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				errField.setText("");
			}
		});
		
		comboLog.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Logger.setLogLevel((LogLevel)comboLog.getSelectedItem());				
			}
		});
		
		
		fenetre.setContentPane(principal);
		fenetre.pack();
		fenetre.setVisible(true);
	}
	
	private class TextAreaStream extends OutputStream {
		private JTextArea area;
		private byte[] buffer;
		private int i;
		
		public TextAreaStream(JTextArea a)
		{
			area = a;
			buffer = new byte[512];
			i = 0;
		}
		
		@Override
		public void write(int c)
		{
			buffer[i++] = (byte) c;
			
			//Force flush
			if (c == '\n' || i == buffer.length - 2)
				flush();
		}
		
		@Override
		public void flush()
		{
			try {
				area.append(new String(buffer, "UTF8"));
			} catch (UnsupportedEncodingException e) {
				area.append(new String(buffer));
			}
			
			buffer = new byte[buffer.length];
			i = 0;
			area.setCaretPosition(area.getDocument().getLength());
		}
	}

}
