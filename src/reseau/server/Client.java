package reseau.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import partie.configuration.ConditionArret;
import partie.configuration.ParametresPartie;
import partie.controles.Controleur;
import partie.controles.ControleurReseau;
import partie.partie.PartieAvecMoteur;
import partie.statistiques.ListeStatistiques;
import utils.LogLevel;
import utils.Logger;

public class Client extends Thread{
	
	public static final int SALLE_QUERY = 1, SALLE_CREATE = 2, SALLE_CONNECT = 3, DISCONNECT = 4, PARTIE_QUERY = 5, ENVOI_COMMANDE = 6,
							LOBBY_QUERY = 7, PARTIE_START = 8, LOBBY_KICK = 9, SET_TEAM = 10, SALLE_DISCONNECT = 11;
	
	private static final long INACTIVITY_TIMEOUT = 10000;

	
	private String id, pseudo;
	private static int numId = 0;
	
	/*
	 * Tous les champs suivant ne doivent pas être envoyés !!
	 */
	private ControleurReseau controleur;
	
	private Socket socket;

	private DataOutputStream outStream;
	private DataInputStream inStream;
	private boolean premierEnvoi, demarre;
	private int equipe;
	private Server server;
	private PartieAvecMoteur partie;
	private Object lockPartie;
	private Salle salle;
	private List<Integer> events;

	
	public Client(Socket sock, Server s) throws IOException
	{
		id = getNextId();
		
		pseudo = id;
		socket = sock;

		BufferedOutputStream buffer = new BufferedOutputStream(socket.getOutputStream());
		outStream = new DataOutputStream(buffer);
		inStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		
		//Connection init
		initConnection();
		
		//On attache le controlable au controleur plus tard dans salle.connexion()
		controleur = new ControleurReseau(null);

		premierEnvoi = true;
		server = s;
		demarre = false;
		partie = null;
		lockPartie = new Object();
		salle = null;
		events = new ArrayList<>();
		equipe = 0;
	}

	//Client en tant que modele uniquement
	public Client(DataInputStream stream) throws IOException
	{
		lire(stream);
	}

	private void initConnection() throws IOException {
		//Version exchange 
		outStream.writeInt(Server.MAJOR_VER);
		outStream.writeInt(Server.MINOR_VER);
		outStream.flush();
		
		int clientMajorVer, clientMinorVer;
		clientMajorVer = inStream.readInt();
		clientMinorVer = inStream.readInt();
		
		String copie = inStream.readUTF();
		if (copie.length() > 10)
			copie = copie.substring(0, 10);
		
		pseudo = copie;
		
		if (clientMajorVer >= 10)
		{
			outStream.writeUTF(id);
			outStream.flush();
		}
	}
	
	private static String getNextId()
	{
		return "" + numId++;
	}
	
	@Override
	public void run()
	{
		demarre = true;
		boolean fini = false;
		boolean enJeu = false;
		
		long derniereActivite = System.currentTimeMillis();
		while (!fini && !interrupted())
		{
			try {
				//On check le delai d'activite
				long temps = System.currentTimeMillis();
				if (temps - derniereActivite > INACTIVITY_TIMEOUT)
				{
					Logger.println("Client " + pseudo + " | Délai maximum d'inactivité dépassée (" + (INACTIVITY_TIMEOUT/1000) + ")");
					fini = true;
				}
				
				//Si pas de donnees reçus, on skip
				if (inStream.available() == 0)
				{
					//Si on n'est pas en jeu on ménage le processeur
					if (!enJeu)
					{
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							fini = true;
						}
					}
					continue;
				}
				
				pushEvent(inStream.readInt());
				derniereActivite = temps;

				for (int cmd : getEvents())
				{
					Logger.println("Client " + pseudo + " | Commande reçue : " + cmd, LogLevel.NET_DEBUG);
					switch(cmd)
					{
						case SALLE_QUERY: 
							envoyerSalles();
							break;
						case SALLE_CREATE:
							creerSalle();
							break;
						case SALLE_CONNECT:
							salle = connexionSalle();
							break;
						case LOBBY_QUERY:
							envoyerLobby();
							if (salle != null && salle.estEnJeu())
								enJeu = true;
							break;
						case LOBBY_KICK:
							if (salle != null && !salle.estEnJeu() && this == salle.getOwner())
								expulserClient();
							break;
						case SET_TEAM:
							if (salle != null && !salle.estEnJeu())
								equipe = inStream.readInt();
							break;
						case PARTIE_START:
							if (salle != null && this == salle.getOwner())
								salle.demarrer();
							break;
						case PARTIE_QUERY:
							envoyerPartie();
							synchronized(lockPartie)
							{
								if (partie.estFinie())
								{
									ListeStatistiques.ecrire(outStream);
									enJeu = false;
									fini = true;
								}
							}
							break;
						case ENVOI_COMMANDE:
							controleur.readCommand(inStream);
							break;
						case SALLE_DISCONNECT:
							if (salle != null)
								salle.deconnexion(pseudo);
							salle = null;
							enJeu = false;
							break;
						case DISCONNECT:
							Logger.println("Client " + pseudo + " | Déconnection solicitée", LogLevel.NET_DEBUG);
							fini = true;
							break;
						default:
							Logger.println("Client " + pseudo + " | Commande non reconue : " + cmd, LogLevel.DEBUG);
							break;

					}
				}
			} catch (IOException e) {
				Logger.printlnErr("Client " + pseudo + " | Erreur de communication lors du choix de salle.");
				fini = true;
			}
		}
		Logger.println("Client " + pseudo + " | Déconnection");
		
		try {
			outStream.writeInt(DISCONNECT);
			outStream.close();
			inStream.close();
		} catch (IOException e) {
			Logger.println("Client " + pseudo + " | Erreur de fermeture de stream : " + e, LogLevel.DEBUG);
		}
		Logger.println("Client " + pseudo + " | Shutdown completed", LogLevel.DEBUG);
	}

	private List<Integer> getEvents() {
		List<Integer> ret =  new ArrayList<>(events);
		events.clear();
		return ret;
	}

	public void pushEvent(int e) {
		events.add(e);
	}

	private void envoyerLobby() throws IOException {
		Logger.println("Salle : " + salle, LogLevel.NET_DEBUG);
		
		outStream.writeBoolean(salle != null);
		if (salle != null)
			salle.ecrire(outStream);
		
		outStream.flush();
	}
	
	private void expulserClient() throws IOException {
		String pseudo = inStream.readUTF();	
		Logger.println("Client " + this.pseudo + " | Expulsion de " + pseudo + " de la salle", LogLevel.NET_DEBUG);
		salle.deconnexion(pseudo);
	}

	private Salle connexionSalle() throws IOException {
		List<Salle> salles = server.getSalles();
		//On recupère le nom de la salle
		String nom = inStream.readUTF();
		Logger.println("Readed nom = " + nom, LogLevel.NET_DEBUG);
		
		boolean aEnvoyeClef = inStream.readBoolean();
		Logger.println("Readed has cled = " + aEnvoyeClef, LogLevel.NET_DEBUG);
		String clef = null;
		if (aEnvoyeClef) {
			clef = inStream.readUTF();
			Logger.println("Readed clef = " + clef, LogLevel.NET_DEBUG);
		}
		
		Salle s = null;
		synchronized(salles) {

			for (int i = 0; i < salles.size() && s == null; i++)
			{
				if (nom.equals(salles.get(i).getNom()))
					s = salles.get(i);
			}
				
			//On connecte le client à cette salle
			boolean correct = s != null && s.peutSeConnecter(this, clef);
			outStream.writeBoolean(correct);
			//On envoie la réponse
			outStream.flush();
			

			Logger.println("Writed correct = " + correct, LogLevel.NET_DEBUG);
			
			if (correct)
			{
				long id = s.connexion(this, clef);

				outStream.writeLong(id);
				outStream.flush();

				Logger.println("Writed id = " + id, LogLevel.NET_DEBUG);				
			}
		}

		return s;
	}

	private void creerSalle() throws IOException {
		String nom = inStream.readUTF();
		boolean hasClef = inStream.readBoolean();
		String clef = null;
		if (hasClef)
			clef = inStream.readUTF();

		int nbJoueurs = inStream.readInt();
		
		int w = inStream.readInt();
		int h = inStream.readInt();
		
		List<Salle> salles = server.getSalles();
		
		boolean nomExiste = false;
		synchronized(salles)
		{
			for (Salle s : salles)
			{
				if (s.getNom().equals(nom))
				{
					nomExiste = true;
					break;
				}
			}
		}
		
		boolean accept = salles.size() < 6 && nbJoueurs >= 2 && nbJoueurs <= 10 && !nomExiste && w > 100 && h > 100 && w < 10000 && h < 10000;
		outStream.writeBoolean(accept);
		outStream.flush();
		
		if (accept)
		{
			ParametresPartie params = new ParametresPartie(w, h, 0);
			params.setConditionArret(new ConditionArret(10l * 1000l));
			params.setViewW(1366);
			params.setViewH(768);
			params.setTeams(true);
			params.setFriendlyFire(false);
			
			Salle s = new Salle(nom, nbJoueurs, params, clef, this);
			server.ajouterSalle(s);
		
		}
	}

	private void envoyerSalles() throws IOException {
		List<Salle> salles = server.getSalles();
			
		//On envoie les salles
		Logger.println("Envoi de " + salles.size() + " salles", LogLevel.NET_DEBUG);
		outStream.writeInt(salles.size());
		for(Salle salle : salles)
			salle.ecrire(outStream);
		
		outStream.flush();
		Logger.println("Envoi des salles terminé", LogLevel.NET_DEBUG);
	}
	
	private void envoyerPartie() throws IOException
	{
			
		long t1 = System.nanoTime();
		
		ByteArrayOutputStream buffStream = new ByteArrayOutputStream();
		//On écrit la partie
		//IMPORTANT : TANT QU'ON SKIP LES INFOS SANS REGARDER S'IL Y A CHANGEMENT, ON DOIT TOUT ENVOYER TT LE TEMPS
		synchronized(lockPartie)
		{
			if (partie != null)
				partie.ecrire(new DataOutputStream(buffStream), true);
		}
		
		byte[] buff = buffStream.toByteArray();
		//On envoie la taille puis la partie
		outStream.writeInt(buff.length);
		outStream.write(buff);
		Logger.println("Taille de la partie : " + buff.length, LogLevel.NET_DEBUG);
		
		long t2 = System.nanoTime();
		
		//On force l'envoi
		outStream.flush();
		
		float d1 = (t2 - t1) / 1E6f, d2 = (System.nanoTime() - t2) / 1E6f;
		
		Logger.println("write :"+ d1 +" ms, flush "+d2+" ms", LogLevel.NET_DEBUG);
		premierEnvoi = false;
	}
	
	public void setPartie(PartieAvecMoteur p)
	{
		synchronized(lockPartie) {
			partie = new PartieAvecMoteur(p);
		}
	}
	
	public Controleur getControleur()
	{
		return controleur;
	}
	
	//Conflit de nom avec Thread.getId()
	public String getClientId() 
	{
		return id;
	}
	
	public String getPseudo() 
	{
		return pseudo;
	}

	public int getTeam() {
		return equipe;
	}

	public boolean estDemarre() {
		return demarre;
	}
	

	public void ecrire(DataOutputStream stream) throws IOException {

		stream.writeUTF(id);
		stream.writeUTF(pseudo);
		stream.writeInt(equipe);
	}

	public void lire(DataInputStream stream) throws IOException {
		id = stream.readUTF();
		pseudo = stream.readUTF();
		equipe = stream.readInt();
	}

}
