package reseau.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;

import utils.LogLevel;
import utils.Logger;

public class DiscoveryServer extends Thread{
	
		public static final int BROADCAST_PORT = 5001;
		
		public DiscoveryServer()
		{
			super("Discovery");
		}
		
		@Override
		public void run()
		{
			try (DatagramSocket socket = new DatagramSocket(BROADCAST_PORT)){
	
				socket.setSoTimeout(1000);
				
				byte[] buffer = new byte[2];
				DatagramPacket packet = new DatagramPacket(buffer, 2);
										
				while(!interrupted())
				{
					try{
						socket.receive(packet);
					} catch (SocketTimeoutException e) {
						continue;
					}
					Logger.println("Received ping request from " + packet.getAddress());

					buffer = new byte[]{Server.MAJOR_VER, Server.MINOR_VER};
					packet.setData(buffer);
					
					socket.send(packet);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			Logger.println("Discovery thread exited", LogLevel.DEBUG);
		}

}
