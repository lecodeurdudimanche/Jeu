package reseau.server;

import java.util.ArrayList;
import java.util.List;

public abstract class Gestion {
	private List<EvenementServeur> events;
	
	
	protected Gestion()
	{
		events = new ArrayList<>();
	}

	protected void pushEvent(EvenementServeur event) {
		synchronized(events)
		{
			events.add(event);
		}		
	}
	
	public List<EvenementServeur> getEvenements() {
		List<EvenementServeur> ret;
		synchronized(events)
		{
			ret = new ArrayList<>(events);
			events.clear();
		}
		return ret;
	}

}
