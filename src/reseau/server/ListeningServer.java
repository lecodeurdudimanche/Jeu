package reseau.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

import utils.LogLevel;
import utils.Logger;

public class ListeningServer extends Thread{

	private ServerSocket srv;
	private Consumer<Socket> newClient;
	private BooleanSupplier isClosed;
	
	
	public ListeningServer(int port, BooleanSupplier isClosed, Consumer<Socket> newClient) throws IOException {
		srv = new ServerSocket(port);
		srv.setSoTimeout(1000);
		this.newClient = newClient;
		this.isClosed = isClosed;
	}

	@Override
	public void run()
	{
		boolean error = false;
		while(!error && !isClosed.getAsBoolean()) {
			
			//Si jamais le srv.accept() fail, on shutdown le server
			try{
				Socket socket = null;
				//Si jamais erreur de timeout on ignore
				try {
					socket = srv.accept();
				} catch (SocketTimeoutException e1) {}
				
				//Si jamais il y a bien un client
				if (socket != null)
				{
					Logger.println("Un client s'est connecté au serveur");
					newClient.accept(socket);
				}				
			}catch(IOException e)
			{
				Logger.printlnErr("Error "+e+" : shutting down...");
				error = true;
			}
		}
		
		try {
			srv.close();
		} catch (IOException e) {
			Logger.printlnErr("Erreur lors de la femeture du serveur principal : " + e.getMessage());
		}
		
		Logger.println("Listening thread exited", LogLevel.DEBUG);
	}
}
