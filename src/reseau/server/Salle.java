/**
 * 
 */
package reseau.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import partie.configuration.ParametresPartie;
import partie.partie.PartieAvecMoteur;
import utils.LogLevel;
import utils.Logger;
import utils.Utils;


/**
 * @author Mael
 *
 */
public class Salle implements Runnable{
	
	private final static int FPS_LIMIT = 200;	
	
	private int maxClients;
	private List<Client> clients;
	private Client owner;
	
	
	private String nom;
	//Une clef d'acces ou une string null pour pas de clef, ne doit PAS être enoyée
	private String cle;
	
	private PartieAvecMoteur partie;
	private boolean fini, rejoignable, enJeu;
	private ParametresPartie params;
	
	
	/**
	 * constructeurs d'une partie sans cle
	 * @param nom
	 * @param nbJoueur
	 */
	public Salle(String nom, int nbJoueur) {
		this(nom, nbJoueur, new ParametresPartie(1366, 768, 0));
	}
	public Salle(String nom, int nbJoueur, ParametresPartie p) {
		this(nom, nbJoueur, p, null);
	}
	public Salle(String nom, int nbJoueur, ParametresPartie p, String cle) {
		this(nom, nbJoueur, p, null, null);
	}
	
	/**
	 * constructeur d'une partie avec cle
	 * @param nom
	 * @param nbJoueur
	 * @param cle
	 */
	public Salle(String nom, int nbJoueur, ParametresPartie p, String cle, Client c) {
		this.nom = nom;
		this.maxClients = nbJoueur;
		this.cle = cle;
		//numEnvoi = 0;
		Logger.println("Lancement de la salle " + nom);
	
		//Possible concurrent access
		clients = Collections.synchronizedList(new ArrayList<Client>());
		params = p;
		
		partie = new PartieAvecMoteur(params);
		
		fini = enJeu = false;
		rejoignable = true;
		
		owner = c;
	}
	
	public Salle(DataInputStream inStream) throws IOException {
		lire(inStream);
	}

	//POSSIBLE CONFLIT ENTRE THREADS : appelle par Client
	public long connexion(Client c, String s) {	
		if (peutSeConnecter(c, s))
		{
			long id = 0;
			clients.add(c);
			
			if (enJeu)
				id = ajouterJoueur(c);
			
			Logger.println("Salle " + nom + " | Le client " + c.getPseudo() + " est entré dans la salle");
			return id;
		}
		return -1;
	}
	
	public void deconnexion(String pseudo) {
		synchronized (clients)
		{
			deconnexion(Utils.indexOf(clients, c -> c.getPseudo().equals(pseudo)));
		}
	}
	
	private void deconnexion(int i) {
		if (i >= 0 && i < clients.size())
		{
			clients.get(i).pushEvent(Client.SALLE_DISCONNECT);
			clients.remove(i);
			if (enJeu) {
				synchronized(partie) {
					partie.supprimerJoueur(i);
				}
			}
		}
	}
	
	private long ajouterJoueur(Client c) {
		synchronized (clients)
		{
			c.setPartie(partie);
		}
		
		long id;
		synchronized(partie) {
			int i = partie.ajouterJoueur();
			partie.attacherControleur(c.getControleur(), i);
			
			if (c.getTeam() != -1)
				partie.setEquipeJoueur(i, c.getTeam());
			
			id = partie.getJoueurs().get(i).getId();
			partie.getJoueurs().get(i).setNom(c.getPseudo());
		}
		return id;
	}
	
	public boolean peutSeConnecter(Client c, String s) {
		return rejoignable && getNbClients() < maxClients && (!estProtege() || cle.equals(s));
	}
	
	public Client getOwner()
	{
		return owner;
	}
	
	public void demarrer()
	{
		if (!enJeu)
		{
			synchronized (partie) {
				partie.demarrer();
			}
			synchronized (clients)
			{
				for (Client c : clients)
					ajouterJoueur(c);
			}
			enJeu = true;
			rejoignable = false;
		}
	}
	
	public void setRejoignable(boolean b)
	{
		rejoignable = b;
	}
	
	public boolean estFini()
	{
		return fini;
	}
	
	public boolean estEnJeu() {
		return enJeu;
	}
	
	public String toString() {
		return "nom de la partie : " + nom + " nombre de joueurs : " + clients.size()+"/"+maxClients;
	}
	
	public List<Client> getClients(){
		synchronized(clients)
		{
			return new ArrayList<>(clients);
		}
	}

	public ParametresPartie getParams() {
		return params;
	}
	
	public void setParams(ParametresPartie params) {
		if (!enJeu && params != null)
		{
			this.params = params;
			partie.setParams(params);
		}
	}
	
	public int getMaxClients() {
		return maxClients;
	}

	public String getNom() {
		return nom;
	}

	public int getNbClients()
	{ 
		int nb;
		synchronized(clients) {
			nb =  clients.size();
		}		
		return nb;
	}
	
	public boolean estProtege()
	{
		return cle != null;
	}
	
	public boolean evoluer()
	{
		boolean fini = false;

		//Si la partie a commencé
		if (enJeu)
		{
			//On fait evoluer la partie
			synchronized(partie) {
				fini = ! partie.evoluer();
			}
		}
				
		synchronized(clients)
		{
			for (Client c : clients)
			{
				c.setPartie(partie);
			}

			//On remove les clients deconnectes
			for (int i = clients.size() - 1; i >= 0; i--)
			{
				if (! clients.get(i).isAlive())
				{
					Logger.println("Salle " + nom +" | " + clients.get(i).getPseudo() + " est deconnecté", LogLevel.DEBUG);
					deconnexion(i);
				}
			}
		}
		
		return fini;
	}

	@Override
	public void run() {
		boolean fini = false;
		long lastActivite = System.currentTimeMillis();
		while (! fini)
		{
			float limiteFPS = FPS_LIMIT;
			//Si pas de clients on fait des sleeps de 500ms pour pas surcharger
			if (getNbClients() == 0)
			{
				limiteFPS = 1;
				//Si la salle est inactive depuis plus de 15s et que ce n'est pas la salle principale, on quitte
				if (System.currentTimeMillis() - lastActivite > 15000 && !nom.equals("Principale"))
					fini = true;
			}
			else if (! enJeu)
				limiteFPS = 5;
			
			if (getNbClients() != 0)
				lastActivite = System.currentTimeMillis();
			
			long start = System.currentTimeMillis();
			
			if (!fini)
				fini = evoluer();
			
			float d = (System.currentTimeMillis() - start);
			if (d > 10)
				Logger.println("L'évolution de la partie a duré "+d+" ms", LogLevel.NET_DEBUG);
			
			int sleep = (int)(1000f / limiteFPS - (System.currentTimeMillis() - start));
			if (sleep > 0)
			{
				try {
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
					fini = true;
				}
			}
		}
		for (int i = clients.size() - 1; i >= 0; i--)
		{
			Logger.println("En attente du client " + clients.get(i).getPseudo(), LogLevel.DEBUG);
			try {
				while (clients.get(i).isAlive())
					Thread.sleep(100);
			} catch (InterruptedException e) {break;}
			clients.remove(i);
		}
		Logger.println("Exiting salle " + nom, LogLevel.DEBUG);
		this.fini = true;
	}

	public void ecrire(DataOutputStream outStream) throws IOException {
		outStream.writeUTF(nom);
		outStream.writeBoolean(estProtege());
		
		outStream.writeInt(maxClients);
		outStream.writeInt(getNbClients());
		
		synchronized(clients) {
			for (Client c : clients)
				c.ecrire(outStream);
		}

		outStream.writeBoolean(rejoignable);
		outStream.writeBoolean(enJeu);
		
		params.ecrire(outStream);
	}
	

	private void lire(DataInputStream inStream) throws IOException {
		nom = inStream.readUTF();
		cle = inStream.readBoolean() ? "CleInconnue" : null;
		
		maxClients = inStream.readInt();
		
		int nb = inStream.readInt();
		clients = new ArrayList<Client>(nb);
		for (int i = 0; i < nb; i++)
			clients.add(new Client(inStream));
		
		rejoignable = inStream.readBoolean();
		enJeu = inStream.readBoolean();
		
		params = new ParametresPartie();
		params.lire(inStream);
	}
	
}
