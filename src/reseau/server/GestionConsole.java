package reseau.server;

public class GestionConsole extends Gestion {
	
	private Thread t;
	
	public GestionConsole()
	{
		super();
		t = new Thread() {
			@Override
			public void run() {
				while(!Thread.interrupted())
				{
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						break;
					}
				}
			}
		};
		t.start();
	}

}
