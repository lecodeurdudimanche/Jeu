package partie.statistiques;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public enum TypeStatistique {
	
	DEGATS("Degats"), KILLS("Kills"), TIRS("Tirs"), TOUCHES("Touches appuyées"), FRIENDLY_FIRE("Tirs sur des alliés ou suicide"), MORTS("Morts"), ITEMS("Nombre d'items ramassés");
	
	private String desc;
	
	private TypeStatistique(String str)
	{
		desc = str;
	}
	
	public String toString() {
		return desc;
	}

	public void ecrire(DataOutputStream stream) throws IOException {
		stream.writeInt(ordinal());
	}

	public static TypeStatistique lire(DataInputStream stream) throws IOException {
		int index = stream.readInt();
		return values()[index];
	}
}
