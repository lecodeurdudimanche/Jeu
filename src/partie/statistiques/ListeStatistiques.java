package partie.statistiques;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ListeStatistiques {
	
	private static Map<Long, Map<TypeStatistique, Statistique>> statsGlobales;
	
	//Il ne faut pas instancier cette classe
	private ListeStatistiques() {};

	public static synchronized void log(long l, TypeStatistique type)
	{
		log(l, type, 1);
	}
	
	public static synchronized void log(long l, TypeStatistique type, double valeur)
	{		
		Map<TypeStatistique, Statistique> listeStats = getStatsGlobales().get(l);
		if (listeStats == null)
		{
			listeStats = new HashMap<>();
			getStatsGlobales().put(l, listeStats);
		}
		
		Statistique stats = listeStats.get(type);
		if (stats == null)
		{
			stats = new Statistique();
			listeStats.put(type, stats);
		}
				
		stats.ajouter(valeur);
	}
	
	public static void reset() {
		getStatsGlobales().clear();
	}

	public static synchronized Statistique get(long id, TypeStatistique type)
	{
		Statistique s = getStatsGlobales().containsKey(id) ? getStatsGlobales().get(id).get(type) : null;
		
		return s == null ? new Statistique() : s;
	}
	
	private static Map<Long, Map<TypeStatistique, Statistique>> getStatsGlobales() {
		if (statsGlobales == null)
			init();
		return statsGlobales;
	}

	private static void init() {
		statsGlobales = new HashMap<>();
	}
	
	public static synchronized void ecrire(DataOutputStream stream) throws IOException
	{
		stream.writeInt(getStatsGlobales().size());
		for (Entry<Long, Map<TypeStatistique, Statistique>> stats : getStatsGlobales().entrySet())
		{
			stream.writeLong(stats.getKey());
			
			Map<TypeStatistique, Statistique> statsJoueur = stats.getValue();
			stream.writeInt(statsJoueur.size());
			for(Entry<TypeStatistique, Statistique> stat : statsJoueur.entrySet())
			{
				stat.getKey().ecrire(stream);
				stat.getValue().ecrire(stream);
			}
		}
	}
	
	public static synchronized void lire(DataInputStream stream) throws IOException
	{
		getStatsGlobales().clear();
		int taille = stream.readInt();
		for (int i = 0; i < taille; i++)
		{
			long id = stream.readLong();
			
			Map<TypeStatistique, Statistique> statsJoueur = new HashMap<>();
			int nbStats = stream.readInt();
			
			for(int j = 0; j < nbStats; j++)
			{
				TypeStatistique type = TypeStatistique.lire(stream);
				Statistique stat = new Statistique();
				stat.lire(stream);
				statsJoueur.put(type, stat);
			}
			
			getStatsGlobales().put(id, statsJoueur);
		}
	}
}
