package partie.statistiques;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Statistique {
	
	private double max, min, moyenne, somme;
	private int nb;
	
	public Statistique()
	{
		reset();
	}

	public void reset() {
		max = Double.NaN; 
		min = Double.NaN;
		moyenne = somme = 0;
		nb = 0;
	}

	public void ajouter(double valeur) {
		if (Double.isNaN(max) || valeur > max)
			max = valeur;
		if (Double.isNaN(min) || valeur < min)
			min = valeur;
		
		moyenne = (moyenne * nb + valeur) / (nb + 1);
		nb++;
		somme += valeur;
	}
	
	public double getMax()
	{
		return max;
	}

	public double getMin() {
		return min;
	}

	public double getMoyenne() {
		return moyenne;
	}
	
	public double getSomme() {
		return somme;
	}

	public int getNb() {
		return nb;
	}

	public void ecrire(DataOutputStream stream) throws IOException {
		stream.writeInt(nb);
		
		stream.writeDouble(min);
		stream.writeDouble(max);
		stream.writeDouble(moyenne);
		stream.writeDouble(somme);
	}

	public void lire(DataInputStream stream) throws IOException {
		nb = stream.readInt();
		
		min = stream.readDouble();
		max = stream.readDouble();
		moyenne = stream.readDouble();
		somme = stream.readDouble();
		
	}

}
