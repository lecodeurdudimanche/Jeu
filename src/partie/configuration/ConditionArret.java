package partie.configuration;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class ConditionArret {
	
	private boolean infini;
	private double scoreMax;
	private int nbVies;
	private long end, duree;
	
	public ConditionArret()
	{
		this(-1, -1, -1);
		infini = true;
	}
	
	public ConditionArret(double score)
	{
		this(score, -1, -1);
	}
	
	public ConditionArret(int vivants)
	{
		this(-1, vivants, -1);
	}
	
	public ConditionArret(long duree)
	{
		this(-1, -1, duree);
	}
	
	public ConditionArret(double score, int vivants, long duree)
	{
		scoreMax = score;
		nbVies = vivants;
		this.duree = duree;
		infini = false;
	}
	
	public void demarrer(long t)
	{
		end = duree == -1 ? -1 : t + duree;
	}

	public boolean doitArreter(double score, int vivants, long time) {
		return (!infini) && (
					(scoreMax != -1 && score >= scoreMax) ||
					(nbVies != -1 && vivants >= 0) ||
					(end != -1 && time >= end));
	}
	
	public boolean estInfini()
	{
		return infini;
	}

	public double getScoreMax() {
		return scoreMax;
	}

	public int getNbPersos() {
		return nbVies;
	}

	public long getEnd() {
		return end;
	}

	public void ecrire(DataOutputStream stream) throws IOException {
		stream.writeBoolean(infini);
		if (!infini)
		{
			stream.writeDouble(scoreMax);
			stream.writeInt(nbVies);
			stream.writeLong(duree);
			stream.writeLong(end);
		}
	}

	public void lire(DataInputStream stream) throws IOException {
		infini = stream.readBoolean();
		if (!infini)
		{
			scoreMax = stream.readDouble();
			nbVies = stream.readInt();
			duree = stream.readLong();
			end = stream.readLong();
		}
	}

}
