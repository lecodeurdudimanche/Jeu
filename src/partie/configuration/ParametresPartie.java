package partie.configuration;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import config.Config;
import config.ConfigKey;
import utils.Utils;

public class ParametresPartie {

	
	private static final float MIN_W = 100, MAX_W = 10000, MIN_H = 100, MAX_H = 10000;
	
	private ConditionArret conditionArret;
	private float w, h, viewW, viewH;
	private boolean friendlyFire, teams;
	private int teamSize;
	private int nbBlocs, nbPersos;
	
	public ParametresPartie()
	{
		this(1366, 768, 2);
	}

	public ParametresPartie(float wMap, float hMap, int nbPersos) {
		conditionArret = new ConditionArret();
		w = viewW = wMap;
		h = viewH = hMap;
		friendlyFire = Config.readBoolean(ConfigKey.FRIENDLY_FIRE);
		teams = Config.readBoolean(ConfigKey.TEAMS);
		teamSize = 3;
		nbBlocs = (int) Utils.map(wMap * hMap, 0, 1366 * 768, 0, 20);
		this.nbPersos = nbPersos;
	}
	
	public ParametresPartie copy()
	{
		ParametresPartie p = new ParametresPartie(w, h, nbPersos);
		p.conditionArret = conditionArret;
		p.viewH = viewH;
		p.viewW = viewW;
		p.friendlyFire = friendlyFire;
		p.teams = teams;
		p.teamSize = teamSize;
		p.nbBlocs = nbBlocs;
		return p;
	}

	public ConditionArret getConditionArret() {
		return conditionArret;
	}

	public void setConditionArret(ConditionArret conditionArret) {
		if (conditionArret != null)
			this.conditionArret = conditionArret;
	}

	public float getW() {
		return w;
	}

	public void setW(float w) {
		if (w > MIN_W && w < MAX_W)
			this.w = w;
	}

	public float getH() {
		return h;
	}

	public void setH(float h) {
		if (h > MIN_H && h < MAX_H)
			this.h = h;
	}

	public float getViewW() {
		return viewW;
	}

	public void setViewW(float viewW) {
		if (viewW > MIN_W && viewW < MAX_W)
			this.viewW = viewW;
	}

	public float getViewH() {
		return viewH;
	}

	public void setViewH(float viewH) {
		if (viewH > MIN_H && viewH < MAX_H)
			this.viewH = viewH;
	}

	public boolean isFriendlyFire() {
		return friendlyFire;
	}

	public void setFriendlyFire(boolean friendlyFire) {
		this.friendlyFire = friendlyFire;
	}

	public boolean isTeams() {
		return teams;
	}

	public void setTeams(boolean teams) {
		this.teams = teams;
	}

	public int getTeamSize() {
		return teamSize;
	}

	public void setTeamSize(int teamSize) {
		if (teamSize > 1)
			this.teamSize = teamSize;
	}

	public int getNbBlocs() {
		return nbBlocs;
	}

	public void setNbBlocs(int nbBlocs) {
		if (nbBlocs >= 0)
			this.nbBlocs = nbBlocs;
	}

	public int getNbPersos() {
		return nbPersos;
	}

	public void setNbPersos(int nbPersos) {
		if (nbPersos > 0)
			this.nbPersos = nbPersos;
	}

	public void ecrire(DataOutputStream stream) throws IOException {
		stream.writeFloat(w);
		stream.writeFloat(h);
		
		stream.writeFloat(viewW);
		stream.writeFloat(viewH);
		
		stream.writeBoolean(friendlyFire);
		stream.writeBoolean(teams);
		
		conditionArret.ecrire(stream);
		
		//Pas besoin du reste en fait
	}

	public void lire(DataInputStream stream) throws IOException {
		w = stream.readFloat();
		h = stream.readFloat();
		
		viewW = stream.readFloat();
		viewH = stream.readFloat();
		
		friendlyFire = stream.readBoolean();
		teams = stream.readBoolean();
		
		conditionArret.lire(stream);
	}
}
