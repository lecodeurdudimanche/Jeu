package partie.controles;

import config.Config;
import config.ConfigKey;
import element.element.Element;
import partie.partie.Partie;
import partie.statistiques.ListeStatistiques;
import partie.statistiques.TypeStatistique;

public class ControleurClavier extends Controleur{
	
	private static final int NB_JOUEURS_MAX = 3;
	/**
	 * Enum des etats des touches
	 */
	private static final int ETAT_VIDE = 0, ETAT_APPUYE = 1, ETAT_RELACHE = 2;
	
	/**
	 * Etats des differentes touches
	 */
	private int[] touches;
	/**
	 * Indice de la configuration clavier
	 */
	private int indiceConfig;
	
	/**
	 * Configs predefiniees
	 */
	private static int[][] configs = null;
	
	/**
	 * Nb de controleurs instanciés
	 */
	private static int nbControleurs = 0;
	
	private long debutTir;
	private long tempsTir;
	
	
	public ControleurClavier(Controlable c) throws TropDeControleursException
	{
		super(c);
		
		if (configs == null)
			readDefaultConfigs();
		
		int indice = nbControleurs;
		if (indice >= configs.length)
			throw new TropDeControleursException("Pas assez de controleurs: "+configs.length+" disponibles, "+(indice+1)+" demandés");
		
		indiceConfig = indice;
		nbControleurs++;
		debutTir = tempsTir = -1;
		
		touches = new int[configs[indiceConfig].length];
	}
	
	public static void readDefaultConfigs() {
		Controle[] controles = Controle.values();
		configs = new int[NB_JOUEURS_MAX][controles.length / 2];
		for (int i = 0; i < configs.length; i++)
		{
			for (int k = 0; k < configs[i].length; k++)
			{
				String key = "Joueur[" + (i + 1) + "]." + controles[k];
				configs[i][k] = Config.readKey(ConfigKey.fromString(key));
			}
		}
		
	}

	public void keyPressed(int keycode)
	{
		//Logger.println("Touche appuyée (" + KeyEvent.getKeyText(keycode) + "), t = " + (System.nanoTime() / 1E6) % 4000, LogLevel.DEBUG);
		setValue(keycode, true);

	}
	
	public void keyReleased(int keycode)
	{
		//Logger.println("Touche relachée (" + KeyEvent.getKeyText(keycode) + "), t = " + (System.nanoTime() / 1E6) % 4000, LogLevel.DEBUG);
		setValue(keycode, false);
	}
	
	private void setValue(int keycode, boolean val)
	{		
		Controle[] controles = Controle.values();
		for (int i = 0; i < configs[indiceConfig].length; i++)
		{
			int code = configs[indiceConfig][i];
			if (code == keycode)
			{
				if (touches[i] != ETAT_APPUYE && val && joueur instanceof Element)
					ListeStatistiques.log(((Element) joueur).getId(), TypeStatistique.TOUCHES);
				
				touches[i] = val ? ETAT_APPUYE : ETAT_RELACHE;
				if (controles[i] == Controle.TIR)
				{
					if (val && debutTir == -1)
						debutTir = System.currentTimeMillis();
					else if (!val)
					{
						tempsTir = System.currentTimeMillis() - debutTir;
						debutTir = -1;
					}
				}
			}
		}
	}
	
	public void doActions(Partie p)
	{
		if (joueur == null)
			return;
		
		Controle[] controles = Controle.values();
		int offsetAppuye = controles.length / 2;
		for (int i = 0; i < touches.length; i++)
		{
			float[] arg = null;
			//Traitement special pour le tir
			if (controles[i] == Controle.TIR && touches[i] == ETAT_RELACHE)
			{
				arg = new float[]{tempsTir};
				tempsTir = -1;
			}
			
			if (touches[i] == ETAT_APPUYE)
				joueur.action(controles[i], p, arg);
			else if (touches[i] == ETAT_RELACHE)
			{
				joueur.action(controles[i + offsetAppuye], p, arg);
				touches[i] = ETAT_VIDE;
			}
		}
	}
	
	public void liberer()
	{
		touches = null;
		joueur = null;
		nbControleurs--;
	}
	
}
