package partie.controles;

import partie.partie.Partie;
import reseau.client.DialogueClient;

public class ControleClient implements Controlable {

	private boolean[] touches;
	//private String id;
	
	public ControleClient(String id)
	{
		//this.id = id;
		touches = new boolean[Controle.values().length / 2];
	}
	
	@Override
	public void action(Controle c, Partie p, float[] args) {		
				
		int index = c.getIndex();
		int offsetAppuye = Controle.values().length / 2;;
		boolean etat = index < offsetAppuye;
		index %= offsetAppuye;
		
		if (touches[index] != etat)
		{
			touches[index] = etat;
			
			DialogueClient.envoyerCommande(c, args);
		}
		
	}

}
