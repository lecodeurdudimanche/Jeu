package partie.controles;

import partie.partie.Partie;

public interface Controlable {

	public void action(Controle c, Partie p, float[] args);
}
