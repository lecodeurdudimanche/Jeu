package partie.controles;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import element.element.Element;
import partie.partie.Partie;
import partie.statistiques.ListeStatistiques;
import partie.statistiques.TypeStatistique;

public class ControleurReseau extends Controleur{

	private static final int ETAT_VIDE = 0, ETAT_APPUYE = 1, ETAT_RELACHE = 2;
	private int[] touches;
	private Object lockTouches;
	private List<float[]> arguments;
	
	public ControleurReseau(Controlable c) {
		super(c);
		
		touches = new int[Controle.values().length / 2];
		lockTouches = new Object();
		arguments = Collections.synchronizedList(new ArrayList<>());
	}

	@Override
	public void doActions(Partie p)
	{
		int nb = 0;
		synchronized(lockTouches)
		{
			nb = touches.length;
		}

		Controle[] controles = Controle.values();
		int offsetAppuye = controles.length / 2;
		for (int i = 0; i < nb; i++)
		{

			int etat;
			synchronized(lockTouches)
			{
				etat = touches[i];
			}
			float[] arg = null;
			//Traitement special pour le tir
			if (controles[i] == Controle.TIR && etat == ETAT_RELACHE)
			{
				if (arguments.size() > 0)
				{
					arg = arguments.get(0);
					arguments.remove(0);
				}
			}
			
			if (touches[i] == ETAT_APPUYE)
			{
				joueur.action(controles[i], p, arg);
				//Logger.println("Commande appliquée " + controles[i] + ", t = " + (System.nanoTime() / 1E6) % 4000, LogLevel.DEBUG);

			}
			else if (touches[i] == ETAT_RELACHE)
			{
				joueur.action(controles[i + offsetAppuye], p, arg);
				touches[i] = ETAT_VIDE;
			}
		}			
	}

	public void readCommand(DataInputStream inStream) throws IOException {
			
		int indiceCommande = inStream.readInt();

		//True puis argument s'il y a des arguments, false sinon
		float[] args = null;
		int nbArgs = inStream.readInt();
		if (nbArgs > 0)
		{
			args = new float[nbArgs];
			for (int i = 0; i < nbArgs; i++)
			{
				args[i] = inStream.readFloat();
				arguments.add(args);
			}
		}
		
		int offsetAppuye = Controle.values().length / 2;
		boolean etat = indiceCommande < offsetAppuye;
		indiceCommande %= offsetAppuye;
		synchronized(lockTouches)
		{
			touches[indiceCommande] = etat ? ETAT_APPUYE : ETAT_RELACHE;
		}		

		if (etat && joueur instanceof Element)
			ListeStatistiques.log(((Element) joueur).getId(), TypeStatistique.TOUCHES);
	}
}
