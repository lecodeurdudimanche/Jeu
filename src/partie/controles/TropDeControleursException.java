package partie.controles;

@SuppressWarnings("serial")
public class TropDeControleursException extends RuntimeException {

	public TropDeControleursException(String string) {
		super(string);
	}

}
