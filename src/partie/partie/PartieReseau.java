package partie.partie;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import config.Config;
import config.ConfigKey;
import element.collision.Forme;
import element.element.Element;
import element.element.Personnage;
import partie.configuration.ParametresPartie;
import partie.controles.Controlable;
import partie.controles.Controleur;
import processing.core.PApplet;
import reseau.client.DialogueClient;
import utils.Logger;
import utils.Utils;

public class PartieReseau implements Partie {
	
	private Partie partie;
	private List<Controleur> controleurs;
	
	public PartieReseau() {
		partie = DialogueClient.getPartie();
		controleurs = new ArrayList<>();
	}

	@Override
	public List<Personnage> getJoueurs() {
		return partie.getJoueurs();
	}

	@Override
	public void attacherControleur(Controleur c, int j) {
		//Les controleurs reseau sont deja attaches a leur cible
		if (j == controleurs.size())
			controleurs.add(c);
	}

	@Override
	public List<Controleur> getControleurs() {
		return controleurs;
	}

	@Override
	public boolean evoluer() {
		
		if (DialogueClient.communicationFinie())
			return false;
		
		for(Controleur c : controleurs)
			c.doActions(partie);
		
		partie = DialogueClient.getPartie();
		return partie.estFinie();
	}

	@Override
	public int getFPS(PApplet p) {
		return (int) DialogueClient.getNbPerSec();
	}

	@Override
	public void afficher(PApplet p) {
		partie.afficher(p);

		if (Config.readBoolean(ConfigKey.SHOW_FPS))
		{
			Utils.drawIndicator(p, p.width - 70, 30, (int)p.frameRate, 5, 30, "FPSR", -1);
			Utils.drawIndicator(p, p.width - 70, 50, DialogueClient.getPing(), 400, 30, "Ping", -1);
		}
	}

	@Override
	public Element checkCollision(Element e) {
		return partie.checkCollision(e);
	}
	@Override
	public Element checkCollision(Forme f) {
		return partie.checkCollision(f);
	}
	@Override
	public Element checkCollision(Forme f, Element e) {
		return partie.checkCollision(f, e);
	}
	
	@Override
	public boolean ajouterElement(Element e) {
		return partie.ajouterElement(e);
	}

	@Override
	public void setElementFollowed(long id) {
		partie.setElementFollowed(id);		
	}

	@Override
	public int ajouterJoueur() {
		throw new RuntimeException("Method not implemented ajouterJoueur()");
		// Normalement ne devrait pas etre invoquee
	}

	@Override
	public void supprimerJoueur(int i) {
		throw new RuntimeException("Method not implemented supprimerJoueur()");
		// Normalement ne devrait pas etre invoquee
	}

	@Override
	public Controleur getControleur(Controlable owner) {
		throw new RuntimeException("Method not implemented getControleur(Controlable)");
		// Normalement ne devrait pas etre invoquee
	}
	@Override
	public void close()
	{
		partie.close();
		try {
			DialogueClient.disconnect();
		} catch (IOException e) {
			Logger.printlnErr("Erreur lors de la déconnexion du serveur : " + e);
		}
	}

	@Override
	public ParametresPartie getParametres()
	{
		return partie.getParametres();
	}

	@Override
	public boolean estFinie() {
		return partie.estFinie() || DialogueClient.communicationFinie();
	}

	@Override
	public boolean aErreur() {
		return partie.aErreur() || DialogueClient.getErreur();
	}

	@Override
	public List<Element> getElements() {
		return partie.getElements();
	}


}
