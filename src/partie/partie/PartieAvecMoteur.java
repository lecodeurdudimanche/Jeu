package partie.partie;

import java.io.*;
import java.util.*;

import element.arme.*;
import element.buff.*;
import element.collision.*;
import element.element.*;
import partie.IA.IATouretteSimple;
import partie.IA.IATouretteTrack;
import partie.configuration.ConditionArret;
import partie.configuration.ParametresPartie;
import partie.configuration.Scroll;
import partie.controles.*;
import processing.core.PApplet;
import processing.core.PVector;
import reseau.client.AlgoListes;
import utils.LogLevel;
import utils.Logger;
import utils.Utils;

/**
 * Une partie avec un moteur de jeu mais sans affichage
 * 
 * @author adrien
 *
 */
public class PartieAvecMoteur implements Partie {

	protected List<Personnage> persos;
	protected List<Element> elements;
	/**
	 * Les controleurs ne doivent pas être envoyés : ils sont ajoutés avant/après
	 */
	private List<Controleur> controleurs;

	protected final static PVector GRAVITE = new PVector(0, 0.5f);
	private static int idJoueur = 0;

	private long last, lastGravityChange, lastMeteorite;
	private float coeffGravite;
	private int nbBlocs, nbCollectables;
	private ParametresPartie params;
	private Scroll scroll;
	private long idPersoFollowed;
	private boolean fini;

	public PartieAvecMoteur(PApplet fenetre, int nbPersos) {
		this(fenetre.width, fenetre.height, nbPersos);
		// this(fenetre.width * 3, fenetre.height * 2, nbPersos);
		// scroll = new Scroll(params.getW(), params.getH(), params.getW() / 3,
		// params.getH() / 2);
		// idPersoFollowed = persos.get(0).getId();
	}

	public PartieAvecMoteur(float wMap, float hMap, int nbPersos) {
		this(new ParametresPartie(wMap, hMap, nbPersos));
	}

	public PartieAvecMoteur(ParametresPartie p) {
		// Setup
		initialize();
		controleurs = new ArrayList<Controleur>();

		last = System.currentTimeMillis();

		parametrer(p);

		// On ajoute le sol et les murs
		float mapW = params.getW(), mapH = params.getH();
		ajouterElement(new Sol(mapW, mapH));
		ajouterElement(new Sol(mapW, 0));
		ajouterElement(new Mur(mapW, mapH - Sol.H - 1, 0));
		ajouterElement(new Mur(mapW, mapH - Sol.H - 1, mapW - 5));
	}

	// Que pour le reseau :
	public PartieAvecMoteur(DataInputStream inStream) throws IOException {
		initialize();
		lire(inStream);
	}

	public PartieAvecMoteur(PartieAvecMoteur partie) {
		initialize();
		params = partie.params.copy();
		scroll = partie.scroll.copy();
		elements = Utils.clone(partie.elements);
		persos = Utils.clone(partie.persos);
		idPersoFollowed = partie.idPersoFollowed;
		fini = partie.estFinie();
	}

	public void demarrer() {
		params.getConditionArret().demarrer(System.currentTimeMillis());
	}

	public void setParams(ParametresPartie p) {
		if (p != null)
			parametrer(p);
	}

	private void parametrer(ParametresPartie p) {
		params = p;
		
		// On cree NB_PERSOS joueurs
		while (persos.size() < params.getNbPersos())
			ajouterJoueur();

		//On enleve les potentiels persos en trop
		for (int i = persos.size() - 1; i >= params.getNbPersos(); i--)
			persos.remove(i);
		
		//On propage les changement de taille d'equipe aux joueurs existants
		for (Personnage perso : persos)
			perso.setEquipe((int) (perso.getId() % params.getTeamSize()));

		regenElements();

		scroll = new Scroll(params.getViewW(), params.getViewH(), params.getW(), params.getH());
	}

	private void initialize() {

		params = new ParametresPartie();
		scroll = new Scroll(0, 0, 0, 0);
		elements = new ArrayList<>();
		persos = new ArrayList<>();
		coeffGravite = .5f;
		nbBlocs = nbCollectables = 0;
		idPersoFollowed = -1;
		fini = false;
	}

	@Override
	public List<Personnage> getJoueurs() {
		return persos;
	}

	@Override
	public ParametresPartie getParametres() {
		return params.copy();
	}

	@Override
	public int ajouterJoueur() {
		int idJoueur = getNextIdJoueur();
		Personnage p = new Personnage(100 + 200 * persos.size(), 50, System.currentTimeMillis(), idJoueur, idJoueur % params.getTeamSize());
		persos.add(p);
		return persos.size() - 1;
	}

	private synchronized static int getNextIdJoueur() {
		return idJoueur++;
	}

	@Override
	public void supprimerJoueur(int i) {
		persos.remove(i);
		controleurs.remove(i);
	}
	
	@Override
	public List<Element> getElements()
	{
		return Collections.unmodifiableList(elements);
	}

	@Override
	public void afficher(PApplet p) {
		// On recupere l'element a suivre
		// TODO: stocker ça pour sauver du temps proc
		Element elem = persos.stream().filter(e -> e.getId() == idPersoFollowed).findAny().orElse(null);
		if (elem == null)
			elem = elements.stream().filter(e -> e.getId() == idPersoFollowed).findAny().orElse(null);

		scroll.update(elem);

		// On applique la transformation de la vue
		p.pushMatrix();
		p.scale(p.width / scroll.getW(), p.height / scroll.getH());
		p.translate(-scroll.getX(), -scroll.getY());

		// Pas de contours
		p.noStroke();

		// On affiche tous les elements
		for (Element e : elements)
			e.afficher(p);

		// Affichages persos
		p.textSize(14);
		for (int i = 0; i < persos.size(); i++) {
			Personnage perso = persos.get(i);
			perso.afficher(p);
		}

		p.popMatrix();

		// Affichage score
		// Fond score
		int x = 20, y = 20, w = 50, h = 15;
		p.fill(0);
		p.rectMode(PApplet.CORNER);
		p.rect(x - 5, y - h / 2 - 5, w + 10, h * persos.size() + 5);

		// Scores
		String[] couleurs = { "rouge", "verte", "bleue" };

		Map<Personnage, Integer> scores = calculerScores();
		int i = 0;
		p.textAlign(PApplet.LEFT);
		for (Map.Entry<Personnage, Integer> score : scores.entrySet()) {
			p.fill(score.getKey().getCouleurAffichage());
			String str = params.isTeams() ? "Équipe " + couleurs[score.getKey().getCouleur() % couleurs.length]
					: score.getKey().getNom();
			p.text(str + " : " + score.getValue(), x, y + i * h);
			i++;
		}

		// Affichage condition arret
		ConditionArret ca = params.getConditionArret();
		double scoreMax = ca.getScoreMax(),
				score = calculerScores().values().stream().max((e1, e2) -> e1 - e2).orElse(0);
		long tempsRestant = ca.getEnd() == -1 ? -1 : (ca.getEnd() - System.currentTimeMillis()) / 1000 + 1;

		int xText = p.width / 2 - 50, yText = 15;
		p.textSize(15);
		if (scoreMax != -1) {
			p.fill(0);
			p.rect(xText, yText, 100, 15);
			p.fill(255);
			p.text("Score " + score + " sur " + scoreMax, xText, yText);
			yText += 15;
		}
		if (tempsRestant != -1) {
			p.fill(0);
			p.rect(xText, yText, 100, 15);
			p.fill(255);
			p.text((tempsRestant / 60) + " : " + (tempsRestant % 60), xText, yText);
		}
		if (ca.estInfini()) {
			p.fill(0);
			p.rect(xText, yText, 100, 15);
			p.fill(255);
			p.text("Partie infinie", xText, yText);
		}

	}

	// Retourne une map avec un personnage par equipe et le score de l'equipe
	private Map<Personnage, Integer> calculerScores() {
		Map<Personnage, Integer> map = new TreeMap<>();
		Map<Integer, Personnage> mapTeams = new HashMap<>();

		boolean hasTeams = params.isTeams();
		for (Personnage p : persos) {
			if (hasTeams) {
				Personnage key;
				if (mapTeams.containsKey(p.getEquipe()))
					key = mapTeams.get(p.getEquipe());
				else {
					key = p;
					mapTeams.put(p.getEquipe(), p);
				}

				int val = p.getScore();

				if (map.containsKey(p))
					val += map.get(key);

				map.put(key, val);
			} else
				map.put(p, p.getScore());
		}

		return map;
	}

	@Override
	public boolean evoluer() {
		if (estFinie())
			return false;

		if (last == 0)
			last = System.currentTimeMillis();

		float mapW = params.getW(), mapH = params.getH();

		float coeff = (System.currentTimeMillis() - last);
		PVector gravite = PVector.mult(GRAVITE, coeff);
		last = System.currentTimeMillis();

		/*
		 * if (last - lastGravityChange > 300) { coeffGravite *= -1; lastGravityChange =
		 * last; }
		 * 
		 * gravite.mult(coeffGravite);
		 */

		// Meteorites
		if (last - lastMeteorite > Utils.random(5000, 10000)) {
			Balle proj = new Grenade(Utils.random(15, mapW - 15), 20, System.currentTimeMillis(), Utils.random(4, 8),
					(int) (Utils.random(0.5, 5) * 1000), null);

			proj.setVitesse(new PVector(0, Utils.random(0, 500)));
			ajouterElement(proj);
			lastMeteorite = last;
		}

		for (Controleur c : controleurs)
			c.doActions(this);

		// Elems : Ordre important pour le tir ! (projectiles puis personnages)
		for (int i = 0; i < elements.size(); i++) {
			Element e = elements.get(i);
			// On applique la gravite
			e.appliquerForce(gravite);
			// On update
			e.evoluer(System.currentTimeMillis(), this);

			if (e.getY() > mapH + 100)
				e.detruire();
		}

		// On fait evoluer les personnage
		for (int i = 0; i < persos.size(); i++) {
			Personnage perso = persos.get(i);
			perso.appliquerForce(gravite);

			// On update
			perso.evoluer(System.currentTimeMillis(), this);

			if (perso.getY() > mapH + 100)
				perso.detruire();

		}

		// On enleve les elements finis ou en dehors de l'ecran
		Iterator<Element> it = elements.iterator();
		while (it.hasNext()) {
			Element e = it.next();
			if (e.estDetruit() || e.getY() > mapH + 100) {
				if (e instanceof Bloc)
					nbBlocs--;
				else if (e instanceof ElementCollectable)
					nbCollectables--;

				it.remove();
			}
		}
		// On regenere les blocs et les items
		regenElements();

		// On agit sur les joueurs touches
		for (int i = 0; i < persos.size(); i++) {
			// Si le joueur est mort
			if (persos.get(i).estDetruit()) {
				// On prend des cordonnees aleatoires
				boolean valid;
				float x, y;
				do {
					valid = true;
					// Coordonnees aleatoire dans le terrain
					x = Utils.random(10 + Personnage.W, mapW - 10 - Personnage.W);
					y = Utils.random(10 + Personnage.H, mapH - 10 - Personnage.H);

					// On check qu'elles soient bien vides
					Rectangle pos = new Rectangle(new PVector(x, y), Personnage.W, Personnage.H);
					valid = checkCollision(pos) == null;

				} while (!valid);
				// On le place a ces coordonnees
				persos.get(i).reset(x, y);
			}
		}

		double scoreMax = calculerScores().values().stream().max((e1, e2) -> e1 - e2).orElse(0);
		int nbPersosVivants = (int) persos.stream().filter(e -> !e.estDetruit()).count();

		return !(fini = params.getConditionArret().doitArreter(scoreMax, nbPersosVivants, System.currentTimeMillis()));

	}

	public boolean ajouterElement(Element e) {
		if (e == null)
			return false;

		// L'element n'est pas en collision ou c'est un projectile en collision avec le
		// joueur
		Element eCollision = checkCollision(e);
		if (eCollision == null || (e instanceof Projectile && eCollision instanceof Personnage)) {
			if (e instanceof Bloc)
				nbBlocs++;
			else if (e instanceof ElementCollectable)
				nbCollectables++;

			elements.add(e);
			return true;
		}
		return false;
	}

	public Element checkCollision(Forme f, Element e) {
		if (f == null)
			return null;

		for (Element elem : elements) {
			if (e != elem && elem.collision(f))
				return elem;
		}
		for (Personnage p : persos) {
			if (e != p && p.collision(f))
				return p;
		}

		return null;
	}

	public Element checkCollision(Element e) {
		if (e == null)
			return null;

		return checkCollision(e.getForme(), e);
	}

	public Element checkCollision(Forme f) {
		if (f == null)
			return null;

		return checkCollision(f, null);
	}

	@Override
	public void setElementFollowed(long id) {
		idPersoFollowed = id;
	}

	@Override
	public void attacherControleur(Controleur c, int j) {
		if (j >= 0 && j < persos.size()) {
			c.attacher(persos.get(j));
			controleurs.add(c);
		}
	}

	public void setEquipeJoueur(int i, int team) {
		if (i >= 0 && i < persos.size())
			persos.get(i).setEquipe(team);

	}

	private void regenElements() {
		// On ajoute des blocs aleatoires
		// On elenve les 2 sols et les 2 murs
		while (nbBlocs - 4 < params.getNbBlocs()) {
			Plateforme b;
			do {
				float w = Utils.random(50, 100);
				float h = Utils.random(20, 100);
				float x = Utils.random(Personnage.W + Mur.W + 2, params.getW() - 5 - Personnage.W - w);
				float y = Utils.random(80, params.getH() - Sol.H - 5 - h);

				b = new Plateforme(x, y, w, h);
			} while (!ajouterElement(b));
		}

		
		while (nbCollectables < 2) {
			ElementCollectable c;
			do {
				float x = Utils.random(42, params.getW() - 42);
				float y = 20;
				if (Utils.random(0, 2) == 0) {
					Arme arme = ArmeAleatoire.getArme(null);
					c = new ArmeCollectable(x, y, System.currentTimeMillis(), arme);
				} else {
					Buff buff = Utils.random(new Buff[] { new BuffInvincibilite(3000), new BuffVitesse(5000, 100),
							new BuffDoubleSaut(15000) });
					c = new BuffCollectable(x, y, System.currentTimeMillis(), buff);
				}
			} while (!ajouterElement(c));
		}
		 

		/*while (nbCollectables < 2) {
			Element c;
			do {
				float x = Utils.random(42, params.getW() - 42);
				float y = 20;
				IATouretteSimple controleur = new IATouretteTrack(null);
				c = new Tourette(x, y, System.currentTimeMillis(), controleur);
				controleurs.add(controleur);
			} while (!ajouterElement(c));
			nbCollectables++;
		}*/
	}

	@Override
	public List<Controleur> getControleurs() {
		return controleurs;
	}

	@Override
	public Controleur getControleur(Controlable owner) {
		for (int i = 0; i < controleurs.size() && i < persos.size(); i++) {
			if (owner == persos.get(i))
				return controleurs.get(i);
		}
		return null;
	}

	public void ecrire(DataOutputStream stream, boolean init) throws IOException {
		// Init or not
		stream.writeBoolean(init);

		if (init) {
			// Paramètres de la partie
			params.ecrire(stream);
		}

		// Elements
		AlgoListes.envoiListeSansModif(stream, elements, init);

		// Persos
		AlgoListes.envoiListeSansModif(stream, persos, init);

		stream.writeBoolean(fini);

		stream.flush();
	}

	public void lire(DataInputStream stream) throws IOException {
		// Init or not
		boolean init = stream.readBoolean();

		if (init) {
			params.lire(stream);
			scroll.setW(params.getViewW());
			scroll.setH(params.getViewH());
			scroll.setTotalW(params.getW());
			scroll.setTotalH(params.getH());
		}
		// Projectiles
		Logger.println("Elements", LogLevel.NET_DEBUG);
		AlgoListes.receptionListeSansModif(stream, elements, e -> elements.add(e));

		// Persos
		Logger.println("Persos", LogLevel.NET_DEBUG);
		AlgoListes.receptionListeSansModif(stream, persos, e -> persos.add((Personnage) e));

		fini = stream.readBoolean();

		Logger.println("List sizes :  e = " + elements.size() + ", p =" + persos.size(), LogLevel.NET_DEBUG);
	}

	@Override
	public void close() {
		idJoueur = 0;
	}

	@Override
	public int getFPS(PApplet p) {
		return (int) p.frameRate;
	}

	public boolean equals(Object o) {
		if (o == null || !(o instanceof PartieAvecMoteur))
			return false;

		PartieAvecMoteur p = (PartieAvecMoteur) o;
		return elements.equals(p.elements) && persos.equals(p.persos);
	}

	@Override
	public boolean estFinie() {
		return fini;
	}

	@Override
	public boolean aErreur() {
		return false;
	}

}
