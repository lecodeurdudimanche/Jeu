package partie.partie;

import java.util.List;

import element.collision.Forme;
import element.element.Element;
import element.element.Personnage;
import partie.configuration.ParametresPartie;
import partie.controles.Controlable;
import partie.controles.Controleur;
import processing.core.PApplet;

public interface Partie{

	/**
	 * Permet d'accéder aux joueurs
	 * @return La liste de joueurs
	 */
	List<Personnage> getJoueurs();
	
	/**
	 * Permet d'attacher un controleur à un joueur
	 * Doit être Thread-Safe car Salle.connexion doit l'etre
	 * @param c Le controleur à attacher
	 * @param j L'indice du joueur auquel il faut l'attacher
	 */
	public void attacherControleur(Controleur c, int j);
	/**
	 * Permet d'avoir les controleurs pour leur donner des infos
	 * @return La liste des controleurs attachés aux joueurs
	 */
	public List<Controleur> getControleurs();
	public Controleur getControleur(Controlable owner);
	
	public List<Element> getElements();

	/**
	 * Permet de faire évoluer la Partie
	 * @return faux si la partie est finie
	 */
	public boolean evoluer();
	
	/**
	 * Permet d'afficher la Partie sur le fenetre p
	 * @param p La fenêtre Processing sur laquelle afficher la Partie
	 */
	public void afficher(PApplet p);
	
	/**
	 * Permet d'ajouter un element à la Partie
	 * @param e Element à ajouter
	 * @return vrai si l'élément a été ajouté, faux si la position de l'élément n'est pas libre
	 */
	public boolean ajouterElement(Element e);
	
	/**
	 * Permet de vérifier la collision d'un élément avec toutes les éléments du jeu (projectiles, blocs et personnages)
	 * @param e Elémént à tester
	 * @return le premier élément avec qui il collisionne, null si pas de collision
	 */
	public Element checkCollision(Element e);
	public Element checkCollision(Forme f);
	public Element checkCollision(Forme f, Element e);

	/**
	 * Permet de demander à ajouter un joueur à la partie
	 */
	public int ajouterJoueur();

	/**
	 * Permet de supprimer le joueur i
	 * @param i
	 */
	public void supprimerJoueur(int i);
	
	public void setElementFollowed(long idFollowed);

	public void close();

	public int getFPS(PApplet p);

	public ParametresPartie getParametres();

	public boolean estFinie();
	public boolean aErreur();




}
