package partie.IA;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import element.arme.Pistolet;
import element.collision.Forme;
import element.collision.Rectangle;
import element.element.ArmeCollectable;
import element.element.Balle;
import element.element.BuffCollectable;
import element.element.Element;
import element.element.Grenade;
import element.element.JoueurEquipe;
import element.element.Personnage;
import element.element.Projectile;
import partie.controles.Controle;
import partie.partie.Partie;
import processing.core.PApplet;
import processing.core.PVector;

public class IAJoueur extends IA{

	public IAJoueur(Personnage c) {
		super(c);
	}

	@Override
	public void doActions(Partie p) {
		if (joueur == null)
			return;
		
		Element perso = (Element) joueur;
		
		int minDistSq = 100 * 100;
		PVector target = null;
		
		List<Element> elems = p.getElements();
		List<Rectangle> boundingBoxes = new ArrayList<>();
		//Tolerance pour prendre en compte la tailel moyenne des objets et ne pas trop s'en approcher (un peu sale d'inclure la taille des objets)
		float tol = 10;
		//Fenetre de temps a prendre en compte : [t, t + tempsAvance]
		long tempsAvance = 100;

		PVector dir = new PVector(0, 0);
		float minDistProj = -1, minDistArme = -1, minDistBuff = -1;
		ArmeCollectable bestArme = null;
		BuffCollectable bestBuff = null;
		//Cleaner tout ca avec des streams
		for (Element e : elems)
		{
			float dist = e.distanceSq((Element) joueur);
			
			if (e instanceof BuffCollectable)
			{
				if (minDistBuff == -1 || minDistBuff > dist)
				{
					minDistBuff = dist;
					bestBuff = (BuffCollectable) e;
				}
			}
			else if (e instanceof ArmeCollectable)
			{
				if (minDistArme == -1 || minDistArme > dist)
				{
					minDistArme = dist;
					bestArme = (ArmeCollectable) e;
				}
			}
			else if (dist <= minDistSq)
			{
				if (e instanceof Balle)
				{
					Balle b = (Balle) e;
					if (perso instanceof JoueurEquipe && b.estMemeEquipe(p.getParametres(), (JoueurEquipe) perso))
						continue;
					
					if (e instanceof Grenade)
					{
						if (minDistProj == -1 || minDistProj > dist)
						{
							minDistProj = dist;
							dir =  new PVector(perso.getX() - e.getX(), perso.getY() - e.getY());
						}
					}
					else if (e instanceof Projectile) {
						if (b.estMemeEquipe(p.getParametres(), (JoueurEquipe) perso))
							continue;
						
						PVector pos = new PVector(e.getX(), e.getY()).sub(tol, tol);
						PVector taille = b.getVitesse().mult(tempsAvance).add(tol, tol);
						if (taille.x < 0) {
							pos.x += taille.x;
							taille.x = - taille.x + 2 * tol;
						}
						if (taille.y < 0) {
							pos.y += taille.y;
							taille.y = - taille.y + 2 * tol;
						}
						boundingBoxes.add(new Rectangle(pos, taille.x, taille.y));
		
						if (minDistProj == -1 || minDistProj > dist)
						{
							minDistProj = dist;
							dir =  new PVector(perso.getX() - e.getX(), 0);
						}
					}
				}
			}
			
		}
		
		for (Personnage j : p.getJoueurs())
		{
			if (j != joueur && !j.estMemeEquipe(p.getParametres(), (JoueurEquipe) perso) && (Math.abs(j.getX() - perso.getX()) < 350 && perso.getY() - j.getY() < 10))
				target = new PVector(j.getX(), j.getY());
		}
		

		float threshold = 0.1f; // ?
		
		//Prio au tir sur le perso
		if (target != null)
		{
			if (target.x > perso.getX())
				joueur.action(Controle.DROITE, p, null);
			else
				joueur.action(Controle.GAUCHE, p, null);

			joueur.action(Controle.SPRINT_RELACHE, p, null);
			joueur.action(Controle.TIR_RELACHE, p, new float[] {500});

			joueur.action(Controle.DROITE_RELACHE, p, null);
			joueur.action(Controle.GAUCHE_RELACHE, p, null);
		}
		float vitesse = 50;
		PVector directions[] = {
				new PVector(dir.x, 0),
				new PVector(dir.x, -vitesse),
				new PVector(-vitesse, 0),
				new PVector(vitesse, 0),
				new PVector(-vitesse, -vitesse),
				new PVector(vitesse, -vitesse),
		};
		
		for (PVector d : directions)
		{
			Forme posHypothetique = perso.getForme().getTranslation(d);
			if (boundingBoxes.stream().noneMatch(rect -> rect.collision(posHypothetique)))
			{
				dir = d;
				break;
			}
		}
		
		joueur.action(Controle.SPRINT, p, null);
		
		if (dir.magSq() == 0)
		{
			//Si une arme est assez proche et que c'est un pistolet on va la chercher
			if (bestArme != null && minDistArme < 1000 && bestArme.itemIs(i -> i != null && i instanceof Pistolet)) 
				dir = new PVector(bestArme.getX() - perso.getX(), bestArme.getY() - perso.getY());
			//Sinon, si un buff est assez proche on y va
			else if (bestBuff != null && minDistBuff < 250) //50
				dir = new PVector(bestBuff.getX() - perso.getX(), bestBuff.getY() - perso.getY());
			//Sinon on essaye de monter le plus haut possible
			else if (perso.getY() > 30)
				dir = new PVector(perso.getX() > p.getParametres().getW() / 2 ? vitesse : -vitesse, -vitesse);
			else 
				dir = new PVector(perso.getX() > p.getParametres().getW() / 2 ? -vitesse : vitesse, -vitesse);
		}

		if (dir.x > threshold)
		{
			joueur.action(Controle.DROITE, p, null);
			joueur.action(Controle.GAUCHE_RELACHE, p, null);
		}
		else if (dir.x < - threshold)
		{
			joueur.action(Controle.GAUCHE, p, null);
			joueur.action(Controle.DROITE_RELACHE, p, null);
		}
		else
		{
			joueur.action(Controle.DROITE_RELACHE, p, null);
			joueur.action(Controle.GAUCHE_RELACHE, p, null);
		}
		
		if (dir.y < - threshold)
		{
			joueur.action(Controle.SAUT, p, null);
			joueur.action(Controle.BAS_RELACHE, p, null);
		}
		else {
			joueur.action(Controle.SAUT_RELACHE, p, null);
		}
		
		
		
	}

}
