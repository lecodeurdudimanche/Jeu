package partie.IA;

import element.element.Personnage;
import element.element.Tourette;
import partie.controles.Controle;
import partie.partie.Partie;
import processing.core.PVector;

public class IATouretteTrack extends IATouretteSimple {

	private boolean moved;
	
	public IATouretteTrack(Tourette t) {
		super(t);
		moved = false;
	}


	@Override
	public void doActions(Partie p) {
		Tourette t = (Tourette) joueur;
		Personnage cible = getNearest(t, p);
		
		PVector dir = new PVector(t.getX() - cible.getX(), t.getY() - cible.getY());
		float goalAngle = (float) (Math.PI - dir.heading());
		
		if (Math.abs(goalAngle - t.getAngle()) < 1E-2f || (moved && t.estBloquee()))
		{
			t.action(Controle.TIR, p, new float[] {.4f});
			moved = false;
		}
		else {
			boolean droite = t.getAngle() > goalAngle;
			
			t.action(droite ? Controle.DROITE : Controle.GAUCHE, p, null);
			moved= true;
		}
		
	}


	private Personnage getNearest(Tourette t, Partie p) {
		Personnage nearest = null;
		float min = -1;
		for (Personnage per : p.getJoueurs())
		{
			float distSq = new PVector(per.getX() - t.getX(), per.getY() - t.getY()).magSq();
			if (min == -1 || min > distSq)
			{
				min = distSq;
				nearest = per;
			}
		}
		
		return nearest;
	}

}
