package partie.IA;

import element.element.Tourette;
import partie.controles.Controlable;
import partie.controles.Controle;
import partie.partie.Partie;

public class IATouretteSimple extends IA{

	protected int sens;
	
	public IATouretteSimple(Tourette t) {
		super(t);
		sens = 1;
	}
	
	@Override
	public void attacher(Controlable c)
	{
		if (c instanceof Tourette)
			super.attacher(c);
	}

	@Override
	public void doActions(Partie p) {
		Tourette t = (Tourette) joueur;
		
		if (t.estBloquee())
			sens *= -1; 
		
		t.action(Controle.TIR, p, new float[] {.2f});
		t.action(sens == -1 ? Controle.DROITE : Controle.GAUCHE, p, null);
	}

}
