package gui.main;

import processing.core.PApplet;

public interface SubApplet {
	
	public void setup(PApplet p);
	
	public default void fermer() {};

	public void draw();

	public default void keyPressed() {};

	public default void keyReleased() {};

	public default void mouseReleased() {};

	public default void mousePressed() {};

}
