package gui.main;
import java.util.ArrayList;
import java.util.List;

import config.Config;
import config.ConfigKey;
import element.element.Personnage;
import gui.menu.AffichageStats;
import gui.menu.Menu;
import partie.IA.IAJoueur;
import partie.configuration.ParametresPartie;
import partie.controles.ControleClient;
import partie.controles.ControleurClavier;
import partie.partie.Partie;
import partie.partie.PartieAvecMoteur;
import partie.partie.PartieReseau;
import partie.statistiques.ListeStatistiques;
import processing.core.PApplet;
import processing.core.PImage;
import reseau.client.DialogueClient;
import utils.LogLevel;
import utils.Logger;
import utils.Ressources;
import utils.Utils;

public class Jeu implements SubApplet{
	
	private Partie partie;
	
	private List<ControleurClavier> controleurs;
	
	private PApplet p;
	
	private PImage fond;
	
	private int nbBots;
	
	/**
	 * Construit une partie solo
	 * @param nbJoueurs nombre de joueurs
	 * @param nbBots nombre de bots controles par IA dans la partie
	 */
	public Jeu(PApplet p, int nbJoueurs, int nbBots)
	{
		this(new PartieAvecMoteur(p, nbJoueurs + nbBots), initControleurs(nbJoueurs), nbBots);
		((PartieAvecMoteur) partie).demarrer();
	}

	public Jeu()
	{
		this(new PartieReseau(), initControleursReseau(), 0);
		setScroll(DialogueClient.getIdJoueur());
	}
	
	private static List<ControleurClavier> initControleurs(int nbJoueurs) {
		List<ControleurClavier> controleurs = new ArrayList<>();
		for (int i = 0; i < nbJoueurs; i++)
		{
			ControleurClavier c = new ControleurClavier(null);
			controleurs.add(c);
		}
		return controleurs;		
	}
	
	private static List<ControleurClavier> initControleursReseau() {
		List<ControleurClavier> controleurs = new ArrayList<>();
		for (int i = 0; i < 1; i++)
		{
			ControleClient cli = new ControleClient(i + "");
			ControleurClavier c = new ControleurClavier(cli);
			controleurs.add(c);
		}
		return controleurs;	
	}

	public Jeu(Partie partie, List<ControleurClavier> controleurs, int nbBots)
	{
		this.partie = partie;
		this.nbBots = nbBots;
		this.controleurs = controleurs;
	}
	

	public void setScroll(long idFollowed) {

		partie.setElementFollowed(idFollowed);
	}
	
	public ParametresPartie getParametres()
	{
		return partie.getParametres();
	}
	
	public void setParametres(ParametresPartie params)
	{
		if (partie instanceof PartieAvecMoteur)
			((PartieAvecMoteur) partie).setParams(params);
	}
	
	public void setup(PApplet p)
	{
		//Setup
		this.p = p;
		
		//On affecte un controleur a chaque joueur
		for (int i = 0; i < controleurs.size(); i++)
			partie.attacherControleur(controleurs.get(i), i);

		//On affecte un controleur (une IA) a chaque bot
		for (int i = 0; i < nbBots; i++)
			partie.attacherControleur(new IAJoueur(null), controleurs.size() + i);

		
		p.rectMode(PApplet.CORNER);
		
		fond = Ressources.getImage("fondJeu");
	}

	//Appellee en boucle pour repaint, comme paintComponent (On peut arreter avec noLoop())
	public void draw() {
		//Fond noir
		p.background(0);
		//p.image(fond, 0, 0, p.width, p.height);
		
		partie.evoluer();
		
		if (partie.aErreur())
		{
			ListeStatistiques.reset();
			Principale.setRunning(new ErreurServer());
		}
		else if (partie.estFinie())
		{
			List<Personnage> joueurs = partie.getJoueurs();
			List<Long> ids = new ArrayList<>();
			List<String> noms = new ArrayList<>();
			for (Personnage p : joueurs)
			{
				ids.add(p.getId());
				noms.add(p.getNom());
			}
			AffichageStats stats = new AffichageStats(ids, noms);
			Principale.setRunning(stats);
		}			
		
		partie.afficher(p);
		

		if (Config.readBoolean(ConfigKey.SHOW_FPS))
		{
			p.textSize(14);
			Utils.drawIndicator(p, p.width - 70, 10, partie.getFPS(p), 8, 50, "FPS");
		}
	}

	public void keyPressed()
	{
		for (ControleurClavier c: controleurs)
			c.keyPressed(p.keyCode);
		
		if (p.key == PApplet.ESC)
			Principale.setRunning(new Menu());
		else if (p.key == 'f')
			Config.set(ConfigKey.SHOW_FPS, Config.readBoolean(ConfigKey.SHOW_FPS) ? "false" : "true");
	}

	public void keyReleased()
	{
		for (ControleurClavier c: controleurs)
			c.keyReleased(p.keyCode);
	}


	@Override
	public void fermer() {
		//Liberer les controleurs
		for (ControleurClavier c : controleurs)
			c.liberer();
		
		partie.close();
	}

}
