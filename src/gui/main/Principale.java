package gui.main;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import config.Config;
import config.ConfigKey;
import gui.menu.Menu;
import processing.core.PApplet;
import updater.Updater;
import utils.LogLevel;
import utils.Logger;
import utils.Ressources;

public class Principale extends PApplet{
	
	private static PApplet pAppletInstance;
	private static SubApplet running;
	
	public static void main(String[] args) {
	
		//Si on ne doit pas afficher en console, on redirige vers des fichiers
		if (! Config.readBoolean(ConfigKey.PRINT_TO_CONSOLE)) {
			
			//On ouvre les fichiers sortie.log et error.log, avec append à true pour conserver les logs
			try {
				Logger.setStdStream(new PrintStream(new FileOutputStream("sortie.log", true)));
				Logger.setErrStream(new PrintStream(new FileOutputStream("error.log", true)));
			} catch (FileNotFoundException e1) {
				Logger.printErr("Impossible de rediriger les logs dans les fichiers de log : " + e1);
			}
			
			//On met une petite info du début du programme dans les fichiers
			DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy à hh:mm:ss");
			Logger.println("Lancement du programme au " + formatter.format(new Date()), LogLevel.VOID);
			Logger.printlnErr("Lancement du programme au " + formatter.format(new Date()));
		}
		
		boolean checkForUpdates = Config.readBoolean(ConfigKey.UPDATE_CHECK);
		
		if (checkForUpdates) {
			Updater u;
			try {
				u = new Updater();
				if (u.needsUpdate())
					Updater.launchUpdate();
			} catch (IOException e) {
				Logger.printErr("Erreur à la mise à jour !");
			}
		}
		
		//Launch le main de processing
		PApplet.main(Principale.class.getCanonicalName());
	}
	
	public static void setRunning(SubApplet s)
	{
		if (running != null)
			running.fermer();
		
		running = s;
		s.setup(pAppletInstance);
	}
	
	
	public void settings()
	{
		if (Config.readBoolean(ConfigKey.FULLSCREEN))
		{
			if (Config.readBoolean(ConfigKey.WINDOWED_FULLSCREEN))
			{
				Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
				size((int)screen.getWidth(), (int)screen.getHeight());
			}
			else
				fullScreen(P2D, SPAN);
		}
		else
			size(640, 480);
	}
	
	public void setup()
	{
		Ressources.init(this);
		
		String imgPreload[] = {
			"bloc0", "bloc1", "bloc2", "bloc3", "bloc4", "bloc5"
		};
		for (String img : imgPreload)
		{
			try {
				Ressources.preload(img, "image");
			} catch (IOException e) {
				Logger.printErr("Erreur à l'ouverture de la ressource : " + e.getMessage());
				System.exit(1);
			}
		}
		
		pAppletInstance = this;
		setRunning(new Menu());
	}
	
	public void draw()
	{
		running.draw();
	}
	
	public void keyPressed()
	{
		running.keyPressed();
		key = 0; //Avoid ESC key to terminte program		
	}
	
	public void keyReleased()
	{
		running.keyReleased();
	}
	
	public void mousePressed()
	{
		running.mousePressed();
	}
	
	public void mouseReleased()
	{
		running.mouseReleased();
	}

}
