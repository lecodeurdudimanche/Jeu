/**
 * 
 */
package gui.main;

import gui.menu.Menu;
import processing.core.PApplet;
import processing.core.PImage;
import utils.Ressources;

/**
 * @author Mael
 *
 */
public class ErreurServer implements SubApplet{
	
	private PApplet p;
	private PImage erreur;
	
	public void setup(PApplet p) {
		erreur = Ressources.getImage("ErreurServer");
		this.p = p;
	}
	
	public void draw() {
		p.image(erreur, 0,0,p.width, p.height);
	}

	@Override
	public void keyPressed() {	
		if (p.keyCode == PApplet.ENTER)
		{
			Menu menu = new Menu();
			Principale.setRunning(menu);
		}
	}
	
	@Override
	public void fermer() {
		 erreur = null; 
	}
}
