package gui.menu;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import config.Config;
import config.ConfigKey;
import gui.main.Principale;
import gui.main.SubApplet;
import processing.core.PApplet;
import processing.core.PImage;
import reseau.client.DialogueClient;
import utils.Logger;
import utils.Ressources;

public class SelectionReseau implements SubApplet{
	
	private PImage fond;
	private PApplet p;
	private List<PButton> boutons;
	private List<String[]> infos;
	private Consumer<String[]> onAddress;
	private Consumer<Long> onStart;
	private PText ipText;
	private long debutRecherche, timeout;
	private boolean local;
	
	public SelectionReseau() {
		infos = new ArrayList<>();
		onAddress = a -> {
			Logger.println("Réponse d'un serveur : " + a[0]);
			infos.add(a);
		};
		onStart = debut -> debutRecherche = debut;
		debutRecherche = -1;
		timeout = Config.readInt(ConfigKey.DISCOVERY_TIMEOUT);
	}

	@Override
	public void setup(PApplet p) {
		this.p = p;
		boutons = new ArrayList<>();
		
		int w = 200, h = 40;
		int x = p.width / 2 - w /2, y = p.height / 2 - 2 * h;
		
		String[] labels = { "Serveur local", "Serveur en ligne", "IP directe", "Retour"};
		for (int i = 0; i < labels.length; i++)
			boutons.add(new PButton(x + (i == 2 ? w : 0), y + 2 * i * h, w, h, labels[i]));
		
		ipText = new PText(x - w, y + 2 * 2 * h, w * 2, h);
		ipText.setPlaceholder("Adresse ou nom du serveur");
		
		fond = Ressources.getImage("Serveur");
		p.rectMode(PApplet.CORNER);
	}


	@Override
	public void draw() {
		p.background(0);
		p.image(fond, 0, 0);
		
		
		float percentage = debutRecherche == -1 ? 0 : (100f * (System.currentTimeMillis() - debutRecherche)) / timeout;
		if (percentage > 0)
		{
			p.rectMode(PApplet.CORNER);
					
			int w = p.width / 4, h = 10;
			//Draw
			int x = p.width / 2 - w / 2, y = p.height / 2 - h / 2;
			p.stroke(255);
			p.noFill();
			p.rect(x, y, w, h);
			p.fill(255);
			p.rect(x, y, w * percentage / 100, h);
			
			if (percentage >= 100)
			{
				Principale.setRunning(new SelectionServeur(infos, local));
			}
		}
		else 
		{
			for(PButton b : boutons) 
				b.afficher(p);
			
			ipText.afficher(p);
		}
	}
	
	@Override
	public void fermer() {
		DialogueClient.terminerScan();
	}
	
	@Override
	public void keyPressed()
	{
		if (p.key == PApplet.ESC)
			Principale.setRunning(new Menu());
		else
			ipText.keyStroke(p.key, p.keyCode);
	}
	
	@Override
	public void mousePressed() {
		if (debutRecherche != -1)
			return;
		
		for(PButton b : boutons)
		{
			if (b.contient(p.mouseX, p.mouseY))
			{
				try {
					switch(b.getLabel())
					{
						case "Serveur local":
							DialogueClient.scannerReseauLocal(onAddress, onStart);
							local = true;
							break;
						case "Serveur en ligne":
							DialogueClient.scannerServeursEnLigne(onAddress, onStart);
							local= false;
							break;
						case "IP directe":
							String ip = ipText.getLabel();
							try {
								InetAddress.getByName(ip);
								DialogueClient.connecterServeur(ip,  Config.readString(ConfigKey.PSEUDO));
								Principale.setRunning(new SelectionSalle());
							} catch(IOException e)
							{
								ipText.setInvalidFor(3000);
								Logger.printlnErr(e.toString());
							}
							break;
						case "Retour":
							Principale.setRunning(new Menu());
							break;
					}
				} catch (IOException e) {
					Logger.printlnErr("Erreur réseau : " + e.getMessage());
				}
			}
		}
		
		ipText.click(p.mouseX, p.mouseY);
	}

}
