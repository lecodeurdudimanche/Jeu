package gui.menu;

import java.util.ArrayList;

import config.Config;
import config.ConfigKey;
import gui.main.Principale;
import partie.controles.ControleurClavier;
import processing.core.PApplet;

public class SettingsControles extends Settings {
	
	@Override
	public void setup(PApplet p) {
		super.setup(p);
		
		champs = new ArrayList<>();
		
		loadCorrespondancesNomCle();
		
		int i = 0;
		int w = p.width, h = 30;
		ConfigKey[] clefs = {
				ConfigKey.J1_SAUT, ConfigKey.J1_GAUCHE, ConfigKey.J1_BAS, ConfigKey.J1_DROITE, ConfigKey.J1_SPRINT, ConfigKey.J1_TIR,
				ConfigKey.J2_SAUT, ConfigKey.J2_GAUCHE, ConfigKey.J2_BAS, ConfigKey.J2_DROITE, ConfigKey.J2_SPRINT, ConfigKey.J2_TIR,
				ConfigKey.J3_SAUT, ConfigKey.J3_GAUCHE, ConfigKey.J3_BAS, ConfigKey.J3_DROITE, ConfigKey.J3_SPRINT, ConfigKey.J3_TIR,
		};
		for (ConfigKey c : clefs)
		{
			int x = 0, y = 30 + i * 40;
			String value = Config.readString(c);
			PField champ = new PField(x, y, w, h, cleToNom.get(c), "Key", value);
			champs.add(champ);
			i++;
		}
		
		boutonControles = new PButton(0, 0, 1, 1, "");
	}
	
	@Override
	public void keyPressed() {
		if (p.key == PApplet.ESC)
			Principale.setRunning(new Settings());
		else
			super.keyPressed();
	}
	
	@Override
	public void mousePressed() {

		if (retour.contient(p.mouseX, p.mouseY))
			Principale.setRunning(new Settings());
		else 
			super.mousePressed();
	}
	
	@Override
	protected void sauvegarder()
	{
		super.sauvegarder();
		ControleurClavier.readDefaultConfigs();
	}
}
