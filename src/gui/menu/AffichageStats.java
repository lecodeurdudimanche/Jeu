package gui.menu;

import java.util.ArrayList;
import java.util.List;

import gui.main.Principale;
import gui.main.SubApplet;
import partie.statistiques.ListeStatistiques;
import partie.statistiques.TypeStatistique;
import processing.core.PApplet;

public class AffichageStats implements SubApplet {

	private List<PLabel> stats;
	private PButton retour;
	private PApplet p;
	private List<Long> ids;
	private List<String> noms;
	
	public AffichageStats(List<Long> ids, List<String> noms)
	{
		this.ids = ids;
		this.noms = noms;
		stats = new ArrayList<>();
	}
	
	
	@Override
	public void setup(PApplet p) {
		this.p = p;
		
		String[] labels = {
			//"Meilleur score",
			"XxxKilleRxxX",
			"El sniperino",
			"Smasher",
			"Rambo",
			//"Le camper",
			"Victime",
			"Schizophrène",
			"Stuffé"
		};
		String[] unites = {/*"points", */"kills", "précision de tir", "touches appuyées", "tirs faits", "morts", "kills contre son camp", "items récupérés"};
		TypeStatistique[] types = {
				//null,
				TypeStatistique.KILLS,
				null,
				TypeStatistique.TOUCHES,
				TypeStatistique.TIRS,
				TypeStatistique.MORTS,
				TypeStatistique.FRIENDLY_FIRE,
				TypeStatistique.ITEMS
		};
		
		int x = 100, y = 20, w = p.width - 200, h = 40;
		
		for (int i = 0; i < labels.length; i++, y += h)
		{
			double max = -1;
			String nom = "";
			for (int j = 0; j < ids.size(); j++)
			{
				double val = -1;
				if (types[i] != null)
					val = ListeStatistiques.get(ids.get(j), types[i]).getSomme();
				else {
					if (labels[i].equals("El sniperino"))
						val = ListeStatistiques.get(ids.get(j), TypeStatistique.TIRS).getSomme() == 0 ? 0 : ListeStatistiques.get(ids.get(j), TypeStatistique.KILLS).getSomme() / ListeStatistiques.get(ids.get(j), TypeStatistique.TIRS).getSomme();
				}
								
				if (val > max)
				{
					max = val;
					nom = noms.get(j);
				}
			}
			
			String str = max > 0 ? " : " + nom + " (" + max + " " + unites[i] +")" : " [Aucune valeur]";
			PLabel label = new PLabel(x, y, w, h, labels[i] + str);
			stats.add(label);
		}
		
		retour = new PButton(p.width / 2 - 200, p.height - 40, 400, 40, "Retour à l'accueil");
	}

	@Override
	public void draw() {
		p.background(0);
		
		for (PLabel l : stats)
			l.afficher(p);
		
		retour.afficher(p);
	}
	
	@Override
	public void keyPressed()
	{
		if (p.key == PApplet.ESC)
			retour();
	}
	
	@Override
	public void mousePressed()
	{
		if (retour.contient(p.mouseX, p.mouseY))
			retour();
	}
	
	private void retour()
	{
		Principale.setRunning(new Menu());
	}
	
	@Override
	public void fermer()
	{
		ListeStatistiques.reset();
	}
}
