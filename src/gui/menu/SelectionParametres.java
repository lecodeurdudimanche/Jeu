package gui.menu;

import java.util.ArrayList;
import java.util.List;

import gui.main.SubApplet;
import partie.configuration.ParametresPartie;
import processing.core.PApplet;

public class SelectionParametres implements SubApplet{

	private PApplet p;
	private ParametresPartie params;
	private boolean enableTeams;
	private List<PField> champs;
	
	public SelectionParametres(ParametresPartie params, boolean teams)
	{
		this.params = params;
		enableTeams = teams;
		
		champs = new ArrayList<>();
	}
	
	@Override
	public void setup(PApplet p) {
		this.p = p;
		
		String[] noms = {"Largeur totale", "Hauteur totale", "Largeur de l'écran", "Hauteur de l'écran", "Tir ami", "Equipes", "Taille des équipes", "Nombre de blocs"};
		String[] types = {"Integer", "Integer", "Integer", "Integer", "Boolean", "Boolean", "Integer", "Integer"};
		String[] val = {"1366", "768", "1366", "768", "False", "False", "2", "20"};
		
		int w = p.width - 20, h = 30;
		int x = 10, y = 30;
		for (int i = 0; i < noms.length; i++)
		{
			PField f = new PField(x, y, w, h, noms[i], types[i], val[i]);
			y += h + 10;

			if ((i != 6 && i != 7) || enableTeams)
				champs.add(f);
		}
			
	}

	@Override
	public void draw() {
		p.background(0);
		
		for (PField f : champs)
			f.afficher(p);
	}
	
	@Override
	public void mousePressed() {
		for (PField f : champs)
			f.click(p.mouseX, p.mouseY);
	}
	
	@Override
	public void keyPressed() {
		for (PField f : champs)
			f.keyStroke(p.key, p.keyCode);
	}
}
