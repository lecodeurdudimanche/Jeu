package gui.menu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gui.main.ErreurServer;
import gui.main.Principale;
import gui.main.SubApplet;
import processing.core.PApplet;
import processing.core.PImage;
import reseau.client.DialogueClient;
import reseau.server.Salle;
import utils.LogLevel;
import utils.Logger;
import utils.Ressources;
import utils.Utils;

public class SelectionSalle implements SubApplet{
	
	private final static int NB_SALLES_X = 3, NB_SALLES_Y = 2, NB_SALLES_PAGE = NB_SALLES_X * NB_SALLES_Y;
	
	private PImage fond, cadre, cadreProtegee, cadreAjout, cadreAjoutSelect;
	private PApplet p;
	private List<PButton> boutons;
	private List<PText> champs;
	private Map<PButton, PText> assoChamps;
	private PButton prev, retour, next, refresh;
	private long last;
	private int page, nbPagesMax;
	List<Salle> salles;

	public SelectionSalle(){
		
		boutons = new ArrayList<>();
		champs = new ArrayList<>();
		assoChamps = new HashMap<>();
		
		salles = new ArrayList<>();
	}

	@Override
	public void setup(PApplet p) {
		this.p = p;
		fond = Ressources.getImage("Serveur");
		cadre = Ressources.getImage("cadre");
		cadreProtegee = Ressources.getImage("cadreProtegee");
		cadreAjout = Ressources.getImage("cadreAjout");
		cadreAjoutSelect = Ressources.getImage("cadreAjoutSelect");
		
		prev = new PButton(60, p.height - 30, 120, 30, "Précédent");
		next = new PButton(p.width - 120, p.height - 30, 120, 30, "Suivant");
		retour = new PButton(p.width / 2 + 90, p.height - 30, 120, 30, "Retour");
		refresh = new PButton(p.width / 2 - 90, p.height - 30, 120, 30, "Actualiser");

		page = 0;
		nbPagesMax = 0;
		actualiserSalles();
	}

	@Override
	public void draw() {

		p.noTint();
		p.background(0);
		p.image(fond, 0, 0, p.width, p.height);
		
		for(int i = page * NB_SALLES_PAGE; i < (page + 1) * NB_SALLES_PAGE && i <= salles.size(); i++) {
			PButton b = boutons.get(i);
			b.afficher(p);
			PText c = assoChamps.get(b);
			if (c != null)
				c.afficher(p);
		}
		
		prev.afficher(p);
		next.afficher(p);
		retour.afficher(p);
		refresh.afficher(p);
		
		p.fill(255);
		p.noStroke();
		p.text((page + 1) + " / " + nbPagesMax, p.width / 2 + 55, p.height - 25);
	
		long t = System.currentTimeMillis(); 
		if (t - last >= 20000)
			actualiserSalles();
	}
	
	private void actualiserSalles(){		
		int nb;
		page = 0;
		
		try {

			Logger.println("Envoi de la demande des salles...", LogLevel.NET_DEBUG);

			salles.clear();
			DialogueClient.recupererSalles(s -> salles.add(s));
			nb = salles.size();
			Logger.println("Nombre de salles : " + nb, LogLevel.NET_DEBUG);

			Logger.println("Salles ajoutées !", LogLevel.NET_DEBUG);

		} catch (IOException e) {
			Logger.printlnErr("Erreur réseau : " + e.getMessage() + " (" + e + ")");
			Principale.setRunning(new ErreurServer());
			return;
		}
		
		boutons.clear();
		champs.clear();
		assoChamps.clear();
		
		//Taille d'une case (largeur - hauteur)
		float wCase = p.width / NB_SALLES_X, hCase = (p.height - 30) / NB_SALLES_Y;
		//Padding a l'interieur d'une case (largeur - hauteur)
		float wPadding = 20, hPadding = 20;
		
		for (int i = 0; i <= salles.size(); i++)
		{
			float x = wCase * ((i % NB_SALLES_PAGE) % NB_SALLES_X) + wPadding / 2;
			float y = hCase * ((i % NB_SALLES_PAGE) / NB_SALLES_X) + hPadding / 2;
			float w = wCase - wPadding;
			float h = hCase - hPadding;
			
			String str;
			PImage img, hover = null;
			Salle s = null;
			
			if (i == salles.size())
			{
				str = "";
				img = cadreAjout;
				hover = cadreAjoutSelect;
			}
			else
			{
				s = salles.get(i);
				str = s.getNom() + "\n" + s.getNbClients() + " / " + s.getMaxClients();
				if (s.estProtege())
					img = cadreProtegee;
				else
					img = cadre;
			}
			PButton bouton = new PButton(x, y, w, h, str, img, hover);
			boutons.add(bouton);

			if (s != null && s.estProtege())
			{
				PText t = new PText(x, y, w, h + hCase, "");
				t.setPlaceholder("Mot de passe");
				champs.add(t);
				assoChamps.put(bouton, t);
			}
		}

		last = System.currentTimeMillis();
		nbPagesMax = (salles.size() + 1) / 6 + 1;
	}
	
	@Override
	public void keyPressed() {
		if (p.key == PApplet.ESC)
			retour();
		else if (p.key == PApplet.ENTER || p.key == PApplet.RETURN)
		{
			int i = -1;
			for (int j = 0; j < boutons.size(); j++)
			{
				PText t = assoChamps.get(boutons.get(j));
				if (t != null && t.isSelected())
					i = j;
			}
			
			if (i != -1)
				connexion(i);
		}
		else {
			
			for (PText c : champs)
				c.keyStroke(p.key, p.keyCode);
		}
	}
	
	@Override
	public void mousePressed() {
		if (refresh.contient(p.mouseX, p.mouseY))
		{
			actualiserSalles();
		}
		else if (retour.contient(p.mouseX, p.mouseY))
		{
			retour();
		}
		else if (prev.contient(p.mouseX, p.mouseY))
		{
			if (page > 0)
				page--;
		}
		else if (next.contient(p.mouseX, p.mouseY))
		{
			if (page < nbPagesMax - 1)
				page++;
		}
		
		for (PText c : champs)
			c.click(p.mouseX, p.mouseY);		
		
		for (int i = page * NB_SALLES_PAGE; i < (page + 1) * NB_SALLES_PAGE && i < boutons.size(); i++)
		{
			PButton b = boutons.get(i);
			b.setColor(Utils.color(255));
			if (b.contient(p.mouseX, p.mouseY))
			{
				if (b.getLabel().isEmpty()) //Ajout
				{
					Principale.setRunning(new CreationSalle());
				}
				else
					connexion(i);
			}
		}
		
	}

	private void retour() {
		Principale.setRunning(new SelectionReseau());
	}

	private void connexion(int i) {
		boolean res = false;
		
		PButton b = boutons.get(i);
		Salle s = salles.get(i);
		SubApplet app = null;

		Logger.println("Connexion à la salle... ");
		try {
			if (assoChamps.containsKey(b))
			{
				String mdp = assoChamps.get(b).getLabel();
				res = DialogueClient.connecterSalle(s.getNom(), mdp);
			}
			else
			{
				res = DialogueClient.connecterSalle(s.getNom());
			}
		} catch (IOException e) {
			app = new ErreurServer();
		}

		if (! res)
		{
			Logger.println("Cannot connect to " + b.getLabel(), LogLevel.WARNING);
			b.setInvalidFor(3000);
		}
		else
		{
			app = new Lobby(false);
		}
		
		if (app != null)
			Principale.setRunning(app);
	}

}
