package gui.menu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import gui.main.ErreurServer;
import gui.main.Jeu;
import gui.main.Principale;
import gui.main.SubApplet;
import processing.core.PApplet;
import reseau.client.DialogueClient;
import reseau.server.Client;
import reseau.server.Salle;
import utils.Logger;
import utils.Utils;

public class Lobby implements SubApplet {

	private boolean isOwner;
	private int equipeActuelle;
	private List<Client> clients;
	private List<PLabel> labelsClients, equipes;
	private List<PButton> supprClients;
	private PButton start, actualiser, quitter, equipe;
	private PApplet p;
	private PLabel titre, capacite,labelClients;
	private long lastActu;
	
	public Lobby(boolean isOwner)
	{
		this.isOwner = isOwner;
		labelsClients = new ArrayList<>();
		supprClients = new ArrayList<>();
		clients = new ArrayList<>();
		equipes = new ArrayList<>();
		equipeActuelle = -1;
	}
	
	@Override
	public void setup(PApplet p) {
		this.p = p;
		
		actualiser = new PButton(p.width / 2 - 100, p.height - 60, 200, 30, "Actualiser");
		if (isOwner)
			start = new PButton(p.width / 2 - 100, p.height - 30, 200, 30, "Démarrer");
		else
			quitter = new PButton(p.width / 2 - 100, p.height - 30, 200, 30, "Retour");
		actualiserLobby();
	}

	@Override
	public void draw() {
		p.background(0);
		
		titre.afficher(p);
		capacite.afficher(p);
		labelClients.afficher(p);
		
		for (PLabel l : labelsClients)
			l.afficher(p);
		for (PButton but : supprClients)
			but.afficher(p);
			
		if (isOwner)
			start.afficher(p);
		else
			quitter.afficher(p);
		
		if (equipe != null)
			equipe.afficher(p);
		
		for (PLabel e : equipes)
			e.afficher(p);
		
		actualiser.afficher(p);
		
		if (System.currentTimeMillis() - lastActu > 500)
			actualiserLobby();
	}
	
	@Override
	public void mousePressed()
	{
		if (isOwner && start.contient(p.mouseX, p.mouseY))
		{
			try {
				DialogueClient.lancerPartie();
			} catch (IOException e) {
				start.setInvalidFor(3000);
				Logger.printlnErr("Erreur au lancement de la partie : " + e);
			}
		}
		else if (!isOwner && quitter.contient(p.mouseX, p.mouseY))
		{
			retour();
		}
		else if (actualiser.contient(p.mouseX, p.mouseY))
		{
			actualiserLobby();
		}
		else if (equipe != null && equipe.contient(p.mouseX, p.mouseY))
		{
			try {
				equipeActuelle ++;
				equipeActuelle %= 3;
				DialogueClient.setEquipe(equipeActuelle);
				equipe.setLabel("Equipe " + (equipeActuelle + 1));
			} catch (IOException e) {
				Logger.printlnErr("Erreur lors de la définition de l'équipe");
				equipe.setInvalidFor(500);
			}
		}
		else {
			for (int i = 0; i < supprClients.size(); i++)
			{
				if (supprClients.get(i).contient(p.mouseX, p.mouseY))
				{
					supprimerClient(clients.get(i + 1));
				}
			}
		}
		
	}


	private void supprimerClient(Client client) {
		if (isOwner)
		{
			try {
				DialogueClient.kick(client.getPseudo());
			} catch (IOException e) {
				Logger.printlnErr("Impossible d'exclure le client " + client.getPseudo());
			}
		}
	}

	private void actualiserLobby() {
		labelsClients.clear();
		supprClients.clear();
		equipes.clear();
		
		Salle s;
		try {
			s = DialogueClient.recupererLobby();
		} catch (IOException e) {
			Logger.printlnErr("Erreur à la récupération de la partie : " + e);
			Principale.setRunning(new ErreurServer());
			return;
		}
		
		//La salle envoyee est nulle : on est deconnecte de la salle
		if (s == null)
		{
			Principale.setRunning(new SelectionSalle());
			return;
		}
		
		if (s.estEnJeu())
		{
			boolean err = false;
			try {
				DialogueClient.initPartie();
			} catch (IOException e) {
				Logger.printlnErr("Erreur au lancement de la partie : " + e);
				err = true;
			}
			
			if (err)
				Principale.setRunning(new ErreurServer());
			else
				Principale.setRunning(new Jeu());
			return;
		}
				
		clients = s.getClients();
		
		int x = p.width / 2 - 250, y = 100;
		int w = 300, h = 30;

		titre = new PLabel(x, 10, w, 50, s.getNom());
		titre.textSize(24);

		String strCapacite = "Nombre de joueurs : " + clients.size() + "/" + s.getMaxClients();
		capacite = new PLabel(x, 60, w, h, strCapacite);
		
		labelClients = new PLabel(x, y, w, h, "Joueurs");
		y += h;
		
		for (int i = 0; i < clients.size(); i++, y += h)
		{
			Client c = clients.get(i);
			String str = (i + 1) + ". " + c.getPseudo();
			PLabel l = new PLabel(x, y, w, h, str);
			labelsClients.add(l);
			
			boolean estClient = c.getClientId().equals(DialogueClient.getIdClient());
			
			int e = (c.getTeam() == -1 ? i : c.getTeam());
			String nomEquipe = "Equipe " + (e + 1);
			
			if (estClient)
			{
				equipeActuelle = e;
				equipe = new PButton(x + w, y, 90, 30, nomEquipe);
			}
			else
			{
				equipes.add(new PLabel(x + w, y, 90, 30, nomEquipe));
			}
			
			if (!estClient && isOwner)
			{
				PButton b = new PButton(x + w + 90, y, 20, 30, "x");
				b.setColor(Utils.color(255, 0, 0));
				b.textSize(20);
				supprClients.add(b);
			}
				
		}
		
		
		lastActu = System.currentTimeMillis();		
	}
	
	@Override 
	public void keyPressed()
	{
		if (!isOwner && p.key == PApplet.ESC)
			retour();
	}
	
	private void retour()
	{
		try {
			DialogueClient.deconnexionSalle();
		} catch (IOException e) {
			Logger.printlnErr("Erreur à la déconnexion de la salle : " + e);
		}
		Principale.setRunning(new SelectionSalle());
	}
	
	

}
