package gui.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import config.Config;
import config.ConfigKey;
import gui.main.Principale;
import gui.main.SubApplet;
import processing.core.PApplet;
import utils.LogLevel;
import utils.Logger;

public class Settings implements SubApplet {

	protected PApplet p;
	protected List<PField> champs;
	protected Map<ConfigKey, String> cleToNom, types;
	protected Map<String, ConfigKey> nomToCle;
	protected PButton retour, boutonControles;
	protected PField selected;
	
	@Override
	public void setup(PApplet p) {
		this.p = p;
		
		champs = new ArrayList<>();
		types = new HashMap<>();
		cleToNom = new HashMap<>();
		nomToCle = new HashMap<>();
		
		loadTypes();
		loadCorrespondancesNomCle();
		
		retour = new PButton(p.width / 2 - 50, p.height - 30, 100, 30, "Retour");
		boutonControles = new PButton(p.width / 2 - 200, p.height - 100, 400, 30, "Configurer les touches");
		
		int i = 0;
		int w = p.width, h = 30;
		ConfigKey[] clefs = {ConfigKey.FULLSCREEN, ConfigKey.WINDOWED_FULLSCREEN, ConfigKey.FRIENDLY_FIRE, ConfigKey.TEAMS,ConfigKey.SHOW_FPS, ConfigKey.UPDATE_CHECK, ConfigKey.PSEUDO, ConfigKey.SERVER_LIST, ConfigKey.DISCOVERY_TIMEOUT, ConfigKey.SERVER_TIMEOUT, ConfigKey.LOG_LEVEL};
		for (ConfigKey c : clefs)
		{
			int x = 0, y = 30 + i * 40;
			String type = types.get(c), value = Config.readString(c);
			PField champ = new PField(x, y, w, h, cleToNom.get(c), type, value);
			champs.add(champ);
			i++;
		}
	}
	
	protected void loadCorrespondancesNomCle()
	{
		
		for (ConfigKey c : ConfigKey.values())
		{
			cleToNom.put(c, c.toString());
			nomToCle.put(c.toString(), c);
		}
	}
	
	protected void loadTypes()
	{
		types.put(ConfigKey.SERVER_LIST, "String");
		types.put(ConfigKey.DISCOVERY_TIMEOUT, "Integer");
		types.put(ConfigKey.FULLSCREEN, "Boolean");
		types.put(ConfigKey.WINDOWED_FULLSCREEN, "Boolean");
		types.put(ConfigKey.FRIENDLY_FIRE, "Boolean");
		types.put(ConfigKey.TEAMS, "Boolean");
		types.put(ConfigKey.SHOW_FPS, "Boolean");
		types.put(ConfigKey.LOG_LEVEL, "Integer");
		types.put(ConfigKey.PSEUDO, "String");
		types.put(ConfigKey.SERVER_TIMEOUT, "Integer");
		types.put(ConfigKey.UPDATE_CHECK, "Boolean");
	}

	@Override
	public void draw() {
		p.background(0);
		
		for (PField f : champs)
			f.afficher(p);
		
		retour.afficher(p);
		boutonControles.afficher(p);
	}
	
	@Override
	public void keyPressed() {
		if (p.key == PApplet.ENTER)
			sauvegarder();
		else if (p.key == PApplet.ESC)
			Principale.setRunning(new Menu());
		else {
			for (PField f : champs)
				f.keyStroke(p.key, p.keyCode);
		}
	}
	
	@Override
	public void mousePressed() {

		if (retour.contient(p.mouseX, p.mouseY))
			Principale.setRunning(new Menu());
		else if (boutonControles.contient(p.mouseX, p.mouseY))
			Principale.setRunning(new SettingsControles());
		else {
			PField ancienSelected = selected;
			selected = null;
			for(PField c : champs)
			{
				if (c.click(p.mouseX, p.mouseY))
					selected = c;
			}
			if (selected != ancienSelected && ancienSelected != null)
				sauvegarder();
		}
	}

	protected void sauvegarder() {
		for (int i = 0; i < champs.size(); i++)
		{
			ConfigKey cle = nomToCle.get(champs.get(i).getLabel());
			String value = champs.get(i).getStringValue();
			if (champs.get(i) != selected)
				champs.get(i).setSelected(false);
						
			if (!Config.readString(cle).toLowerCase().equals(value))
			{
				if (!Config.set(cle, value))
				{
					champs.get(i).setInvalidFor(3000);
					champs.get(i).setStringValue(Config.readString(cle));
				}
				else
					champs.get(i).setValid();
			}
			
		}
		Logger.setLogLevel(LogLevel.fromLevel(Config.readInt(ConfigKey.LOG_LEVEL)));
	}

	@Override
	public void fermer()
	{
		sauvegarder();
	}

}
