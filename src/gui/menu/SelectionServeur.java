package gui.menu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import config.Config;
import config.ConfigKey;
import gui.main.ErreurServer;
import gui.main.Principale;
import gui.main.SubApplet;
import processing.core.PApplet;
import processing.core.PImage;
import reseau.client.DialogueClient;
import utils.Logger;
import utils.Ressources;
import utils.Utils;

public class SelectionServeur implements SubApplet{
	
	private List<PLabel> boutons;
	private PApplet p;
	private PImage fond;
	private List<String[]> infosServeur;
	private PButton retour, refresh;
	private String pseudo;
	private boolean local;

	public SelectionServeur(List<String[]> infosServeur, boolean local) {
		boutons = new ArrayList<>();
		this.infosServeur = infosServeur;
		pseudo = Config.readString(ConfigKey.PSEUDO);
		this.local = local;
	}

	@Override
	public void setup(PApplet p) {
		this.p = p;
		fond = Ressources.getImage("Serveur");
		
		refreshBoutons();
		
		retour = new PButton(p.width / 2 - 80, p.height - 40, 100, 40, "Retour");
		refresh = new PButton(p.width / 2 + 80, p.height - 40, 100, 40, "Actualiser");
	}

	@Override
	public void draw() {
		p.background(0);
		p.image(fond, 0, 0);
		
		for(PLabel b : boutons) {
			b.afficher(p);
		}
		
		if (boutons.size() == 0)
		{
			p.fill(255);
			p.textSize(20);
			p.rectMode(PApplet.CENTER);
			p.text("Pas de serveur trouvé", p.width/2, p.height/2);
			p.rectMode(PApplet.CORNER);
		}
		
		retour.afficher(p);
		refresh.afficher(p);
		
	}


	private void refreshBoutons() {
		boutons.clear();
		
		int wPad = p.width / 4, hPad = 20;
		int w = p.width / 2, h = 40;
		int nb = (p.width - wPad * 2) / w;
		for(int i = 0; i < infosServeur.size(); i++)
		{
			int x = wPad + (i % nb) * (w + wPad), y = hPad + (i / nb) * (h + hPad);
			
			String[] infos = infosServeur.get(i);
			String ip = infos[0], ver = infos[1], ping = infos[2];
			
			boolean correct = ! ver.equals(DialogueClient.VERSION_TROP_ANCIENNE) && !ver.equals(DialogueClient.VERSION_TROP_RECENTE);
			String str = "Serveur " + (i + 1) + " (" + ip + ", version " + ver + ", ping " + ping + " ms)";
			
			PLabel l = correct ? new PButton(x, y + 2 * i * h, w, h, str) : new PLabel(x, y + 2 * i * h, w, h, str);
			
			if (!correct)
				l.setColor(Utils.color(150));
			
			l.textSize(18);
			boutons.add(l);
		}	
	}
	
	@Override
	public void keyPressed()
	{
		if (p.key == PApplet.ESC)
			retour();
	}
	
	@Override
	public void mousePressed(){
		for (int i = 0; i < boutons.size(); i++)
		{
			PLabel l = boutons.get(i);
			if (l instanceof PButton)
			{
				PButton b = (PButton) l;
				if (b.contient(p.mouseX, p.mouseY))
				{
					connecterServeur(infosServeur.get(i)[0]);
				}
			}
		}
		
		if (retour.contient(p.mouseX, p.mouseY))
			retour();
		else if (refresh.contient(p.mouseX, p.mouseY))
		{
			infosServeur.clear();
			Consumer<String[]> onAddr = infos -> { synchronized(infosServeur){infosServeur.add(infos); refreshBoutons();}};
			Consumer<Long> onStart = l -> {};
			
			try {
				if (local)
					DialogueClient.scannerReseauLocal(onAddr, onStart);
				else
					DialogueClient.scannerServeursEnLigne(onAddr, onStart);
			} catch (IOException e) {
				refresh.setInvalidFor(3000);
				Logger.printlnErr("Erreur lors du scan du serveur : " + e);
			}
			
		}
	}

	private void retour() {
		Principale.setRunning(new SelectionReseau());
	}

	private void connecterServeur(String adresse) {

		SubApplet s;
		try {
			DialogueClient.connecterServeur(adresse, pseudo);
			s = new SelectionSalle();
		} catch (IOException e) {
			Logger.printlnErr("Erreur à la connexion au serveur : " + e.getMessage());
			s = new ErreurServer();
		}

		Principale.setRunning(s);
		
	}

}
