package gui.menu;

import config.Config;
import gui.main.Jeu;
import gui.main.Principale;
import gui.main.SubApplet;
import partie.configuration.ParametresPartie;
import processing.core.PApplet;
import processing.core.PImage;
import utils.Ressources;

/**
 * @author Mael
 *
 */
public class Menu implements SubApplet{
			
	private static PImage menu;
	private PButton[] boutons;
	
	private PApplet p;

	@Override
	public void setup(PApplet p) {

		this.p = p;
	
		//image de fond
		menu = Ressources.getImage("Menu1");
		//icone setting
		PImage setting = Ressources.getImage("setting");
		//icone setting quand la souris est dessus
		PImage onSetting = Ressources.getImage("OnSetting");

		//pas de contour
		p.noStroke();

		float coeffX = p.width / 1366.0f, coeffY = p.height / 768.0f;

		boutons = new PButton[]{

				new PButton(105 * coeffX, 353 * coeffY, 350 * coeffX, 95 * coeffY, "2 joueurs"),
				new PButton(450 * coeffX, 617 * coeffY, 415 * coeffX, 95 * coeffY, "Online"),
				new PButton(905 * coeffX, 410 * coeffY, 360 * coeffX, 95 * coeffY, "2 VS 2 IA"),
				new PButton(p.width - 100 * coeffX, p.height - 100 * coeffY, 75 * coeffX, 75 * coeffY, "", setting, onSetting),

		};
		
		for (PButton b : boutons)
			b.textSize(60 * coeffX);
	}

	@Override
	public void draw() {
		p.noTint();
		p.resetMatrix();
		
		p.image(menu, 0, 0, p.width, p.height);

		for (PButton b : boutons)
			b.afficher(p);
	}

	public void faireAction(String a)
	{
		SubApplet sub = null;
		switch(a)
		{
		case "2 joueurs":
			sub = new Jeu(p, 1, 1);
			break;
		case "Online":
			sub = new SelectionReseau();
			break;
		case "2 VS 2 IA":
			Jeu jeu = new Jeu(p, 2, 2);
			ParametresPartie params = jeu.getParametres();
			params.setTeamSize(2);
			params.setTeams(true);
			jeu.setParametres(params);
			sub = jeu;
			break;
		case "":
			sub = new Settings();
			break;
		}
		
		if (sub != null)
		{
			Principale.setRunning(sub);
		}
	}


	@Override
	public void mousePressed() {

		for (PButton b : boutons)
		{
			if (b.contient(p.mouseX, p.mouseY))
				faireAction(b.getLabel());
		}
	}
	
	@Override
	public void keyPressed()
	{
		if (p.key == PApplet.ESC)
			System.exit(0);
	}
	

}
