package gui.menu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import config.Config;
import config.ConfigKey;
import gui.main.Principale;
import gui.main.SubApplet;
import processing.core.PApplet;
import reseau.client.DialogueClient;
import utils.LogLevel;
import utils.Logger;
import utils.Utils;

public class CreationSalle implements SubApplet {
	
	private List<PField> champs;
	
	private PButton ok, retour;
	private PApplet p;
	private String nom, clef;
	private int nb, w, h;

	public CreationSalle() {
		champs = new ArrayList<>();
	}

	@Override
	public void setup(PApplet p) {
		
		String[] noms = {"Nom de la salle", "Mot de passe", "Nombre de joueurs", "Largeur", "Hauteur"};
		String[] types = {"String", "String", "Integer", "Integer", "Integer"};
		String[] val = {"Partie de " + Config.readString(ConfigKey.PSEUDO), "", "3", "1366", "768"};
		
		for (int i = 0; i < noms.length; i++)
		{
			int w = 200, h = 40;
			int x = p.width / 2 - w, y = p.height / 2 - (h + 10) * noms.length / 2 + (h + 10) * i;
			
			PField champ = new PField(x, y, w * 2, h, noms[i], types[i], val[i]);
			
			champs.add(champ);			
		}
		
		ok = new PButton(p.width / 2 - 50, p.height - 75, 100, 50, "Créer");
		retour = new PButton(p.width / 2 - 50, p.height - 30, 100, 50, "Retour"); 
		this.p = p;
	}

	@Override
	public void draw() {
		p.background(0);
				
		for (PComponent c : champs)
			c.afficher(p);
		
		ok.afficher(p);
		retour.afficher(p);
	}
	
	@Override
	public void keyPressed() {
		if (p.key == PApplet.ESC)
			retour();
		else {
			for (PField f : champs)
			{
				f.keyStroke(p.key, p.keyCode);
			}
			
		}
	}
	
	private void retour() {
		Principale.setRunning(new SelectionSalle());
	}

	@Override
	public void mousePressed() {
		ok.setColor(Utils.color(255));
		
		for(PField f : champs)
			f.click(p.mouseX, p.mouseY);
		
		
		if (ok.contient(p.mouseX, p.mouseY))
		{
			actualiserChamps();

			if (nom != null && nb != 0 && w != 0 && h != 0
					&& DialogueClient.creerSalle(nom, clef, nb, w, h))
			{
				
				try {
					if (!DialogueClient.connecterSalle(nom, clef))
					{
						Logger.printlnErr("Erreur à la connexion de la salle créée");
						Principale.setRunning(new SelectionSalle());
					}
					else
					{
						Principale.setRunning(new Lobby(true));
					}
				} catch (IOException e) {
					Logger.printlnErr("Erreur de communication avec le serveur lors de la connexion à la salle créée");
				}
			}
			else
			{
				Logger.println("Cannot create salle", LogLevel.WARNING);
				ok.setColor(Utils.color(255, 0, 0));
			}
		}
		else if (retour.contient(p.mouseX, p.mouseY))
			retour();
	}

	private void actualiserChamps() {
		nom = readString(0);
		clef = readString(1);
		nb = readInt(2);
		w = readInt(3);
		h = readInt(4);
	}

	private int readInt(int i) {
		try {
			return (Integer) champs.get(i).getValue();
		} catch (ClassCastException | NullPointerException e) {
		}
		return 0;
	}

	private String readString(int i) {
		String s = champs.get(i).getStringValue();
		return s.isEmpty() ? null : s;
	}

}
