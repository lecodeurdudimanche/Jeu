package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import partie.statistiques.Statistique;

class StatistiqueTest {
	
	Statistique stats, stats2;
	
	@BeforeEach
	void init() {
		stats = new Statistique();
		stats2 = new Statistique();
		
		stats.ajouter(1);
		stats.ajouter(- 80);
		stats.ajouter(4);
		stats.ajouter(2.5);
		stats.ajouter(2.5);
		stats.ajouter(12);
		stats.ajouter(22);
		stats.ajouter(2.5);
		stats.ajouter(- 8);
		stats.ajouter(- 7);
		stats.ajouter(4);
		stats.ajouter(79);
		stats.ajouter(7.5);
		stats.ajouter(3.66);
		stats.ajouter(2);
		stats.ajouter(0);
		stats.ajouter(8);
		
		//Nb 17, max 79, min -80, somme 55.66, moyenne 3,274117647
		
		stats2.ajouter(10);
		stats2.ajouter(-5);
		stats2.ajouter(7);
		stats2.ajouter(14);
		
		//Nb 4, max 14, min -5, moyenne 6.5, somme 26
	}

	@Test
	void testReset() {
		stats.reset();
		
		assertEquals(0, stats.getNb());
		assertEquals(0, stats.getMoyenne());
		assertEquals(0, stats.getSomme());
		assertTrue(Double.isNaN(stats.getMin()));
		assertTrue(Double.isNaN(stats.getMax()));
	}

	@Test
	void testGetMax() {
		assertEquals(79, stats.getMax(), 1E-6);
		assertEquals(14, stats2.getMax(), 1E-6);
		
	}

	@Test
	void testGetMin() {
		assertEquals(-80, stats.getMin(), 1E-6);
		assertEquals(-5, stats2.getMin(), 1E-6);
	}

	@Test
	void testGetMoyenne() {
		assertEquals(3.274117647 , stats.getMoyenne(), 1E-8);
		assertEquals(6.5, stats2.getMoyenne(), 1E-6);
	}

	@Test
	void testGetSomme() {
		assertEquals(55.66, stats.getSomme(), 1E-6);
		assertEquals(26, stats2.getSomme(), 1E-6);
	}

	@Test
	void testGetNb() {
		assertEquals(17, stats.getNb());
		assertEquals(4, stats2.getNb());
	}

}
