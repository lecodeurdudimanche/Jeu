package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import element.element.Bloc;
import element.element.Element;
import reseau.client.AlgoListes;

class AlgoListesTest {

	@Test
	void testAlgo()
	{
		List<Element> elems = new ArrayList<Element>();
		for (int i = 0; i < 8; i++)
		{
			//Pour faire des trous dans les ids
			if (Math.random() > .4)
				new Bloc(1,1,1,1);
			
			elems.add(new Bloc(i, i, i, i));
		}
		
		List<Element> copie = new ArrayList<Element>();
		
		copie(elems, copie);
		assertEquals(elems, copie);

		elems.remove(7);
		elems.remove(5);
		elems.remove(4);
		elems.remove(0);
		elems.add(new Bloc(8, 8, 8, 8));
		elems.add(new Bloc(9, 9, 9, 9));

		copie(elems, copie);
		assertEquals(elems, copie);

		elems.remove(3);
		elems.remove(2);
		elems.remove(1);

		copie(elems, copie);
		assertEquals(elems, copie);
		
		elems.remove(1);
		elems.remove(1);

		copie(elems, copie);
		assertEquals(elems, copie);

		elems.remove(0);

		copie(elems, copie);
		assertEquals(elems, copie);
		

		elems.add(new Bloc(10, 10, 10, 10));
		elems.add(new Bloc(11, 11, 11, 11));
		elems.remove(0);

		copie(elems, copie);
		assertEquals(elems, copie);
		
	}
	
	void copie(List<Element> elems, List<Element> copie) {
		List<Integer> s = new ArrayList<Integer>();
		for(int i = 0, j = 0; i < elems.size(); i++, j++)
		{
			long id = elems.get(i).getId();
			j = AlgoListes.insert(id, j, copie, s);
			if (j < copie.size()) 
				copie.set(j, elems.get(i));
			else
				copie.add(elems.get(i));
		}
		for (int i = s.size() - 1; i >=0; i--)
		{
			copie.remove((int)s.get(i));
		}
		for (int i = copie.size() - 1; i >= elems.size(); i--)
			copie.remove(i);
	}


}
