package test;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class CustomTesting {

	public static void main(String[] args) throws IOException {
		Thread t = new Thread() {
			@Override
			public void run() {
				try(Socket s = new Socket(InetAddress.getByName("::1"), 9000)){
				
					//No read
					while(true)
					{
						System.out.println("Client alive");
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							e.printStackTrace();
							break;
						}
					}
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		
		
		ServerSocket socket = new ServerSocket(9000);
		
		t.start();
		
		Socket s = socket.accept();
		socket.close();
		
		
		BufferedOutputStream st = new BufferedOutputStream(s.getOutputStream());
		long debut = System.currentTimeMillis();
		long nbBytes = 0;
		for (int i = 0; i < 1000000; i++)
		{
			System.out.println(i + "\t" + (System.currentTimeMillis() - debut) + "\t" + (nbBytes / (1024*1024)) + " MiB " + ((nbBytes % 1024 * 1024) / 1024) + " KiB " + (nbBytes % 1024) + " bytes");
			st.write("Hello world !".getBytes()); 
			nbBytes += "Hello world !".getBytes().length;
			st.flush();
		}
		
		t.interrupt();
		System.out.println("Main exiting");
	}

}
