package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import partie.statistiques.ListeStatistiques;
import partie.statistiques.Statistique;
import partie.statistiques.TypeStatistique;

class ListeStatistiquesTest {
	
	@BeforeAll
	static void init() {
		ListeStatistiques.log(1, TypeStatistique.TOUCHES, 5);
		
		ListeStatistiques.log(1, TypeStatistique.DEGATS, 10);
		ListeStatistiques.log(1, TypeStatistique.DEGATS, 0);
		ListeStatistiques.log(1, TypeStatistique.DEGATS, 17);

		ListeStatistiques.log(1, TypeStatistique.KILLS, 4);
		ListeStatistiques.log(1, TypeStatistique.KILLS, 8);
		ListeStatistiques.log(1, TypeStatistique.KILLS, 12);
		
		ListeStatistiques.log(1, TypeStatistique.TOUCHES, -5);

		ListeStatistiques.log(2, TypeStatistique.DEGATS, 7);
		ListeStatistiques.log(2, TypeStatistique.DEGATS, 6);
		ListeStatistiques.log(2, TypeStatistique.DEGATS, 5);

		ListeStatistiques.log(3, TypeStatistique.TIRS, 9);
		ListeStatistiques.log(3, TypeStatistique.TIRS, 13);
		ListeStatistiques.log(3, TypeStatistique.TIRS, 11);

		ListeStatistiques.log(2, TypeStatistique.KILLS, 10);
		ListeStatistiques.log(2, TypeStatistique.KILLS, 8);
	}
	
	@Test
	void testLog() {
		Statistique s = ListeStatistiques.get(1, TypeStatistique.DEGATS);
		assertEquals(17, s.getMax());
		assertEquals(0, s.getMin());
		assertEquals(9, s.getMoyenne());
		assertEquals(3, s.getNb());
		assertEquals(27, s.getSomme());
		
		s = ListeStatistiques.get(1, TypeStatistique.KILLS);
		assertEquals(12, s.getMax());
		assertEquals(4, s.getMin());
		assertEquals(8, s.getMoyenne());
		assertEquals(3, s.getNb());
		assertEquals(24, s.getSomme());
		
		s = ListeStatistiques.get(1, TypeStatistique.TOUCHES);
		assertEquals(5, s.getMax());
		assertEquals(-5, s.getMin());
		assertEquals(0, s.getMoyenne());
		assertEquals(2, s.getNb());
		assertEquals(0, s.getSomme());

		s = ListeStatistiques.get(2, TypeStatistique.DEGATS);
		assertEquals(7, s.getMax());
		assertEquals(5, s.getMin());
		assertEquals(6, s.getMoyenne());
		assertEquals(3, s.getNb());
		assertEquals(18, s.getSomme());

		s = ListeStatistiques.get(2, TypeStatistique.KILLS);
		assertEquals(10, s.getMax());
		assertEquals(8, s.getMin());
		assertEquals(9, s.getMoyenne());
		assertEquals(2, s.getNb());
		assertEquals(18, s.getSomme());

		s = ListeStatistiques.get(3, TypeStatistique.TIRS);
		assertEquals(13, s.getMax());
		assertEquals(9, s.getMin());
		assertEquals(11, s.getMoyenne());
		assertEquals(3, s.getNb());
		assertEquals(33, s.getSomme());
	}

	@Test
	void testReset() {
		ListeStatistiques.reset();
		
		assertEquals(0, ListeStatistiques.get(1, TypeStatistique.DEGATS).getNb());
		assertEquals(0, ListeStatistiques.get(1, TypeStatistique.KILLS).getNb());
		assertEquals(0, ListeStatistiques.get(1, TypeStatistique.TOUCHES).getNb());
		assertEquals(0, ListeStatistiques.get(2, TypeStatistique.DEGATS).getNb());
		assertEquals(0, ListeStatistiques.get(2, TypeStatistique.KILLS).getNb());
		assertEquals(0, ListeStatistiques.get(3, TypeStatistique.TIRS).getNb());
	}

}
