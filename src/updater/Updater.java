package updater;

import java.awt.BorderLayout;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.function.Consumer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import config.Config;
import utils.LogLevel;
import utils.Logger;

public class Updater {

	private final static String DEFAULT_VERSION_URI = "https://bitbucket.org/duvalbon1u/release-jeu/raw/master/version.txt";
	private final static String DEFAULT_GAME_URI = "https://bitbucket.org/duvalbon1u/release-jeu/get/master.zip";
	private static final String VERSION_FILE = "version.txt";
	private static final String GAME_DIR_FILE = ".";
	private static final String SCRIPT_FILE = "exec";
	
	private URL urlVersion, urlGame;
	private File endScript;
	private String version, releaseVersion, args[];
	
	public Updater() throws MalformedURLException
	{
		this(new URL(DEFAULT_VERSION_URI), new URL(DEFAULT_GAME_URI));
	}
		
	public Updater(URL version, URL game)
	{
		urlVersion = version;
		urlGame = game;
		this.version = getVersion();
		args = null;
	}

	public String getVersion() {
		if (version == null)
		{
			try (BufferedReader reader = new BufferedReader(new FileReader(GAME_DIR_FILE + File.separator + VERSION_FILE)))
			{
				version = reader.readLine();
			} catch (IOException e) {}
		}
		return version;
	}

	
	public boolean needsUpdate() throws IOException
	{
		HttpURLConnection con = (HttpURLConnection) urlVersion.openConnection();
		con.setDefaultUseCaches(false);
		con.setUseCaches(false);
		con.connect();
		if (con.getResponseCode() != 200)
			throw new IOException("Le serveur a répondu avec un code d'erreur : " + con.getResponseMessage());
		
		String currentVersion;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream())))
		{
			releaseVersion = reader.readLine();
		}
		
		currentVersion = getVersion();
		
		Logger.println("Fecthed version : " + releaseVersion + ", running version : " + currentVersion, LogLevel.DEBUG);
	
		int cur, serv;
		try {
			cur = Integer.parseInt(currentVersion);
		} catch (NumberFormatException e) {
			cur = -1;
		}
		try {
			serv = Integer.parseInt(releaseVersion);
		} catch (NumberFormatException e) {
			serv = -1;
		}
		return cur < serv;
	}
	
	static Consumer<File> recursiveLambda = null;
	
	public void update(Consumer<Long> progress, Consumer<Long> size) throws IOException
	{
		//On etablit la connexion avec le serveur
		HttpURLConnection con = (HttpURLConnection) urlGame.openConnection();
		con.setDefaultUseCaches(false);
		con.setUseCaches(false);
		con.connect();
		if (con.getResponseCode() != 200)
			throw new IOException("Le serveur a répondu avec un code d'erreur : " + con.getResponseMessage());
		
		//Inform of download size
		if (size != null)
			size.accept(con.getContentLengthLong());
		
		//Create an unique dir in temp folder
		File updateDir = Files.createTempDirectory(new File("..").toPath(), "smash").toFile();
		
		try(ZipInputStream inStream = new ZipInputStream(new BufferedInputStream(con.getInputStream())))
		{
			byte[] buffer = new byte[2048];
			ZipEntry entry;
			long writed = 0;
			while((entry = inStream.getNextEntry()) != null)
			{
				File file = new File(updateDir, entry.getName()).getCanonicalFile();
				if (entry.isDirectory())
				{
					boolean ok = file.mkdir();
					if (!ok)
						throw new IOException("Impossible de créer le répertoire " + file);
				}
				else
				{
					try(OutputStream outStream = new BufferedOutputStream(new FileOutputStream(file)))
					{
						int length;
						while ((length = inStream.read(buffer)) != -1)
						{
							outStream.write(buffer, 0, length);
							if (progress != null)
								progress.accept(writed += length);
						}
					}
				}
			}
		}
		
		//On cherche le dossier contenant le jeu dans le telechargement(duvalbon1u-release-jeu-[...].zip)
		//Il devrait être enfant unique du dossier principal
		File gameBaseDir = updateDir;
		while (gameBaseDir != null && !gameBaseDir.getName().startsWith("duvalbon1u"))
		{
			File[] children = updateDir.listFiles();
			gameBaseDir = children.length != 0 ? children[0] : null;
		}
		//Si on ne l'a pas trouvé, tant pis, on prend la racine du téléchargement comme dossier principal
		if (gameBaseDir == null)
			gameBaseDir = updateDir;
				
		//Repertoire du jeu actuel
		File gameDir = new File(GAME_DIR_FILE).getCanonicalFile();		
		
		Consumer<File> loadConfigs = recursiveLambda = f ->{
			if (f.isDirectory())
			{
				for (File child : f.listFiles())
					recursiveLambda.accept(child);
			}
			else if (f.getName().toLowerCase().equals(Config.CONFIG_FILE))
			{
				try {
					Config.loadFile(f.getCanonicalPath());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		//Preserver l'ancienne config, en ajoutant les clefs non présentes dans l'ancienne config (on ecrase avec les valeurs de l'ancienne configs)
		loadConfigs.accept(gameBaseDir);
		loadConfigs.accept(gameDir);
	
		if (!updateDir.equals(gameBaseDir)) {
			//On bouge le sous dossier de l'update contenant le jeu vers le dossier racine du jeu (contenant updater.jar, update et readme.txt)
			for (File f : gameBaseDir.listFiles()) {
				f.renameTo(new File(updateDir, f.getName()));
			}
			gameBaseDir.delete();
		}
		
		//On sort les exec dans le dossier racine
		for (File f : updateDir.listFiles())
		{
			if (f.getName().startsWith("exec."))
				f.renameTo(new File(gameDir.getParentFile(), f.getName()));
		}


		//On sauvegarde la configuration dans le config.ini
		Config.saveConfig(updateDir.getCanonicalPath() + File.separator + Config.CONFIG_FILE);
		
		/*
		 * On finit avec:
		 * /----|
		 * 		|- jeu
		 * 		|- exec.bat/sh
		 * 		|- smash-321random123/
		 */
			
		//On adapte le nom de script a executer à l'OS
		String scriptName = SCRIPT_FILE;	
		if (System.getProperty("os.name").toLowerCase().startsWith("windows"))
			scriptName += ".bat";
		else
			scriptName += ".sh";
		
		
		//On sauvegarde le nom du script
		endScript = new File(gameDir.getParentFile(), scriptName);
		args = new String[] {updateDir.getCanonicalPath(), gameDir.getCanonicalPath()};
		
		version = releaseVersion;
	}
	
	public void callEndScript() throws IOException {
		if (endScript.exists())
		{
			if (! endScript.canExecute())
				endScript.setExecutable(true);

			String path = endScript.getCanonicalPath();
			Logger.println("Started script : " + path + " with args: " + args[0] + ", " + args[1]);
			new ProcessBuilder(path, args[0], args[1]).redirectError(new File("err.log")).redirectOutput(new File("out.log")).start();
		}		
	}
	
	public void callEndScriptAndExit() throws IOException
	{
		callEndScript();
		Logger.println("Exiting program...");
		System.exit(0);
	}
	
	public static void launchUpdate()
	{
		if (GraphicsEnvironment.isHeadless())
			launchConsoleUpdate();
		else 
			launchGUIUpdate();
	}
	
	public static void launchGUIUpdate()
	{
		JFrame fenetre = new JFrame("Mise à jour de Smash Ball");
		boolean[] upToDate = {false};
		
		fenetre.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		fenetre.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e)
			{	
				if (upToDate[0])
					fenetre.dispose();
				
				int c = JOptionPane.showConfirmDialog(fenetre, "Êtes-vous sûr d'annuler la mise à jour ?", "Confirmation de fermeture", JOptionPane.YES_NO_OPTION);
				if (c == JOptionPane.YES_OPTION || c == JOptionPane.OK_OPTION)
					fenetre.dispose();
			}
		});
		
		fenetre.setContentPane(new JPanel(new BorderLayout(5, 5)));
		
		JLabel label = new JLabel();
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		
		fenetre.add(label, BorderLayout.NORTH);
		fenetre.add(progressBar, BorderLayout.CENTER);
		
		label.setText("Vérification de la version...");
		
		fenetre.pack();
		fenetre.setVisible(true);
		
		try {
			Updater u = new Updater();
			upToDate[0] = ! u.needsUpdate();
			if (! upToDate[0])
			{
				label.setText("Mise à jour...");
				progressBar.setMinimum(0);
				
				long debut = System.currentTimeMillis();
				
				Consumer<Long> onProgress = downloaded -> {
					
					progressBar.setValue((int)(downloaded / 1000));
					long temps = (System.currentTimeMillis() - debut);
					long taille = progressBar.getMaximum() * 1000;
					double speed = downloaded / temps;
					
					label.setText("Mise à jour : " + formatBytes(downloaded) + " sur " + formatBytes(taille) + " (" + formatBytes(speed * 1000) + "/s)");

					fenetre.pack();
				};
				Consumer<Long> onLengthReceived = length -> {
					progressBar.setMaximum((int)(length / 1000));
					progressBar.setIndeterminate(false);
				};
				
				u.update(onProgress, onLengthReceived);
			}
			label.setText("Mise à jour terminée !");
			
			JPanel panel = new JPanel(new GridLayout(2, 0));
			panel.add(new JLabel("Votre installation est à jour ! Version " + (u.getVersion() == null ? "inconnue" : u.getVersion())));
			JButton butOk = new JButton("Ok");
			panel.add(butOk);
			butOk.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						if (! upToDate[0])
							u.callEndScriptAndExit();
						else
							fenetre.dispose();
					} catch (IOException e1) {
						JOptionPane.showMessageDialog(fenetre, "La mise à jour s'est mal terminée : " + e1, "Erreur de mise à jour", JOptionPane.ERROR_MESSAGE);
					}
					upToDate[0] = true;
				}
			});
			
			fenetre.setContentPane(panel);
			fenetre.revalidate();
			fenetre.repaint();
			fenetre.pack();
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(fenetre, "La mise à jour à échoué avec l'erreur : " + e1, "Erreur de mise à jour", JOptionPane.ERROR_MESSAGE);
		}
		
		while(fenetre.isDisplayable()) //fenetre ouverte
		{
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				fenetre.dispose();
			}
		}
	}
	
	public static void launchConsoleUpdate()
	{
		Logger.println("Début de la mise à jour");
		Logger.println("Vérification de la version...");
		
		try {
			Updater u = new Updater();
			boolean upToDate = ! u.needsUpdate();
			if (! upToDate)
			{
				Logger.println("Mise à jour...");
				
				long debut = System.currentTimeMillis();
				
				long[] taille = {-1};
				
				Consumer<Long> onProgress = downloaded -> {
					
					long temps = (System.currentTimeMillis() - debut);
					double speed = downloaded / temps;
					//Effaçage
					for (int i = 0; i < 50; i++)
						Logger.print("\r");
					
					Logger.print("Mise à jour : " + formatBytes(downloaded) + " sur " + formatBytes(taille[0]) + " (" + formatBytes(speed * 1000) + "/s)");
				};
				Consumer<Long> onLengthReceived = length -> {
					taille[0] = length;
				};
				
				u.update(onProgress, onLengthReceived);
				Logger.println("\nLancement du script de fin ...");
				Logger.println("\nLe serveur va être arrêté, veuillez le rédemarrer");
				u.callEndScriptAndExit();
			}
			Logger.println("\nMise à jour terminée !");
			
		} catch (IOException e) {
			Logger.printErr("Erreur : La mise à jour à échoué avec l'erreur : " + e);
		}
	}

	public static void main(String[] args)
	{
		launchUpdate();			
	}

	private static String formatBytes(double bytes) {
		final String[] unites = {"bytes", "KiB", "MiB", "GiB"};
		double bytesRel = bytes;
		
		int i = 0;
		for (i = 0; i < unites.length - 1 && bytesRel > 1024; i++, bytesRel /= 1024) ;
		
		String str;
		if (bytesRel < 10)
			str = String.format("%.2f", bytesRel);
		else if (bytesRel < 100)
			str = String.format("%.2f", bytesRel);
		else
			str = "" + (int) bytesRel;
		
		return str + " " + unites[i];
	}
}
