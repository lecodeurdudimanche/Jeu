package config;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;

import partie.controles.Controle;
import utils.LogLevel;
import utils.Logger;

public class Config{

	public static final String CONFIG_FILE = "config.ini";
	private static HashMap<ConfigKey, String> config = new HashMap<>();
	private static HashMap<ConfigKey, Predicate<String>> checks = new HashMap<>();

	public static boolean readBoolean(ConfigKey key) {
		String s = read(key);
		return Boolean.parseBoolean(s);
	}

	public static String readString(ConfigKey key) {
		return read(key);
	}

	public static int readInt(ConfigKey key) {
		try{
			return Integer.parseInt(read(key));
		}catch (NumberFormatException e)
		{
			Logger.printlnErr("Impossible de lire la valeur de configuration de "+key+" : valeur numérique invalide");
		}
		return 0;
	}
	


	public static int readKey(ConfigKey key) {
		String s = read(key);
		return stringToKey(s);
	}
	

	public static String keyToString(int value) {
		for (Field f : KeyEvent.class.getFields())
		{
			String name = f.getName();
			
			try {
				if (name.startsWith("VK_") && f.getInt(null) == value)
				{
					return name.substring(3);
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {}
		}
		return null;
	}
	
	public static int stringToKey(String string) {
		try {
			return KeyEvent.class.getField("VK_" + string.toUpperCase()).getInt(null);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			return -1;
		}
	}
	
	public static Set<ConfigKey> getCles()
	{
		return new TreeSet<ConfigKey>(config.keySet());
	}

	public static boolean set(ConfigKey clef, String valeur) {
		if (check(clef, valeur))
		{
			config.put(clef, valeur.trim());
			saveConfig(CONFIG_FILE);
			return true;
		}
		return false;
	}

	private static boolean check(ConfigKey clef, String valeur) {
		if (clef != null && valeur != null)
		{
			return checks.get(clef).test(valeur);
		}
		return false;
	}

	private static String read(ConfigKey key)
	{
		if (!config.containsKey(key))
			reloadConfig();
		
		return config.get(key);
	}

	private static void reloadConfig() {
		//On charge les parametre par defaut et on ecrase avec les valeurs trouvees dans le fichier de configuration
		loadDefaults();
		loadFile(CONFIG_FILE);
		
	}

	public static void loadFile(String configFile){
		try (BufferedReader reader = new BufferedReader(new FileReader(configFile)))
		{
			String ligne;
			while((ligne = reader.readLine()) != null)
			{
				//Skip comments
				if (ligne.startsWith("#")|| ligne.trim().isEmpty())
					continue;
				
				String[] pair = ligne.split("=");
				//Skip ill-formed lines
				if (pair.length != 2)
				{
					Logger.println("Ligne incorrecte dans le fichier configuration " + configFile + " : " + ligne, LogLevel.WARNING);
					continue;
				}
				//On recupere la valeur d'enum a partir de la chaine
				String str = pair[0].trim();
				ConfigKey key = ConfigKey.fromString(str);

				//Store parameters
				if (key == null)
					Logger.println("Clé incorrecte dans le fichier configuration " + configFile + " : " + str, LogLevel.WARNING);
				else
					config.put(key, pair[1].trim());
			}
		} catch (IOException e) {
			Logger.printlnErr("Atention : impossible de lire la configuration depuis " + configFile);
		}
	}

	
	public static void saveConfig(String configFile) {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(configFile)))
		{
			Set<ConfigKey> set = new TreeSet<>(config.keySet());
			for (ConfigKey key : set)
			{
				writer.write(key.getValue() + " = " + readString(key));
				writer.newLine();
			}
		} catch (IOException e) {
			Logger.printlnErr("Atention : impossible de sauvegarder la configuration vers " + configFile);
		}
		
	}

	private static void loadDefaults() {
		Predicate<String> checkHostList = str -> true;
		Predicate<String> checkString = str -> str != null && !str.isEmpty();
		Predicate<String> checkInt = str -> {
			try {
				Integer.parseInt(str);
				return true;
			}catch(NumberFormatException e)
			{
				return false;
			}
		};
		Predicate<String> checkBool = str -> str.toLowerCase().equals("true") || str.toLowerCase().equals("false");
		Predicate<String> checkKey = str -> stringToKey(str) != -1;
		
		//Default configuration
		config.put(ConfigKey.SERVER_LIST, "");
		checks.put(ConfigKey.SERVER_LIST, checkHostList);
		config.put(ConfigKey.DISCOVERY_TIMEOUT, "1500");
		checks.put(ConfigKey.DISCOVERY_TIMEOUT, checkInt);
		config.put(ConfigKey.FULLSCREEN, "true");
		checks.put(ConfigKey.FULLSCREEN, checkBool);
		config.put(ConfigKey.WINDOWED_FULLSCREEN, "false");
		checks.put(ConfigKey.WINDOWED_FULLSCREEN, checkBool);
		config.put(ConfigKey.FRIENDLY_FIRE, "true");
		checks.put(ConfigKey.FRIENDLY_FIRE, checkBool);
		config.put(ConfigKey.TEAMS, "false");
		checks.put(ConfigKey.TEAMS, checkBool);
		config.put(ConfigKey.SHOW_FPS, "true");
		checks.put(ConfigKey.SHOW_FPS, checkBool);
		config.put(ConfigKey.LOG_LEVEL, "1");
		checks.put(ConfigKey.LOG_LEVEL, checkInt);
		config.put(ConfigKey.PSEUDO, "Anonyme");
		checks.put(ConfigKey.PSEUDO, checkString);
		config.put(ConfigKey.SERVER_TIMEOUT, "5000");
		checks.put(ConfigKey.SERVER_TIMEOUT, checkInt);
		config.put(ConfigKey.UPDATE_CHECK, "true");
		checks.put(ConfigKey.UPDATE_CHECK, checkBool);
		config.put(ConfigKey.PRINT_TO_CONSOLE, "false");
		checks.put(ConfigKey.PRINT_TO_CONSOLE, checkBool);
		
		String[][] defaults = {
				{"UP", "DOWN", "RIGHT", "LEFT", "ENTER", "CONTROL"},
				{"Z", "S", "D", "Q", "SPACE", "SHIFT"},
				{"I", "K", "L", "J", "U", "H"},
				{"NUMPAD8", "NUMPAD2", "NUMPAD6", "NUMPAD4", "NUMPAD0", "NUMPAD5"}
		};
		Controle[] controles = Controle.values();
		for (int i = 0; i < 3; i++)
		{
			for (int k = 0; k < controles.length / 2; k++)
			{
				String keyStr = "Joueur[" + (i + 1) + "]." + controles[k];
				ConfigKey key = ConfigKey.fromString(keyStr);
				config.put(key, defaults[i][k]);
				checks.put(key, checkKey);
			}
		}
		
	}

}
