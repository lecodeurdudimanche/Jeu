package config;

import java.util.HashMap;
import java.util.Map;

public enum ConfigKey {

	SERVER_LIST("Serveurs"),
	DISCOVERY_TIMEOUT("ServerDiscoveryTimeout"),
	FULLSCREEN("Fullscreen"),
	FRIENDLY_FIRE("FriendlyFire"),
	TEAMS("Teams"),
	SHOW_FPS("ShowFPS"), 
	LOG_LEVEL("LogLevel"), 
	WINDOWED_FULLSCREEN("WindowedFullscreen"),
	PSEUDO("Pseudo"),
	SERVER_TIMEOUT("ServerTimeout"),
	UPDATE_CHECK("AutoUpdateCheck"),
	PRINT_TO_CONSOLE("PrintToConsole"),
	J1_DROITE("Joueur[1].DROITE"),
	J1_GAUCHE("Joueur[1].GAUCHE"),
	J1_SAUT("Joueur[1].SAUT"),
	J1_BAS("Joueur[1].BAS"),
	J1_TIR("Joueur[1].TIR"),
	J1_SPRINT("Joueur[1].SPRINT"),
	J2_DROITE("Joueur[2].DROITE"),
	J2_GAUCHE("Joueur[2].GAUCHE"),
	J2_SAUT("Joueur[2].SAUT"),
	J2_BAS("Joueur[2].BAS"),
	J2_TIR("Joueur[2].TIR"),
	J2_SPRINT("Joueur[2].SPRINT"),
	J3_DROITE("Joueur[3].DROITE"),
	J3_GAUCHE("Joueur[3].GAUCHE"),
	J3_SAUT("Joueur[3].SAUT"),
	J3_BAS("Joueur[3].BAS"),
	J3_TIR("Joueur[3].TIR"),
	J3_SPRINT("Joueur[3].SPRINT");
	
	
	private final static Map<String, String> noms = new HashMap<>();
	private String key;
	
	private ConfigKey(String k)
	{
		key = k;
	}
	
	public String getValue()
	{
		return key;
	}
	
	public String toString()
	{
		return traduire(key);
	}
	
	public static ConfigKey fromString(String s)
	{
		for (ConfigKey c : values())
		{
			if (c.getValue().equals(s))
				return c;
		}
		return null;
	}

	private static String traduire(String cle) {
		if (noms.isEmpty())
			initMap();			

		String s = noms.get(cle);
		return s == null ? cle : s;
	}

	private static void initMap() {
		noms.put(SERVER_LIST.key, "Liste de serveurs");
		noms.put(DISCOVERY_TIMEOUT.key, "Temps de recherche des serveurs");
		noms.put(SERVER_TIMEOUT.key, "Temps maximal d'attente du serveur");
		noms.put(FULLSCREEN.key, "Mode plein écran");
		noms.put(WINDOWED_FULLSCREEN.key, "Plein écran fenêtré");
		noms.put(FRIENDLY_FIRE.key, "Activer le tir ami");
		noms.put(TEAMS.key, "Activer les équipes");
		noms.put(SHOW_FPS.key, "Afficher les FPS");
		noms.put(LOG_LEVEL.key, "Niveau de verbosité");
		noms.put(PSEUDO.key, "Pseudo par défaut");
		noms.put(UPDATE_CHECK.key, "Chercher les mise à jours au démarrage");
	}
}
