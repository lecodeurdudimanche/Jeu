package element.item;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import element.arme.ArmeAleatoire;
import element.arme.FlashBall;
import element.arme.LanceGrenade;
import element.arme.LanceMissile;
import element.arme.LanceRoquette;
import element.arme.MachineGun;
import element.arme.Pistolet;
import element.arme.SurpriseGun;
import element.buff.BuffDoubleSaut;
import element.buff.BuffInvincibilite;
import element.buff.BuffVitesse;
import utils.LogLevel;
import utils.Logger;

public interface Item {
	
	public void lire(DataInputStream stream, boolean init) throws IOException;
	public void ecrire(DataOutputStream stream, boolean init) throws IOException;
	
	
	public static Item creer(DataInputStream stream) throws IOException {
		int code = stream.readInt();
		Logger.println("Item code : " + code, LogLevel.NET_DEBUG);
		
		Item i = null;
		switch(code)
		{
			case Pistolet.CODE:
				i = new Pistolet(stream);
				break;
			case ArmeAleatoire.CODE:
				i = new ArmeAleatoire(stream);
				break;
			case FlashBall.CODE:
				i = new FlashBall(stream);
				break;
			case LanceRoquette.CODE:
				i = new LanceRoquette(stream);
				break;
			case LanceGrenade.CODE:
				i = new LanceGrenade(stream);
				break;
			case MachineGun.CODE:
				i = new MachineGun(stream);
				break;
			case SurpriseGun.CODE:
				i = new SurpriseGun(stream);
				break;
			case LanceMissile.CODE:
				i = new LanceMissile(stream);
				break;
			case BuffInvincibilite.CODE:
				i = new BuffInvincibilite(stream);
				break;
			case BuffVitesse.CODE:
				i = new BuffVitesse(stream);
				break;
			case BuffDoubleSaut.CODE:
				i = new BuffDoubleSaut(stream);
				break;
		}

		return i;
	}
}
