package element.arme;

import java.io.DataInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import element.element.Balle;
import element.element.Element;
import processing.core.PVector;
import utils.Utils;

public class ArmeAleatoire extends Arme {
	
	public static final int CODE = 2;
	protected final static String[] ARMES = {"Pistolet", "LanceGrenade", "LanceRoquette", "FlashBall", "MachineGun", "LanceMissile"};
	
	protected Arme a;
	
	public ArmeAleatoire(Element owner)
	{
		super(owner, 0);
		a = getArme(owner);
		cadenceTir = a.cadenceTir;
	}
	
	public ArmeAleatoire(DataInputStream stream) throws IOException {
		this((Element) null);
		lire(stream, true);
	}

	public static Arme getArme(Element owner)
	{		
		Arme a = null;
		String nom = Utils.random(ARMES);
		
		try {
			a = (Arme) Class.forName(Arme.class.getPackage().getName() + "." + nom).getConstructor().newInstance(new Object[] {});
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException e) {
			return a;
		}
		a.setOwner(owner);
		return a;
	}
	
	@Override
	public Balle getProjectile(float x, float y, PVector dir, float force, long tps) {
		return a.getProjectile(x, y, dir, force, tps);
	}

}
