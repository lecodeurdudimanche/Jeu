package element.arme;

import java.io.DataInputStream;
import java.io.IOException;

import element.element.Balle;
import element.element.Element;
import element.element.Missile;
import processing.core.PVector;

public class LanceMissile extends Arme {
	public static final int CODE = 8;
	
	protected float rayonProjectile, vitesseProjectile;	

	public LanceMissile() {
		this((Element) null);
	}
	
	public LanceMissile(Element owner)
	{
		super(owner, 1);
		rayonProjectile = 5; // 5 pixels
		vitesseProjectile = 200; //200 pixels / s
	}
	
	public LanceMissile(DataInputStream stream) throws IOException {
		this();
		lire(stream, true);
	}

	@Override
	public Balle getProjectile(float x, float y, PVector dir, float force, long tps) {
		PVector vitesse = PVector.mult(dir, vitesseProjectile);
		vitesse.y = 0;

		Balle proj = new Missile(x, y, tps, rayonProjectile, 20000, owner);

		proj.setVitesse(vitesse);
		proj.setMasse(50);
		
		return proj;
	}
}
