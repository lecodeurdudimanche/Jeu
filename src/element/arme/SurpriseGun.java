package element.arme;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import element.element.Balle;
import element.element.Element;
import element.item.Item;
import processing.core.PVector;
import utils.Utils;

public class SurpriseGun extends Arme {
	public static final int CODE = 6;
	
	private Arme[] armes;
	
	
	public SurpriseGun(Element owner)
	{
		super(null, 0);
		armes = new Arme[]{new Pistolet(owner), new LanceGrenade(owner), new MachineGun(owner), new FlashBall(owner)};
	}
	
	public SurpriseGun(DataInputStream stream) throws IOException {
		this((Element) null);
		lire(stream, true);
	}

	@Override
	public Balle getProjectile(float x, float y, PVector dir, float force, long tps) {
		return Utils.random(armes).getProjectile(x, y, dir, force, tps);
	}

	@Override
	public void ecrire(DataOutputStream stream, boolean init) throws IOException {
		super.ecrire(stream, init);
		
		stream.writeInt(armes.length);
		for (Arme a : armes)
			a.ecrire(stream, init);
	}

	@Override
	public void lire(DataInputStream stream, boolean init) throws IOException {
		super.lire(stream, true);

		int nb = stream.readInt();
		armes = new Arme[nb];
		for (int i = 0; i < nb; i++)
			armes[i] = (Arme) Item.creer(stream);
	}
	
	@Override
	public void setOwner(Element owner) {
		for(Arme a : armes)
			a.setOwner(owner);
	}
}
