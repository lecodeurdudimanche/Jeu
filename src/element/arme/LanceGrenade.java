package element.arme;

import java.io.DataInputStream;
import java.io.IOException;

import element.element.Balle;
import element.element.Element;
import element.element.Grenade;
import processing.core.PVector;
import utils.Utils;

public class LanceGrenade extends Arme {
	public static final int CODE = 7;
	
	protected int holy, dureeMin, dureeMax;
	protected float rayonProjectile, vitesseProjectile;
	

	public LanceGrenade() {
		this((Element) null);
	}
	
	public LanceGrenade(Element owner)
	{
		this(owner, true);
	}

	public LanceGrenade(Element owner, boolean isHoly) {
		this(owner, isHoly, 1000, 5000);
	}
	
	public LanceGrenade(Element owner, boolean isHoly, int dureeMin, int dureeMax) {
		super(owner, 1);
		rayonProjectile = 5; // 5 pixels
		vitesseProjectile = 600; //600 pixels / s
		holy = isHoly ? 1 : 0; //Grenade sacrée
		this.dureeMin = dureeMin;
		this.dureeMax = dureeMax;
	}
	
	public LanceGrenade(DataInputStream stream) throws IOException {
		this();
		lire(stream, true);
	}

	@Override
	public Balle getProjectile(float x, float y, PVector dir, float force, long tps) {
		PVector vitesse = PVector.mult(dir, vitesseProjectile);
		
		long duree = (long) Utils.map(force, 0, 1, dureeMin, dureeMax);
		
		Balle proj = new Grenade(x, y, tps, rayonProjectile, duree, owner, holy);

		proj.setVitesse(vitesse);
		
		return proj;
	}
}
