package element.arme;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import element.element.Balle;
import element.element.Element;
import element.item.Item;
import processing.core.PVector;

public abstract class Arme implements Item{
	
	protected long lastShot;
	protected float cadenceTir;
	protected Element owner;
	
	protected Arme(Element owner, float cadence)
	{
		lastShot = 0;
		cadenceTir = cadence;
		this.owner = owner;
	}

	public Balle tirer(float x, float y, PVector dir, float force, long tps)
	{
		if (tps - lastShot < 1000 / cadenceTir)
			return null;
	
		lastShot = tps;		

		Balle b = getProjectile(x, y, dir, force, tps);
		return b;
	}
	
	protected abstract Balle getProjectile(float x, float y, PVector dir, float force, long tps);

	public void setOwner(Element owner)
	{
		this.owner = owner;
	}

	public void ecrire(DataOutputStream stream, boolean init) throws IOException
	{
		try {
			stream.writeInt(getClass().getDeclaredField("CODE").getInt(null));
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
		
		//stream.writeLong(owner == null ? -1 : owner.getId());
		
		//stream.writeFloat(cadenceTir);
	}
	
	public void lire(DataInputStream stream, boolean init) throws IOException
	{
		/*long idOwner = stream.readLong();
		if (idOwner == -1)
			owner = null;
		else
			owner = partie.findJoueurById(idOwner);*/
	}
}
