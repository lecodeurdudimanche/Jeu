package element.arme;

import java.io.DataInputStream;
import java.io.IOException;

import element.element.Balle;
import element.element.Element;
import processing.core.PVector;
import utils.Utils;

public class FlashBall extends Arme {
	public static final int CODE = 3;
	
	protected long tempsProjectile;
	protected float rayonProjectile, minSpeed , maxSpeed;
	private Element owner;
	

	public FlashBall() {
		this((Element) null);
	}
	
	public FlashBall(Element owner)
	{
		super(owner, 3);
		rayonProjectile = 8; // 8 pixels
		tempsProjectile = 20000; //20s
		minSpeed = 800; //Vitesse entre 800
		maxSpeed = 2000; // et 2000 pixels / s
	}
	
	public FlashBall(DataInputStream stream) throws IOException {
		this();
		lire(stream, true);
	}

	@Override
	public Balle getProjectile(float x, float y, PVector dir, float force, long tps) {
		float coeff = Utils.map(force, 0, 1, minSpeed, maxSpeed);
		
		PVector vitesse = PVector.mult(dir, coeff);
		
		Balle proj = new Balle(x, y, tps, rayonProjectile, tempsProjectile, owner);

		proj.setVitesse(vitesse);
		proj.setMasse(20);
		
		return proj;
	}
}
