package element.arme;

import java.io.DataInputStream;
import java.io.IOException;

import element.element.Element;

public class MachineGun extends Pistolet{
	
	public static final int CODE = 5;

	public MachineGun() {
		this((Element) null);
	}
	
	public MachineGun(Element owner)
	{
		super(owner, 8);
		rayonProjectile = 2;
		minSpeed = 800;
		maxSpeed = 800;
		tempsProjectile = 2000;
	}

	public MachineGun(DataInputStream stream) throws IOException {
		super(stream);
	}
}
