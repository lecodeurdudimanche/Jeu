package element.arme;

import java.io.DataInputStream;
import java.io.IOException;

import element.element.Balle;
import element.element.Element;
import element.element.Projectile;
import processing.core.PVector;
import utils.Utils;

public class Pistolet extends Arme {
	public static final int CODE = 1;
	
	protected long tempsProjectile;
	protected float rayonProjectile, minSpeed , maxSpeed;

	public Pistolet() {
		this((Element) null);
	}
	
	public Pistolet(Element owner)
	{
		this(owner, 2);
	}
	
	protected Pistolet(Element owner, int cadence)
	{
		super(owner, cadence);
		rayonProjectile = 5; // 5 pixels
		tempsProjectile = 10000; //10s
		minSpeed = 200; //Vitesse entre 200
		maxSpeed = 1500; // et 1500 pixels / s
	}
	
	public Pistolet(DataInputStream stream) throws IOException {
		this();
		lire(stream, true);
	}

	@Override
	public Balle getProjectile(float x, float y, PVector dir, float force, long tps) {
		float coeff = Utils.map(force, 0, 1, minSpeed, maxSpeed);
		
		PVector vitesse = PVector.mult(dir, coeff);
		
		Balle proj = new Projectile(x, y, tps, rayonProjectile, tempsProjectile, owner);

		proj.setVitesse(vitesse);
		
		return proj;
	}
}
