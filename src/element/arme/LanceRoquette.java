package element.arme;

import java.io.DataInputStream;
import java.io.IOException;

import element.element.Element;

public class LanceRoquette extends LanceGrenade {

	public static final int CODE = 4;

	public LanceRoquette() {
		this((Element) null);
	}

	public LanceRoquette(Element owner) {
		super(owner);
		cadenceTir = 1; //coups / secondes
		rayonProjectile = 7; //pixels
		vitesseProjectile = 1200; //pixels / s
		holy = 0; // Une roquette n'est pas sacrée
	}

	public LanceRoquette(DataInputStream stream) throws IOException {
		super(stream);
	}

}
