package element.element;

import java.io.DataInputStream;
import java.io.IOException;

import element.arme.Arme;
import element.collision.Rectangle;
import processing.core.PApplet;

public class ArmeCollectable extends ElementCollectable{
	
	public static final int CODE = 1;
	
	private int W = 40, H = 12;

	public ArmeCollectable(float x, float y, long tps, Arme arme) {
		super(x, y, tps, arme);
		forme = new Rectangle(pos, W, H);
	}
	
	protected ArmeCollectable(DataInputStream stream, long id) throws IOException {
		super(stream, id);
	}

	@Override
	public void afficher(PApplet p) {
		p.stroke(255, 255, 0);
		p.noFill();
		
		p.rect(pos.x, pos.y, W, H);
		
		p.noStroke();
		p.fill(255, 255, 0);
		
		p.textSize(8);
		p.text(item == null ? "Vide" : item.getClass().getSimpleName(), pos.x, pos.y, W, H);
	}
}
