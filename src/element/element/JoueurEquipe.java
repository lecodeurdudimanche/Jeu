package element.element;

import config.Config;
import config.ConfigKey;
import partie.configuration.ParametresPartie;

/**
 * Interface symbolisant un joueur (ou IA, ou matériel) appartenant à une équipe et qui peut être endommagé 
 * @author duvaladr
 *
 */
public interface JoueurEquipe {

	public int getEquipe();

	public default boolean estMemeEquipe(ParametresPartie params, JoueurEquipe e) {
		return getEquipe() == e.getEquipe() && !params.isFriendlyFire();
	}
}
