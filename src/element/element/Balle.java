package element.element;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import element.collision.Cercle;
import partie.partie.Partie;
import processing.core.PApplet;
import utils.LogLevel;
import utils.Logger;
import utils.Utils;


public class Balle extends ElementMobile implements JoueurEquipe {
	
	public static final int CODE = 2;
	
	protected float r;
	protected long endOfLife, resteAVivre;
	protected Element owner;
	
	public Balle(float x, float y, long tps, float r, long ttl, Element owner) {
		super(x, y, tps);
		forme = new Cercle(pos, r);	
		this.r = r < 0 ? 10 : r;
		masse = (float) Math.sqrt(r);
		resteAVivre = ttl;
		endOfLife = tps + ttl;
		this.owner = owner;
	}
	
	protected Balle(DataInputStream stream, long id) throws IOException
	{
		super(stream, id);
	}

	@Override
	public void afficher(PApplet p) {
		float alpha = 255;
		float fin = 100;
		if (resteAVivre <= fin)
		{
			alpha = Utils.map(resteAVivre, fin, 0, 255, 0);
		}
			
		p.fill(255, alpha);
		p.ellipse(pos.x, pos.y, r * 2, r * 2);
	}
	
	@Override
	public void evoluer(long t, Partie j)
	{
		super.evoluer(t, j);
		
		resteAVivre = endOfLife - t;
	
		//Une balle n'est pas mortelle, elle se détruit juste au bout d'un temps
		if (resteAVivre <= 0)
		{
			detruire();
		}
	}

	@Override
	public int getEquipe() {
		return owner != null && owner instanceof JoueurEquipe ? ((JoueurEquipe)owner).getEquipe() : -1;
	}
	
	
	@Override
	protected void faireCollision(Element collider, Partie p) {
	}
	
	public float getRayon() {
		return r;
	}

	@Override
	public void ecrire(DataOutputStream stream, boolean init) throws IOException{
		super.ecrire(stream, init);
		
		if (init || !estInit)
		{
			stream.writeFloat(r);
			Logger.println("Writed r "+ r, LogLevel.NET_DEBUG);
		}
		
		stream.writeLong(resteAVivre);
		Logger.println("Writed rav "+ resteAVivre, LogLevel.NET_DEBUG);
	}

	@Override
	public void lire(DataInputStream stream, boolean init) throws IOException{
		super.lire(stream, init);
		
		if (init)
		{
			r = stream.readFloat();
			Logger.println("Readed r "+ r, LogLevel.NET_DEBUG);
		}
		
		resteAVivre = stream.readLong();
		Logger.println("Readed rav "+ resteAVivre, LogLevel.NET_DEBUG);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Balle other = (Balle) obj;
		if (Float.floatToIntBits(r) != Float.floatToIntBits(other.r))
			return false;
		if (resteAVivre != other.resteAVivre)
			return false;
		return true;
	}
	
}
