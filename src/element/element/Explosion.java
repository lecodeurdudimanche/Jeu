package element.element;

import java.io.DataInputStream;
import java.io.IOException;

import element.collision.Cercle;
import partie.partie.Partie;
import processing.core.PApplet;
import processing.core.PVector;
import utils.Utils;

public class Explosion extends Projectile {
	
	public static final int CODE = 5;
	
	protected long duree;
	private float vraiRayon;

	protected Explosion(DataInputStream stream, long id) throws IOException {
		super(stream, id);
	}

	public Explosion(float x, float y, long tps, float r, Element owner) {
		super(x, y, tps, r, 140, owner);
		duree = 140;
		forme = new Cercle(pos, 1);
		vraiRayon = 0;
	}

	@Override
	public void afficher(PApplet p) {
		float alpha = 255;
		float fin = 100;
		if (resteAVivre <= fin)
		{
			alpha = Utils.map(resteAVivre, fin, 0, 255, 0);
		}
		
		float d = vraiRayon * 2;
			
		p.noFill();
		p.stroke(255, 0, 0, alpha);
		p.strokeWeight(2);
		p.ellipse(pos.x, pos.y, d, d);
	}
	
	@Override
	public void evoluer(long t, Partie p)
	{
		forme = new Cercle(pos, vraiRayon);
		super.evoluer(t, p);
		vraiRayon = Utils.map(resteAVivre, 0, duree, r, 0);
	}
	
	@Override
	public PVector getResponseForce(Element e)
	{
		return super.getResponseForce(e).mult(r / duree);
	}
	
	@Override
	public void appliquerForce(PVector p)
	{
	}
	
	@Override
	public void setVitesse(PVector p)
	{
	}
	
	@Override
	public void setAccelerationY(float y)
	{
	}
	
	@Override
	public void detruire()
	{
		if (resteAVivre <= 0)
			super.detruire();
	}
	
}
