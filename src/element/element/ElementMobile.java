package element.element;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

import partie.partie.Partie;
import processing.core.PVector;
import utils.LogLevel;
import utils.Logger;

public abstract class ElementMobile extends Element{
	/**
	 * Vitesse, en pixels/s, de l'élément
	 * Accélération, en pixels/s^-2, de l'élément
	 */
	protected PVector vitesse, acceleration;
	
	/**
	 * Temps du dernier mouvement
	 */
	protected long tps;
	
	protected PVector coeffRebond;
	
	/**
	 * Si on est bloqué en haut, en bas, a droite ou a gauche
	 */
	protected boolean bloquageBas, bloquageHaut, bloquageDroite, bloquageGauche;
	
	/**
	 * Derniere collisions, 0 => x, 1 => y
	 */
	protected Element[] lastCollision;
	
	
	public ElementMobile(float x, float y, long t) {
		super(x, y);
		vitesse = new PVector(0, 0);
		acceleration = new PVector(0, 0);
		
		tps = t;
		lastCollision = new Element[2];
		coeffRebond = new PVector((float).5, (float).5);
	}
	
	protected ElementMobile(DataInputStream stream, long id) throws IOException {
		super(stream, id);
	}

	/**
	 * Permet de faire évoluer l'élément, en fonction du temps actuel
	 * @param t temps en millisecondes
	 * @param jeu Partie pour les collisions
	 */
	@Override
	public void evoluer(long t, Partie p)
	{
		Arrays.fill(lastCollision, null);
		
		bloquageBas = bloquageHaut = bloquageDroite = bloquageGauche = false;
		
		//duree en secondes
		float duree = (t - tps) / 1000f;
		tps = t;
		
		//On ajoute l'acceleraion a la vitesse
		vitesse.add(acceleration);
		
		float mag = vitesse.mag();
		float deplacementMag = mag * duree;
		
		//vitesse * duree = deplacement
		PVector deplacement = PVector.mult(vitesse, duree);
		
		int iterations = 1;
		if (deplacementMag > 1) //Si on se deplace plus de 1 pixels, on fragmente le mouvement
		{
			iterations = (int) deplacementMag;
			deplacement.div(iterations);
		}
		
		for (int i = 0; i < iterations /*&& (deplacement.x != 0 || deplacement.y != 0)*/; i++)
		{
			//On essaye le mouvement en x
			float x = pos.x;
			pos.x += deplacement.x;

			Element e = p.checkCollision(this);
			if (e != null)
			{
				pos.x = x;
				
				if (vitesse.x > 0)
					bloquageDroite = true; //On va a droite, on est bloque a droite
				else
					bloquageGauche = true;
	
				vitesse.x = 0;
	
				faireCollision(e, coeffRebond.x, mag);
				
				lastCollision[0] = e;
				
				deplacement.x = 0;
			}
			
			//On essaye le mouvement en y
			float y = pos.y;
			pos.y += deplacement.y;
			
			e = p.checkCollision(this);
			if (e != null)
			{
				pos.y = y;
				
				if (vitesse.y > 0)
					bloquageBas = true; // On va en bas, on est bloques en bas
				else
					bloquageHaut = true;
				
				vitesse.y = 0;
				
				faireCollision(e, coeffRebond.y, mag);
				
				lastCollision[1] = e;

				deplacement.y = 0;
			}
		}

		vitesse.add(acceleration);
		/*if (vitesse.mag() > mag * 0.99)
			vitesse.setMag((float)(mag * 0.99));*/
		//On reset l'acceleration
		acceleration.set(0, 0);
		
		//Friction
		vitesse.mult((float)0.999);
		
		for (Element col : lastCollision)
		{
			if (col != null)
			{
				faireCollision(col, p);
				col.faireCollision(this, p);
			}
			
		}
	}
	
	@Override
	protected void faireCollision(Element collider, Partie p)
	{
	}
	
	protected void faireCollision(Element e, float coeff, float mag)
	{
		for (Element autre : lastCollision)
		{
			if (autre != null)
				return;
		}
		float magTot = mag;
		float masseTot = masse + e.masse;
		
		if (e instanceof ElementMobile)
		{
			ElementMobile em = (ElementMobile) e;
			magTot += em.getVitesse().mag();
			
			float coeffAutre = (float) (-coeff * magTot * (masse / masseTot));
			
			em.appliquerForce(PVector.mult(e.getResponseForce(this), coeffAutre));
		}
		float coeffNous = (float) (coeff * magTot * (e.masse / masseTot)); 
		appliquerForce(PVector.mult(e.getResponseForce(this), coeffNous));
	}
	
	/**
	 * Permet de définir la vitesse précise de l'élément
	 * @param vit Vitesse à définir
	 */
	public void setVitesse(PVector vit)
	{
		vitesse.set(vit);
	}
	
	/**
	 * Permet de set l'accéleration verticale d'un élément
	 */
	public void setAccelerationY(float y)
	{
		acceleration.y = y;
	}
	
	/**
	 * Permet de recuperer la vitesse de l'élément
	 * @return la vitesse
	 */
	public PVector getVitesse()
	{
		return vitesse.copy();
	}
	
	/**
	 * Permet d'ajouter une force agissant sur l'élément, en pixels/s^-2
	 * @param force Force à appliquer
	 */
	@Override
	public void appliquerForce(PVector force)
	{
		acceleration.add(force);
	}
	
	//ElementMobile : on envoie la pos a chaque fois
	//On envoie la pos si Element ne l'a pas envoye (on est pas a l'init)
	@Override
	public void ecrire(DataOutputStream stream, boolean init) throws IOException{
		super.ecrire(stream, init);
		
		if (!init && estInit)
		{
			Logger.println("Writed float " + pos.x, LogLevel.NET_DEBUG);
			stream.writeFloat(pos.x);
			Logger.println("Writed float " + pos.y, LogLevel.NET_DEBUG);
			stream.writeFloat(pos.y);
		}
	}

	@Override
	public void lire(DataInputStream stream, boolean init) throws IOException{
		super.lire(stream, init);
		
		if (!init)
		{
			pos.x = stream.readFloat();
			Logger.println("Readed x float " + pos.x, LogLevel.NET_DEBUG);
			pos.y = stream.readFloat();
			Logger.println("Readed y float " + pos.y, LogLevel.NET_DEBUG);
		}
	}
}
