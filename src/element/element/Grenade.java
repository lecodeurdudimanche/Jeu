package element.element;

import java.io.DataInputStream;
import java.io.IOException;

import partie.partie.Partie;
import processing.core.PApplet;
import processing.core.PVector;
import utils.Utils;

public class Grenade extends Balle {
	
	public static final int CODE = 6;

	private int remainingHolyExplosions;

	public Grenade(float x, float y, long tps, float r, long ttl, Element owner) {
		this(x, y, tps, r, ttl, owner, 0);
	}
	
	public Grenade(float x, float y, long tps, float r, long ttl, Element owner, int holy) {
		super(x, y, tps, r, ttl, owner);
		remainingHolyExplosions = holy;
	}
	
	protected Grenade(DataInputStream stream, long id) throws IOException{
		super(stream, id);
	}
	
	@Override 
	public void afficher(PApplet p)
	{
		float alpha = 255;
		float fin = 100;
		if (resteAVivre <= fin)
		{
			alpha = Utils.map(resteAVivre, fin, 0, 255, 0);
		}
			
		p.fill(255, 0, 0, alpha);
		p.ellipse(pos.x, pos.y, r * 2, r * 2);
	}
	
	@Override
	public void evoluer(long t, Partie j)
	{
		super.evoluer(t, j);
		
		//La grenade explose
		if (t >= endOfLife)
		{
			int tot = remainingHolyExplosions > 0 ? 4 : 16;
			float speed = 1500;
			
			for (int i = 0; i < tot; i++)
			{
				double a = (Math.PI * 2 / tot) * i;
				
				float xCoeff = (float) Math.cos(a);
				float yCoeff = (float) Math.sin(a);
				
				float xSpeed = xCoeff * speed;
				float ySpeed = yCoeff * speed;
				
				float x = this.pos.x + xCoeff * r;
				float y = this.pos.y + yCoeff * r;
				
				Balle proj;
				if (remainingHolyExplosions > 0)
				{
					proj = new Grenade(x, y, t, r / 1.5f, 500, owner, remainingHolyExplosions - 1);
					ySpeed /= 4;
					xSpeed /= 4;
					ySpeed -= 10;
				}
				else
					proj = new Projectile(x, y, t, r / 3, 200, owner);

				proj.setVitesse(new PVector(xSpeed, ySpeed));
				
				j.ajouterElement(proj);
			}
		}
	}

}
