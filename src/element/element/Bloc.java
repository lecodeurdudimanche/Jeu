package element.element;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import element.collision.Rectangle;
import partie.partie.Partie;
import processing.core.PApplet;
import utils.LogLevel;
import utils.Logger;
import utils.Utils;

public class Bloc extends Element implements Destructible{
	
	public static final int CODE = 3;
	
	protected float w, h;
	
	protected int vie;
	
	protected final static int VIE_MAX = 10;
	
	
	public Bloc(float x, float y, float w, float h) {
		super(x, y);
		forme = new Rectangle(pos, w, h);
		this.w = w;
		this.h = h;
		
		vie = VIE_MAX;
		
	}

	protected Bloc(DataInputStream stream, long id) throws IOException {
		super(stream, id);
	}

	@Override
	public void afficher(PApplet p) {
		if (estDetruit())
			return; 
		
		/*float blanc = 128 + Utils.map(vie, 0, VIE_MAX, 0, 127);
		p.fill(255, blanc, blanc);
		p.rect(pos.x, pos.y, w, h);*/
	}
	
	@Override
	protected void faireCollision(Element collider, Partie p) {
	}
	
	@Override 
	public void detruire()
	{
		if (vie == 0)
			detruit = true;
		else
			vie--;

	}
	
	@Override
	public void ecrire(DataOutputStream stream, boolean init) throws IOException{
		super.ecrire(stream, init);
		
		if (init || !estInit)
		{
			Logger.println("Writed w " + w, LogLevel.NET_DEBUG);
			stream.writeFloat(w);
			Logger.println("Writed h " + h, LogLevel.NET_DEBUG);
			stream.writeFloat(h);
		}

		
		Logger.println("Writed vie " + vie, LogLevel.NET_DEBUG);
		stream.writeInt(vie);
	}
	
	@Override
	public void lire(DataInputStream stream, boolean init) throws IOException{
		super.lire(stream, init);
		
		if (init)
		{
			w = stream.readFloat();
			Logger.println("Readed w " + w, LogLevel.NET_DEBUG);
			h = stream.readFloat();
			Logger.println("Readed h " + h, LogLevel.NET_DEBUG);
		}
		
		vie = stream.readInt();
		Logger.println("Readed vie " + vie, LogLevel.NET_DEBUG);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bloc other = (Bloc) obj;
		if (Float.floatToIntBits(h) != Float.floatToIntBits(other.h))
			return false;
		if (vie != other.vie)
			return false;
		if (Float.floatToIntBits(w) != Float.floatToIntBits(other.w))
			return false;
		return true;
	}


}
