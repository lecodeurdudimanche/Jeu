package element.element;

import java.io.DataInputStream;
import java.io.IOException;

import element.buff.Buff;
import element.collision.Rectangle;
import processing.core.PApplet;

public class BuffCollectable extends ElementCollectable {

	public static final int CODE = 12;

	public BuffCollectable(float x, float y, long tps, Buff contenu) {
		super(x, y, tps, contenu);
		forme = new Rectangle(pos, 40, 15);
	}
	
	protected BuffCollectable(DataInputStream stream, long id) throws IOException {
		super(stream, id);
	}

	@Override
	public void afficher(PApplet p) {
		p.stroke(255, 255, 0);
		p.noFill();
		
		p.rect(pos.x, pos.y, 40, 15);
		
		p.noStroke();
		p.fill(0, 255, 0);
		
		p.textSize(8);
		p.text(item == null ? "Vide" : item.getClass().getSimpleName().substring(4), pos.x, pos.y, 40, 15);
	}

}
