package element.element;

import java.io.DataInputStream;
import java.io.IOException;

import partie.IA.IA;
import partie.controles.Controlable;

public abstract class ElementIA extends ElementMobile implements Controlable{
	
	protected IA intelligence;
	
	public ElementIA(float x, float y, long t, IA intelligence) {
		super(x, y, t);
		setIA(intelligence);
	}

	protected ElementIA(DataInputStream stream, long id) throws IOException {
		super(stream, id);
	}
	
	public void setIA(IA ia)
	{
		intelligence = ia;
		ia.attacher(this);
	}	
	
}
