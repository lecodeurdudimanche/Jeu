package element.element;


import java.io.DataInputStream;
import java.io.IOException;

import processing.core.PApplet;
import processing.core.PImage;
import utils.Ressources;
import utils.Utils;

public class Plateforme extends Bloc{
	
	public static final int CODE = 13;
	private static final int NB_IMAGES = 5;

	public Plateforme(float x, float y, float w, float h) {
		super(x, y, w, h);
	}

	public Plateforme(DataInputStream stream, long id) throws IOException {
		super(stream, id);
	}

	public void afficher(PApplet p) {
		if (estDetruit())
			return; 
		else {
			PImage img = Ressources.getImage("bloc" + ((int) Utils.map(vie, 1, VIE_MAX, 1, NB_IMAGES)));
			int wImg = 32, hImg = 32;
			for (int x = 0; x < w; x += wImg)
			{
				for (int y = 0; y < h; y += hImg)
				{
					float cropW = Math.min(wImg, w - x), cropH = Math.min(hImg, h - y);
					int srcW = (int) ((img.width * cropW) / wImg), srcH = (int) ((img.height * cropH) / hImg);
					p.image(img, x + pos.x, y + pos.y, cropW, cropH, 0, 0, srcW, srcH);
				}
			}
		}
	}

}
