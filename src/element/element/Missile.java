package element.element;

import java.io.DataInputStream;
import java.io.IOException;

import partie.controles.Controlable;
import partie.controles.Controle;
import partie.controles.Controleur;
import partie.partie.Partie;
import processing.core.PVector;

public class Missile extends Grenade implements Controlable {
	
	public static final int CODE = 7;
	
	private static final int DUREE_TURBO = 1000;
	
	private boolean turbo;
	private Controleur controleur;

	public Missile(float x, float y, long tps, float r, long ttl, Element owner) {
		super(x, y, tps, r, ttl, owner);
		turbo = false;
	}
	
	protected Missile(DataInputStream stream, long id) throws IOException {
		super(stream, id);
		turbo = false;
	}
	
	@Override
	public void evoluer(long t, Partie j)
	{
		super.evoluer(t, j);
		
		for (Element e : lastCollision)
		{
			if (e != null)
				exploser();
		}
		
		
		if (controleur == null && owner != null)
		{
			Controleur c = j.getControleur((Controlable) owner);
			controleur = c;
			controleur.attacher(this);
			j.setElementFollowed(id);
		}
		
		if (estDetruit())
		{
			controleur.attacher((Personnage) owner); 
			j.setElementFollowed(owner.getId());
		}
	}

	@Override
	public void appliquerForce(PVector force)
	{
		//super.appliquerForce(PVector.div(force, 5));
	}

	@Override
	public void action(Controle c, Partie p, float[] args) {
		if (turbo)
			return;
	//	vitesse.mult(0.99f);
		//vitesse.mult(0);
		float v = 300;
		switch(c)
		{
			case DROITE:
				vitesse.x = v;
				vitesse.y = 0;
				break;
			case GAUCHE:
				vitesse.x = -v;
				vitesse.y = 0;
				break;
			case SAUT:
				vitesse.y = -v;
				vitesse.x = 0;
				break;
			case BAS:
				vitesse.y = v;
				vitesse.x = 0;
				break;
			case TIR_RELACHE:
				turbo = true;
				vitesse.mult(3);
				endOfLife = System.currentTimeMillis() + DUREE_TURBO;
				break;
			default:
				break;
		}
		
	}

	private void exploser() {
		endOfLife = System.currentTimeMillis();
	}

	

}
