package element.element;

import java.io.DataInputStream;
import java.io.IOException;

import partie.partie.Partie;
import partie.statistiques.ListeStatistiques;
import partie.statistiques.TypeStatistique;

public class Projectile extends Balle {
	
	public static final int CODE = 10;

	public Projectile(float x, float y, long tps, float r, long ttl, Element owner) {
		super(x, y, tps, r, ttl, owner);
	}

	protected Projectile(DataInputStream stream, long id) throws IOException{
		super(stream, id);
	}

	@Override
	public void faireCollision(Element collider, Partie p) {
		if (estDetruit() || collider.estDetruit())
			return;
		
		if (collider instanceof Destructible)
			collider.detruire();
		else if (collider instanceof Personnage)
		{
			Personnage pTireur = (Personnage) owner;
			Personnage pTouche = (Personnage) collider;

			boolean friendlyFire = p.getParametres().isFriendlyFire();
			
			boolean friends = owner != null && (pTireur.getId() == pTouche.getId() || pTireur.getEquipe() == pTouche.getEquipe());
			
			//Si friendly fire ou alors s'ils sont pas friends
			if (friendlyFire || !friends)
			{
				collider.detruire();
				detruire();
			}
			
			//Si le tir a tué : 
			if (collider.estDetruit())
			{
				if (owner == null) //Tireur = mere nature
					pTouche.ajouterScore(-1);
				else if (friendlyFire && friends)
				{
					pTireur.ajouterScore(-1);
					ListeStatistiques.log(pTireur.getId(), TypeStatistique.FRIENDLY_FIRE);
				}
				else
				{
					pTireur.ajouterScore(1);
					ListeStatistiques.log(pTireur.getId(), TypeStatistique.KILLS);
				}
			}
		}
		
		if (collider.estDetruit() && owner != null)
			ListeStatistiques.log(owner.getId(), TypeStatistique.DEGATS);
	}
	
}
