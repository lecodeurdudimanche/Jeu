package element.element;

import java.io.DataInputStream;
import java.io.IOException;

import processing.core.PApplet;
import processing.core.PImage;
import utils.Ressources;

public class Sol extends Bloc {
	
	public static final int CODE = 11;
	
	public final static int H = 30;
	
	public Sol(float wEcran, float hEcran) {
		super(0, hEcran - H, wEcran, H);
	}
	
	protected Sol(DataInputStream stream, long id) throws IOException{
		super(stream, id);
	}
	
	@Override
	public void detruire()
	{
		
	}
	
	public void afficher(PApplet p) {
		if (estDetruit())
			return; 
		else {PImage img = Ressources.getImage("sol2");
		for (int x = 0; x < w; x += img.width)
		{
			for (int y = 0; y < h; y += img.height)
			{
				float cropW = Math.min(img.width, w - x), cropH = Math.min(img.height, h - y);
				p.image(img, x + pos.x, y + pos.y, cropW, cropH);
			}
		}
		}
	}

}
