package element.element;

import java.io.DataInputStream;
import java.io.IOException;

import element.arme.Arme;
import element.arme.LanceGrenade;
import element.collision.Cercle;
import partie.IA.IA;
import partie.controles.Controle;
import partie.partie.Partie;
import processing.core.PApplet;
import processing.core.PVector;
import utils.Utils;

public class Tourette extends ElementIA implements Destructible{
	
	public static final int CODE = 14;

	protected static final int W = 20;
	protected Arme arme;
	protected float angle;
	protected final static float MIN = (float) (0), MAX = (float) (Math.PI); 

	public Tourette(float x, float y, long t, IA intelligence) {
		super(x, y, t, intelligence);
		forme = new Cercle(pos, W);
		arme = new LanceGrenade(null, false, 400, 600);
		masse = 10000;
	}
	
	protected Tourette(DataInputStream stream, long id) throws IOException {
		super(stream, id);
	}
	@Override
	public void action(Controle c, Partie p, float[] args) {
		float vitesse = 0.02f;
		switch(c)
		{
		case DROITE:
			setAngle(angle - vitesse);
			break;
		case GAUCHE:
			setAngle(angle + vitesse);
			break;
		case TIR:
			if (args != null)
			{				
				float force = args[0];
				float x = pos.x, y = pos.y;
				
				float xA = (float) Math.cos(angle), yA = (float) - Math.sin(angle);
				
				x += xA * W * 2f;
				y += yA * W * 2f;
				
				
				PVector dir = new PVector(xA, yA);
				
				Balle projectile = arme.tirer(x, y, dir, force, System.currentTimeMillis());
				p.ajouterElement(projectile);
				break;
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void afficher(PApplet p) {
		
		p.fill(Utils.color(100, 100, 255));
		p.rect(pos.x - 2, pos.y, 4, W);
		
		p.pushMatrix();
		p.translate(pos.x,  pos.y);
		p.rotate((float) (2 * Math.PI - angle - Math.PI / 2));
		p.rect(-3, 0, 6, W);
		p.popMatrix();
	}
	
	private void setAngle(float angle)
	{
		angle %= Math.PI * 2;
		this.angle = angle < MIN ? MIN : (angle > MAX ? MAX : angle);
	}
	
	public boolean estBloquee()
	{	
		float delta = 1E-9f;
		return (angle - MIN < delta || MAX - angle < delta);
	}

	public float getAngle() {
		return angle;
	}

}
