package element.element;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import element.collision.Forme;
import partie.controles.Controlable;
import partie.partie.Partie;
import processing.core.PApplet;
import processing.core.PVector;
import utils.LogLevel;
import utils.Logger;

/**
 * Tout élément affichable, qui a des collisions
 * @author adrien
 *
 */
public abstract class Element{
	/**
	 * Vrai si l'élément est détruit
	 */
	protected boolean detruit;
	/**
	 * Position de l'élément
	 */
	protected PVector pos;
	/**
	 * Forme de l'element, pour la collision (doit etre intialise par les sous-classes)
	 */
	protected Forme forme;
	/**
	 * Masse de l'élément
	 */
	protected float masse;
	/**
	 * Id de l'élément
	 */
	protected long id;
	/**
	 * Vrai quand l'element a ete init
	 * Non utilise, mais implemente, tant que on force l'init dans envoyerPartie de Client
	 */
	protected boolean estInit;
	
	/**
	 * Count des ids;
	 */
	private static long idCount = 0;
	
	public Element(float x, float y)
	{
		pos = new PVector(x, y);
		detruit = false;
		masse = 1;
		id = getNextId();
		estInit = false;
	}
	
	private long getNextId() {
		return idCount++;
	}

	protected Element(DataInputStream stream, long id) throws IOException
	{
		this(0, 0);
		this.id = id;
		lire(stream, true);
	}
	
	/**
	 * Permet de détruire l'élément
	 */
	public void detruire() {
		detruit = true;
	}
	
	/**
	 * Permet de savoir si l'élément est détruit
	 * @return Vrai s'il est détruit
	 */
	public boolean estDetruit()
	{
		return detruit;
	}
	/**
	 * Permet de faire la collision avec cet élément
	 * @param e Element avec lequel détecter la collision
	 * @return vrai s'il y a collision
	 */
	public boolean collision(Element e) {
		return e.getForme() != null && collision(e.getForme());
	}
	/**
	 * Permet de faire la collision avec cette forme
	 * @param f Forme de l'élément
	 * @return vrai s'il y a collision
	 */
	public boolean collision(Forme f) {
		return !detruit && forme != null && forme.collision(f);
	}
	
	/**
	 * Permet d'afficher l'élément
	 * @param p Là où on va dessiner
	 */
	public abstract void afficher(PApplet p);
	
	/**
	 * Que faire quand on est en collision avec un element ?
	 * @param collider element avec lequel on est en collision
	 * @param p 
	 */
	protected abstract void faireCollision(Element collider, Partie p);
	
	
	protected PVector getResponseForce(Element e)
	{
		return e.getForme().getResponseForce(forme);
	}
	
	/**
	 * Retourne la forme de l'élément
	 */
	public Forme getForme()
	{
		return forme;
	}
	
	public long getId() 
	{
		return id;
	}
	
	public float getX()
	{
		return pos.x;
	}
	
	public float getY() {
		return pos.y;
	}
	
	public void setY(float y)
	{
		pos.y = y;
	}
	
	public void setX(float x)
	{
		pos.x = x;
	}
	
	public void setInit()
	{
		estInit = true;
	}
	
	public void setMasse(float m)
	{
		masse = m;
	}

	//Un element n'evolue pas par defaut
	public void evoluer(long t, Partie p) {}
	public void appliquerForce(PVector force) {}


	public void ecrire(DataOutputStream stream, boolean init) throws IOException{

		if (init || !estInit)
		{
			Logger.println("Writed code " + getCode(), LogLevel.NET_DEBUG);
			stream.writeInt(getCode());
			Logger.println("Writed masse " + masse, LogLevel.NET_DEBUG);
			stream.writeFloat(masse);
			Logger.println("Writed x " + pos.x, LogLevel.NET_DEBUG);
			stream.writeFloat(pos.x);
			Logger.println("Writed y " + pos.y, LogLevel.NET_DEBUG);
			stream.writeFloat(pos.y);
			Logger.println("Writed id " + id, LogLevel.NET_DEBUG);
			stream.writeLong(id);
		}

		Logger.println("Writed detruit " + detruit, LogLevel.NET_DEBUG);
		stream.writeBoolean(detruit);
	}
	
	public void lire(DataInputStream stream, boolean init) throws IOException{
		
		if (init)
		{
			masse = stream.readFloat();
			Logger.println("Readed masse float " + masse, LogLevel.NET_DEBUG);
			pos.x = stream.readFloat();
			Logger.println("Readed x float " + pos.x, LogLevel.NET_DEBUG);
			pos.y = stream.readFloat();
			Logger.println("Readed y float " + pos.y, LogLevel.NET_DEBUG);
			id = stream.readLong();
		}
		detruit = stream.readBoolean();
		Logger.println("Readed detruit " + detruit, LogLevel.NET_DEBUG);
	}
	
	//Peut etre remplacee par une methode surchargee
	private int getCode() {
		try {
			return getClass().getField("CODE").getInt(null);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			//e.printStackTrace();
			return -1;
		}
	}
	
	public static Element creer(DataInputStream stream, long id) throws IOException{
		int code = stream.readInt();
		
		Logger.print("Type = " + code + " , ", LogLevel.NET_DEBUG);
			
		Element e = null;
		switch(code)
		{
		case Balle.CODE:
			e = new Balle(stream, id);
			break;
		case Bloc.CODE:
			e = new Bloc(stream, id);
			break;
		case BlocRond.CODE:
			e = new BlocRond(stream, id);
			break;
		case Mur.CODE:
			e = new Mur(stream, id);
			break;
		case Personnage.CODE:
			e = new Personnage(stream, id);
			break;
		case Grenade.CODE:
			e = new Grenade(stream, id);
			break;
		case Missile.CODE:
			e = new Missile(stream, id);
			break;
		case Explosion.CODE:
			e = new Explosion(stream, id);
			break;
		case Projectile.CODE:
			e = new Projectile(stream, id);
			break;
		case Sol.CODE:
			e = new Sol(stream, id);
			break;
		case Tourette.CODE:
			e = new Tourette(stream, id);
			break;
		case Plateforme.CODE:
			e = new Plateforme(stream, id);
			break;
		case ArmeCollectable.CODE:
			e = new ArmeCollectable(stream, id);
			break;
		case BuffCollectable.CODE:
			e = new BuffCollectable(stream, id);
			break;
		}
		
		return e;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Element other = (Element) obj;
		if (detruit != other.detruit)
			return false;
		if (Float.floatToIntBits(masse) != Float.floatToIntBits(other.masse))
			return false;
		if (pos == null) {
			if (other.pos != null)
				return false;
		} else if (!pos.equals(other.pos))
			return false;
		return true;
	}

	public float distanceSq(Element b) {
		PVector posA = forme.getNearest(b.pos.x, b.pos.y);
		PVector posB = b.forme.getNearest(pos.x, pos.y);
		
		float dx = posA.x - posB.x, dy = posA.y - posB.y;
		return dx * dx + dy * dy;
	}

	
}
