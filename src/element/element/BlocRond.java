package element.element;

import java.io.DataInputStream;
import java.io.IOException;

import element.collision.Cercle;
import processing.core.PApplet;

public class BlocRond extends Bloc {
	
	public static final int CODE = 4;
	

	public BlocRond(float x, float y, float r) {
		super(x, y, r * 2, r * 2);
		forme = new Cercle(pos, r);
	}

	protected BlocRond(DataInputStream stream, long id) throws IOException {
		super(stream, id);
	}

	@Override
	public void afficher(PApplet p) {
		p.fill(255);
		p.ellipse(pos.x, pos.y, h, w);
	}

}
