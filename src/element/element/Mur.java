package element.element;

import java.io.DataInputStream;
import java.io.IOException;


import processing.core.PApplet;
import processing.core.PImage;
import utils.Ressources;
import utils.Utils;

public class Mur extends Bloc {
	
	public static final int CODE = 8;
	
	public final static  int W = 10;
	
	public Mur(float wEcran, float hEcran, float x) {
		super(x, 5, W, hEcran -5);
	}
	
	protected Mur(DataInputStream stream, long id) throws IOException{
		super(stream, id);
	}
	
	@Override
	public void detruire()
	{
		
	}
	
	public void afficher(PApplet p) {
		if (estDetruit())
			return; 
		else {
			PImage img = Ressources.getImage("mur");
			for (int x = 0; x < w; x += img.width)
			{
				for (int y = 0; y < h; y += img.height)
				{
					float cropW = Math.min(img.width, w - x), cropH = Math.min(img.height, h - y);
					p.image(img, x + pos.x, y + pos.y, cropW, cropH);
				}
			}
			
		}
	}

}
