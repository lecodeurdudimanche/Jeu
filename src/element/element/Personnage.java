package element.element;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import element.arme.*;
import element.buff.Buff;
import element.collision.Forme;
import element.collision.Rectangle;
import element.item.Item;
import partie.controles.Controlable;
import partie.controles.Controle;
import partie.partie.Partie;
import partie.statistiques.ListeStatistiques;
import partie.statistiques.TypeStatistique;
import processing.core.*;
import utils.LogLevel;
import utils.Logger;
import utils.Ressources;
import utils.Utils;


public class Personnage extends ElementMobile implements JoueurEquipe, Controlable, Comparable<Personnage>{
	
	public static final int CODE = 9;
	public static final int ROUGE = 0, VERT = 1, BLEU = 2;
	private final static char[] COLORS_CHARS = new char[] {'R', 'V', 'B'};
	private final static int[] COLORS = {
			Utils.color(255,0,0),
			Utils.color(0,255,0),
			Utils.color(0,0,255),
			Utils.color(125,125,255),
			Utils.color(255,125,125),
			Utils.color(125,255,125)
	};
	private final static long COOLDOWN_SAUT = 300;
	public final static int W = 32, H = 32;
	
	
	private long invincibilite, lastSaut, lastInvicibiliteFrame;
	private boolean shootDroite, blink, invincible, sprint;
	private float vitesseMax;

	private PImage gauche, droite;
	private static PImage invincibleGauche, invincibleDroite;
	
	private int couleur, score, equipe;
	private Arme arme;
	private String nom;
	private List<Buff> buffs;
	private int nbSauts, nbSautsMax;

	public Personnage(float x, float y, long t, int couleur, int equipe) {
		super(x, y, t);
		forme = new Rectangle(pos, W, H);
		shootDroite = true;
		masse = 5;
		coeffRebond.y = 0;
		coeffRebond.x = 0;
		score = 0;
		lastSaut = 0;
		
		reset(x, y);
		
		this.couleur = couleur;
		nom = "J" + (couleur + 1);
		this.equipe = equipe;
		loadImages();
		
		changerArme(new Pistolet(this));
	}


	public void reset(float x, float y)
	{
		pos.set(x, y);
		vitesse.set(0, 0);
		acceleration.set(0, 0);
		detruit = false;
		setInvincible(tps, 3000);
		vitesseMax = 200;
		sprint = false;
		lastInvicibiliteFrame = 0;
		buffs = new ArrayList<>();
		nbSauts = 0;
		nbSautsMax = 1;
	}
	
	protected Personnage(DataInputStream stream, long id) throws IOException{
		super(stream, id);
		loadImages();
	}
	
	protected void loadImages()
	{
		char couleurChar = COLORS_CHARS[couleur % COLORS_CHARS.length];
		gauche = Ressources.getImage("PersoG" + couleurChar);
		droite = Ressources.getImage("PersoD" + couleurChar);
		invincibleGauche = Ressources.getImage("PersoGI");
		invincibleDroite = Ressources.getImage("PersoDI");
	}

	@Override
	public void evoluer(long t, Partie j)
	{
		float duree = (t - tps) / 1000f;
		super.evoluer(t, j);
		

		if (t > invincibilite)
		{
			invincible = false;
		}
		
		//Buffs
		buffs.stream().filter(b -> b.estFini()).forEach(b -> b.enlever(this));
		buffs.removeIf(b -> b.estFini());
		
		//Friction horizontale
		if (bloquageBas)
		{
			//% de friction / s
			float frict = 10f;
			float frictionX =  vitesse.x * (frict * duree);
			vitesse.x -= frictionX;
		}
		
		if (t - lastInvicibiliteFrame > 100)
		{
			blink = !blink;
			lastInvicibiliteFrame = t;
		}
	}

	@Override
	public void afficher(PApplet p) {		
		PImage aAfficher;
		
		if (invincible && !blink) {
			if (shootDroite)
				aAfficher = invincibleDroite;
			else
				aAfficher = invincibleGauche;
		}
		else {	
			if (shootDroite)
				aAfficher = droite;
			else
				aAfficher = gauche;
		}
		
		/*p.stroke(255, 255, 0);
		p.strokeWeight(3);
		if (bloquageDroite)
			p.line(pos.x + W, pos.y, pos.x + W, pos.y + H);
		if (bloquageHaut)
			p.line(pos.x, pos.y, pos.x + W, pos.y);
		if (bloquageBas)
			p.line(pos.x, pos.y + H, pos.x + W, pos.y + H);
		if (bloquageGauche)
			p.line(pos.x, pos.y, pos.x, pos.y + H);
		p.strokeWeight(1);*/
		
		
		p.image(aAfficher, pos.x, pos.y, W, H);
		
		p.fill(getCouleurAffichage());
		p.textSize(12);
		p.textAlign(PApplet.CENTER);
		p.text(nom, pos.x + W / 2, pos.y - 12);
	}
	
	public void changerArme(Arme a)
	{
		if (a != null)
		{
			arme = a;
			arme.setOwner(this);
		}
	}
	
	public void ajouterBuff(Buff b)
	{
		if (b != null)
		{
			b.appliquer(this);
			buffs.add(b);
		}
	}

	@Override
	public void action(Controle c, Partie j, float[] args) {		
		long currentTime = System.currentTimeMillis();
		
		Forme boxSaut = new Rectangle(new PVector(pos.x - 1, pos.y), W + 2, H + 1);
		boolean peutSauter = j.checkCollision(boxSaut, this) != null;
		
		switch(c)
		{
		case DROITE:
			//Si on touche le sol, on va plus vite que si on se dirige en l'air
			if (peutSauter)
				vitesse.x = vitesseMax;
			else
				vitesse.x = Math.max(vitesse.x, vitesseMax / 5);
			setShootDroite(true);
			break;
		case GAUCHE:
			if (peutSauter)
				vitesse.x = -vitesseMax;
			else
				vitesse.x = Math.min(vitesse.x, -vitesseMax / 5);
			setShootDroite(false);
			break;
		case SPRINT:
			if (!sprint)
				incrementVitesseMax(70);
			sprint = true;
			break;
		case SPRINT_RELACHE:
			if (sprint)
				incrementVitesseMax(-70);
			sprint = false;
			break;
		case TIR_RELACHE:
			if (args == null || sprint)
				break;
			
			long time = (long) args[0];
			
			float force = Utils.map(Utils.constrain(time, 100, 1500), 0, 1500, 0, 1);
			float x, y = pos.y + H/2;
			float xSpeed;
			
			if (shootDroite)
			{
				x = pos.x + W;
				xSpeed = 1;
			}
			else
			{
				x = pos.x;
				xSpeed = -1;
			}
			
			
			PVector dir = new PVector(xSpeed, -0.1f).normalize();

			Balle projectile = arme.tirer(x, y, dir, force, currentTime);
			
			if (projectile != null)
			{
				projectile.setX(projectile.getX() + xSpeed * (projectile.getRayon() * 2 + 3));
				boolean ajoute = j.ajouterElement(projectile);
				if (ajoute)
					ListeStatistiques.log(getId(), TypeStatistique.TIRS, 1);
			}
			break;
		case SAUT:
			if (currentTime > lastSaut + COOLDOWN_SAUT)
			{

				if (peutSauter)
					nbSauts = 0;
				else if (nbSauts < nbSautsMax - 1)
				{
					nbSauts++;
					peutSauter = true;
				}
				
				if (peutSauter)
				{
					vitesse.y = - 350;
					lastSaut = currentTime;
				}
				/*for(Element e : lastCollision)
				{
					if (e!= null && e instanceof ElementCollectable)
					{
						((ElementCollectable) e).collecter(this);
					}
				}*/
			}
			break;
		default:
			break;
		}

	}

	public int getCouleurAffichage() {
		return COLORS[getCouleur() % 3];
	}
	
	public int getCouleur() {
		return couleur;
	}

	public int getScore() {
		return score;
	}

	@Override
	public int getEquipe() {
		return equipe;
	}
	
	public void setEquipe(int e) {
		equipe = e;
		couleur = e;
	}
	
	public int ajouterScore(int s)
	{
		//if (s + score >= 0)
			score += s;
		return score;
	}


	private void setShootDroite(boolean b) {
		shootDroite = b;
		
	}
	
	public void setInvincible(long tps, long dur) {
		invincible = true;
		invincibilite = tps + dur;
	}

	@Override
	public void detruire()
	{
		if (!invincible)
		{
			super.detruire();
			ListeStatistiques.log(getId(), TypeStatistique.MORTS);
		}
	}
	
	@Override
	public void ecrire(DataOutputStream stream, boolean init) throws IOException{
		super.ecrire(stream, init);
		
		if (init || !estInit)
		{
			stream.writeInt(couleur);
			Logger.println("Writed color char " + (int) couleur, LogLevel.NET_DEBUG);
			stream.writeUTF(nom);
			Logger.println("Writed nom" + nom, LogLevel.NET_DEBUG);
		}
		
		stream.writeBoolean(shootDroite);
		Logger.println("Writed shotDroite boolean " + shootDroite, LogLevel.NET_DEBUG);
		stream.writeBoolean(invincible);
		Logger.println("Writed invincible boolean " + invincible, LogLevel.NET_DEBUG);
		stream.writeBoolean(blink);
		Logger.println("Writed blink boolean " + blink, LogLevel.NET_DEBUG);
		stream.writeInt(score);
		Logger.println("Writed score int " + score, LogLevel.NET_DEBUG);
		
		//arme.ecrire(stream, init);
	}

	@Override
	public void lire(DataInputStream stream, boolean init) throws IOException{
		super.lire(stream, init);
		
		if (init)
		{
			couleur = stream.readInt();
			Logger.println("Readed color char " + (int) couleur, LogLevel.NET_DEBUG);
			nom = stream.readUTF();
			Logger.println("Readed nom" + nom, LogLevel.NET_DEBUG);
		}
		
		shootDroite = stream.readBoolean();
		Logger.println("Readed shotDroite boolean " + shootDroite, LogLevel.NET_DEBUG);
		invincible = stream.readBoolean();
		Logger.println("Readed invincible boolean " + invincible, LogLevel.NET_DEBUG);

		blink = stream.readBoolean();
		Logger.println("Readed blink boolean " + blink, LogLevel.NET_DEBUG);
		
		score = stream.readInt();
		Logger.println("Readed score int " + score, LogLevel.NET_DEBUG);
		//arme.lire(stream);
	}

	public void equiper(Item item) {
		ListeStatistiques.log(getId(), TypeStatistique.ITEMS);
		if (item instanceof Arme)
			changerArme((Arme) item);
		else if (item instanceof Buff)
			ajouterBuff((Buff) item);
	}

	public String getNom() {
		return nom;
	}
	
	public void setNom(String nouveau) {
		if (nouveau != null) {
			int max = 10;
			nom = nouveau.length() > max ?nouveau.substring(0, max) : nouveau;
		}
	}
	
	public void ajouterSaut(int i)
	{
		if (nbSautsMax + i > 0)
			nbSautsMax += i;
	}


	public void incrementVitesseMax(float amt) {
		if (vitesseMax + amt >= 0)
			vitesseMax += amt;
		else
			vitesseMax = 0;
	}

	@Override
	public int compareTo(Personnage o) {
		int s = o.score - score;
		if (s == 0)
			return (int) (id - o.id);
		return s;
	}
	
	@Override 
	public boolean equals(Object o) {
		return (o != null) && (o instanceof Personnage) && (((Personnage) o).id == id);
	}



}
