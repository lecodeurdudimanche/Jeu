package element.element;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.function.Predicate;

import element.item.Item;
import partie.partie.Partie;
import utils.LogLevel;
import utils.Logger;

public abstract class ElementCollectable extends ElementMobile{

	protected Item item;
	
	public ElementCollectable(float x, float y, long tps, Item contenu) {
		super(x, y, tps);
		item = contenu;
	}
	
	protected ElementCollectable(DataInputStream stream, long id) throws IOException {
		super(stream, id);
	}
	
	@Override
	protected void faireCollision(Element collider, Partie p) {
		if (collider instanceof Personnage)
		{
			collecter(((Personnage) collider));
		}
	}
	
	@Override
	public void ecrire(DataOutputStream stream, boolean init) throws IOException
	{
		super.ecrire(stream, init);
		
		stream.writeBoolean(item != null);
		if (item != null)
			item.ecrire(stream, init);
	}
	
	@Override
	public void lire(DataInputStream stream, boolean init) throws IOException
	{
		super.lire(stream, init);
		
		boolean hasItem = stream.readBoolean();
		Logger.println("Has item : " + hasItem, LogLevel.NET_DEBUG);
		
		if (hasItem)
			item = Item.creer(stream);
		else
			item = null;

		Logger.println("Item :" +item, LogLevel.NET_DEBUG);
	}
	
	public boolean itemIs(Predicate<Item> cond)
	{
		return cond.test(item);
	}

	protected Item collecter(Personnage p)
	{
		if (!estDetruit())
		{
			detruire();
			p.equiper(item);
		}
		return null;
	}

	
}
