package element.buff;

import element.element.Personnage;
import element.item.Item;

public interface Buff extends Item{

	public void appliquer(Personnage p);
	
	public void enlever(Personnage p);
	
	public boolean estFini();
}
