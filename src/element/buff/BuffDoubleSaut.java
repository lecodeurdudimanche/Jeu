package element.buff;

import java.io.DataInputStream;
import java.io.IOException;

import element.element.Personnage;

public class BuffDoubleSaut extends BuffBasique{

	public static final int CODE = 103;

	public BuffDoubleSaut(long duree) {
		super(duree);
	}

	public BuffDoubleSaut(DataInputStream stream) throws IOException {
		super(stream);
	}
	
	@Override
	public void appliquer(Personnage p) {
		super.appliquer(p);
		p.ajouterSaut(1);
	}

	@Override
	public void enlever(Personnage p) {
		p.ajouterSaut(-1);
	}


}
