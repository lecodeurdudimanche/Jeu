package element.buff;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import element.element.Personnage;

public class BuffBasique implements Buff {
	
	protected long endOfLife, duree;
	
	protected BuffBasique(long duree)
	{
		this.duree = duree;
	}

	public BuffBasique(DataInputStream stream) throws IOException {
		lire(stream, true);
	}

	@Override
	public void appliquer(Personnage p) {
		endOfLife = System.currentTimeMillis() + duree;
	}

	@Override
	public void enlever(Personnage p) {
		
	}

	@Override
	public boolean estFini() {
		return System.currentTimeMillis() >= endOfLife;
	}

	@Override
	public void lire(DataInputStream stream, boolean init) throws IOException {
		
	}

	@Override
	public void ecrire(DataOutputStream stream, boolean init) throws IOException {
		if (init)
		{
			try {
				stream.writeInt(getClass().getDeclaredField("CODE").getInt(null));
			} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			}
		}
	}

}
