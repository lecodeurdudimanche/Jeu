package element.buff;

import java.io.DataInputStream;
import java.io.IOException;

import element.element.Personnage;

public class BuffInvincibilite extends BuffBasique {

	public static final int CODE = 101;

	public BuffInvincibilite(long duree) {
		super(duree);
	}
	
	 public BuffInvincibilite(DataInputStream stream) throws IOException {
		 super(stream);
	}

	@Override
	 public void appliquer(Personnage p) {
		 super.appliquer(p);
		 p.setInvincible(System.currentTimeMillis(), duree);
	 }
}
