package element.buff;

import java.io.DataInputStream;
import java.io.IOException;

import element.element.Personnage;

public class BuffVitesse extends BuffBasique {

	public static final int CODE = 102;
	
	protected float buff;
	
	public BuffVitesse() {
		this(5000);
	}
	
	public BuffVitesse(long duree) {
		this(duree, 75);
	}
	
	public BuffVitesse(long duree, float buff) {
		super(duree);
		this.buff = buff;
	}
	
	 public BuffVitesse(DataInputStream stream) throws IOException {
		 super(stream);
	 }

	@Override
	 public void appliquer(Personnage p) {
		 super.appliquer(p);
		 p.incrementVitesseMax(buff);
	 }
	 
	 @Override
	 public void enlever(Personnage p) {
		 p.incrementVitesseMax(-buff);
	 }

}
