#!/bin/bash

echo "Removing old jars"
git stash > /dev/null
mv config.ini .config
git checkout release > /dev/null
git rm client.jar server.jar updater.jar> /dev/null
git commit -m "Remove old jars"
git checkout master > /dev/null
git tag -a -f -m "Le dernier export vers release a été effectué" export
echo "Building client.jar"
[ -e client.jar ] && rm client.jar
ant -buildfile export_client.xml>/dev/null
[ $? -ne 0 ] && exit 1

echo "Building server.jar"
[ -e server.jar ] && rm server.jar
ant -buildfile export_server.xml>/dev/null
[ $? -ne 0 ] && exit 1

echo "Building updater.jar"
[ -e updater.jar ] && rm updater.jar
ant -buildfile export_updater.xml >/dev/null
[ $? -ne 0 ] && exit 1

echo "Adding new jars"
git checkout release

version=$(cat version.txt)
version=$((version + 1))
echo $version>version.txt

git add server.jar client.jar updater.jar version.txt exec.sh exec.bat
git commit -m "Adding new jars - Version $version"
git push releaseOrigin HEAD:master
git checkout master > /dev/null
git stash pop > /dev/null
mv .config config.ini

echo -e "\nExport terminé de la version $version\n"
