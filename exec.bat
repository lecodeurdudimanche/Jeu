@echo off

rem Hack de ping parceque windows a pas de sleep
rem On attend (3 - 1) = 2 secondes
ping 127.0.0.1 -n 3 > nul

del /s /f /q "%~2"
move "%~1" "%~2"

rem delete other script
del /f /q %~dp0\exec.sh
rem exit and delete this file
(goto) 2>nul & del /f /q %~dp0\exec.bat
